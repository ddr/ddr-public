# Caching

In production ddr-public uses [memcached](https://memcached.org/).

## Rails

Rails config specifies `:mem_cache_store` as the cache backend.  Two variables
affect the config:

    MEMCACHE_SERVERS   # a comma separated list
    MEMCACHE_POOL_SIZE # an integer number of clients to create in the connection pool

See the [Rails caching guide](https://guides.rubyonrails.org/caching_with_rails.html)
for details on caching in Rails.

## Image Server

The image server is also configured to use memcached.  See the
[server documentation](https://iipimage.sourceforge.io/documentation/server/) for
details.

## Cache commands

A convenience wrapper script, `cache-cmd`, has been added to memcached container
image to support a few operations.

Example:

```
$ docker-compose exec cache cache-cmd stats
STAT pid 1
STAT uptime 447
STAT time 1580492644
STAT version 1.5.21
STAT libevent 2.1.8-stable
STAT pointer_size 64
STAT rusage_user 0.113090
STAT rusage_system 0.109949
STAT max_connections 1024
STAT curr_connections 4
STAT total_connections 19
STAT rejected_connections 17
STAT connection_structures 5
STAT reserved_fds 20
STAT cmd_get 35
STAT cmd_set 23
STAT cmd_flush 0
STAT cmd_touch 0
STAT cmd_meta 0
STAT get_hits 12
STAT get_misses 23
STAT get_expired 0
STAT get_flushed 0
STAT delete_misses 0
[...]
END
```
