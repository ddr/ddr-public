# frozen_string_literal: true

source 'https://rubygems.org'

ruby File.read(File.expand_path('.ruby-version', __dir__))

gem 'aws-sdk-s3', require: false
gem 'blacklight', '~> 7.32'
gem 'blacklight-gallery', '~> 4.0'
gem 'blacklight_oai_provider', '~> 7.0'
gem 'blacklight_range_limit', '~> 8.2'
gem 'bootstrap', '~> 4.0'
gem 'cancancan', '~> 3.4'
gem 'coffee-rails', '~> 5.0'
gem 'connection_pool'
gem 'dalli', '~> 3.0'
gem 'devise', '~> 4.8'
gem 'devise-guests', '~> 0.8'
gem 'edtf-humanize', '~> 2.1.0'
gem 'font-awesome-sass', '~> 6.2'
gem 'hexapdf'
gem 'i18n-tasks'
gem 'jquery-rails'
gem 'jstree-rails-4', '3.3.8'
gem 'loofah', '>= 2.19.0'
gem 'mime-types'
gem 'omniauth'
gem 'omniauth_openid_connect'
gem 'pg'
gem 'prawn'
gem 'puma'
gem 'rack-attack'
gem 'rails', '~> 7.2.0'
gem 'rails_autolink'
gem 'recaptcha'
gem 'rsolr', '>= 1.0', '< 3'
gem 'ruby-progressbar'
gem 'sassc-rails', '~> 2.0'
gem 'terser'
gem 'webvtt-ruby', '0.4.0'
gem 'zip_kit', '~> 6.3.0'

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

# Profiling
gem 'memory_profiler'
gem 'rack-mini-profiler'
gem 'stackprof'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
end

group :test do
  gem 'axe-core-rspec' # accessibility tests
  gem 'capybara' # feature tests
  gem 'equivalent-xml'
  gem 'factory_bot_rails'
  gem 'rails-controller-testing'
  gem 'rspec-its'
  gem 'rspec-rails', '~> 7.0'
  gem 'selenium-webdriver' # feature tests w/javascript
end

group :development do
  gem 'bundler-audit'
  gem 'debug'
  gem 'erb_lint', require: false
  gem 'listen', '>= 3.0.5', '< 3.8'
  gem 'rubocop', require: false
  gem 'rubocop-capybara', require: false
  gem 'rubocop-factory_bot', require: false
  gem 'rubocop-performance', require: false
  gem 'rubocop-rails', require: false
  gem 'rubocop-rspec', require: false

  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
end

gem 'jsbundling-rails', '~> 1.3'
