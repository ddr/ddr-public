# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'IIIF presentation requests', :iiif do
  let(:file) { double(read: file_fixture('iiif/presentation/manifest.json').read) }
  let(:iiif_file) { double(file:) }
  let(:permanent_id) { 'ark:/99999/fk4zyzzyx' }
  let(:resource_id) { SecureRandom.uuid }
  let(:body) { double('body') }
  let(:iiif_manifest) { {body:} }
  let(:manifest_json) { '{"manifest": "data"}' }
  let(:document) { double(permanent_id:, resource_id:, id: resource_id, iiif_file:, rights_ssi: nil, common_model_name: 'Item') }
  let(:ability) { Ability.new }

  # rubocop:disable RSpec/AnyInstance
  before do
    allow_any_instance_of(IiifPresentationController).to receive(:current_ability).and_return(ability)
    allow_any_instance_of(IiifPresentationController).to receive(:iiif_manifest).and_return(iiif_manifest)
    allow(body).to receive(:to_ordered_hash).and_return(manifest_json)
  end
  # rubocop:enable RSpec/AnyInstance

  describe 'with a resource ID' do
    before do
      allow(SolrDocument).to receive(:find).with(resource_id).and_return(document)
    end

    describe 'when the user CANNOT download the manifest file' do
      it 'is forbidden' do
        get "/iiif/#{resource_id}/manifest"
        expect(response.response_code).to eq 403
      end
    end

    describe 'when the user CAN download the manifest file' do
      before { ability.can :download_iiif_file, document }

      it 'is successful' do
        get "/iiif/#{resource_id}/manifest"
        expect(response.response_code).to eq 200
      end
    end
  end

  describe 'with a permanent ID' do
    # rubocop:disable RSpec/AnyInstance
    before do
      allow_any_instance_of(IiifPresentationController).to receive(:find_by_permanent_id).and_return(document)
    end
    # rubocop:enable RSpec/AnyInstance

    describe 'when the user CANNOT download the manifest file' do
      it 'is forbidden' do
        get "/iiif/#{URI.encode_www_form_component(permanent_id)}/manifest"
        expect(response.response_code).to eq 403
      end
    end

    describe 'when the user CAN download the manifest file' do
      before { ability.can :download_iiif_file, document }

      it 'is successful' do
        get "/iiif/#{URI.encode_www_form_component(permanent_id)}/manifest"
        expect(response.response_code).to eq 200
      end
    end
  end
end
