# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Home page requests' do
  after { Warden.test_reset! }

  describe 'GET /' do
    describe 'when authentication is required' do
      before do
        allow(Ddr::Public).to receive(:require_authentication).and_return(true)
      end

      describe 'when the user is not authenticated' do
        it 'redirects to the login page' do
          get '/'
          expect(response).to redirect_to('/users/sign_in')
        end
      end

      describe 'when the user is authenticated' do
        let(:user) { create(:user) }

        before { sign_in user }

        it 'returns a 200' do
          get '/'
          expect(response).to have_http_status(:ok)
        end
      end
    end

    describe 'when authentication is not required' do
      before do
        allow(Ddr::Public).to receive(:require_authentication).and_return(false)
      end

      describe 'when the user is not authenticated' do
        it 'redirects to the login page' do
          get '/'
          expect(response).to have_http_status(:ok)
        end
      end

      describe 'when the user is authenticated' do
        let(:user) { create(:user) }

        before { sign_in user }

        it 'returns a 200' do
          get '/'
          expect(response).to have_http_status(:ok)
        end
      end
    end
  end
end
