# frozen_string_literal: true

# Tests for OAI-PMH responses provided via blacklight_oai_provider gem & local
# configurations. See https://github.com/projectblacklight/blacklight_oai_provider

require 'rails_helper'

RSpec.describe 'OAI-PMH endpoints' do
  let(:xml) { Nokogiri::XML(response.body) }
  let(:namespaces) do
    {
      dc: 'http://purl.org/dc/elements/1.1/',
      xmlns: 'http://www.openarchives.org/OAI/2.0/',
      oai_dc: 'http://www.openarchives.org/OAI/2.0/oai_dc/'
    }
  end

  context 'within the catalog controller' do
    context 'with Identify verb' do
      it 'identifies the repository' do
        get '/catalog/oai?verb=Identify'
        expect(xml.at_xpath('/xmlns:OAI-PMH/xmlns:Identify/xmlns:repositoryName').text).to eql 'Duke Digital Repository'
      end
    end

    context 'with ListSets verb' do
      it 'creates a set for each admin set' do
        get '/catalog/oai?verb=ListSets'
        nodes = xml.xpath('//xmlns:ListSets//xmlns:set/xmlns:setSpec', namespaces)
        expect(nodes.map(&:text)).to include('admin_set:Digital Collections', 'admin_set:Rubenstein Library')
      end

      it 'creates a set for each collection' do
        get '/catalog/oai?verb=ListSets'
        nodes = xml.xpath('//xmlns:ListSets//xmlns:set/xmlns:setSpec', namespaces)
        expect(nodes.map(&:text)).to include('collection:abcd0001-0000-0000-0000-000000000000')
      end
    end
  end

  context 'within the items controller (item objects only)' do
    context 'with ListRecords verb' do
      it 'only lists item objects' do
        get '/items/oai?verb=ListRecords&metadataPrefix=oai_dc'
        nodes = xml.xpath('//xmlns:ListRecords//xmlns:record/xmlns:header/xmlns:identifier', namespaces)
        expect(nodes.map(&:text)).not_to include('oai:ddr:abcd0004-0000-0000-0000-000000000000')
        expect(nodes.map(&:text)).to include('oai:ddr:abcd0005-0000-0000-0000-000000000000')
        expect(nodes.map(&:text)).not_to include('oai:ddr:abcd0006-0000-0000-0000-000000000000')
      end
    end

    context 'with DPLA harvest DC items URL' do
      it 'only lists items in DC admin set' do
        get '/items/oai?verb=ListRecords&metadataPrefix=oai_dc&set=admin_set:Digital%20Collections'
        nodes = xml.xpath('//xmlns:ListRecords//xmlns:record/xmlns:header/xmlns:identifier', namespaces)
        expect(nodes.map(&:text)).to include('oai:ddr:abcd0002-0000-0000-0000-000000000000')
        expect(nodes.map(&:text)).not_to include('oai:ddr:abcd0005-0000-0000-0000-000000000000')
      end

      it 'does not list metadata-only items in DC admin set' do
        get '/items/oai?verb=ListRecords&metadataPrefix=oai_dc&set=admin_set:Digital%20Collections'
        nodes = xml.xpath('//xmlns:ListRecords//xmlns:record/xmlns:header/xmlns:identifier', namespaces)
        expect(nodes.map(&:text)).not_to include('oai:ddr:abcd0013-0000-0000-0000-000000000000')
      end
    end

    context 'with getRecord verb' do
      it 'maps item metadata to Dublin Core' do
        get '/items/oai?verb=GetRecord&metadataPrefix=oai_dc&identifier=oai:ddr:2896e5ec-af34-4f1d-90c5-da335474e211'
        expect(xml.at_xpath('//xmlns:metadata/oai_dc:dc/dc:title', namespaces).text).to eql 'Keep your Beauty on duty!'
        expect(xml.at_xpath('//xmlns:metadata/oai_dc:dc/dc:creator', namespaces).text).to eql 'Procter & Gamble Co.'
        expect(xml.at_xpath('//xmlns:metadata/oai_dc:dc/dc:source', namespaces).text).to eql 'Ad*Access'
      end

      it 'does not return a record for a metadata-only item' do
        get '/items/oai?verb=GetRecord&metadataPrefix=oai_dc&identifier=oai:ddr:abcd0013-0000-0000-0000-000000000000'
        puts response.body
        expect(xml.at_xpath('/xmlns:OAI-PMH/xmlns:error', namespaces).text).to eql 'The value of the identifier argument is unknown or illegal in this repository.'
        expect(xml.at_xpath('//xmlns:metadata', namespaces)).to be_nil
      end
    end
  end
end
