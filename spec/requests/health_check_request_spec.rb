# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'health check' do
  it 'returns a 200' do
    get '/up'
    expect(response.response_code).to eq 200
  end
end
