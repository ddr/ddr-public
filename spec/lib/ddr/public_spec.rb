# frozen_string_literal: true

RSpec.describe Ddr::Public do
  describe 'configuration defaults' do
    subject { described_class }

    its(:adopt_url) { is_expected.to eq('http://library.duke.edu/about/adopt-digital-collection') }
    its(:catalog_url) { is_expected.to eq('https://find.library.duke.edu/catalog/DUKE') }
    its(:contact_email) { is_expected.to eq('repositoryhelp@duke.edu') }
    its(:ddr_logo) { is_expected.to be false }
    its(:ddr_public_preview) { is_expected.to be false }
    its(:matomo_analytics_debug) { is_expected.to be false }
    its(:matomo_analytics_embed_id) { is_expected.to be false }
    its(:matomo_analytics_host) { is_expected.to eq('analytics.lib.duke.edu') }
    its(:matomo_analytics_site_id) { is_expected.to be false }

    its(:help_url) { is_expected.to eq('https://duke.qualtrics.com/jfe/form/SV_eo1QIMoUtvmteQZ?Q_JFE=qdg') }

    describe 'Image server URL', skip: 'DDR-2032' do
      its(:image_server_url) { is_expected.to eq('https://repository.duke.edu/iipsrv/iipsrv.fcgi') }
    end

    its(:paging_limit_facets) { is_expected.to eq('50') }
    its(:paging_limit_results) { is_expected.to eq('250') }
    its(:require_authentication) { is_expected.to be false }
    its(:research_guides) { is_expected.to eq('guides.library.duke.edu') }
    its(:root_url) { is_expected.to eq('https://repository.duke.edu') }
    its(:staff_app_url) { is_expected.to eq('https://ddr-admin.lib.duke.edu/') }
    its(:storage_base_path) { is_expected.to eq('/data') }
  end
end
