# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Portal::ControllerSetup do
  it 'provides a view path with a local_id' do
    setup = described_class.new({ local_id: 'wdukesons' })
    expect(setup.view_path).to eq 'app/views/ddr-portals/wdukesons'
  end

  context 'When a portal is configured to be scoped to multiple collections' do
    it 'provides an array of parent collection ids' do
      document_01 = double({ id: 'e113ce18-04ad-4a82-be7a-77e2bef328e8' })
      document_02 = double({ id: 'eba3d5dc-7f1c-41e4-8c0b-60c6ae9cb8aa' })
      setup = described_class.new
      allow(setup).to receive(:portal_local_ids).and_return(%w[adaccess adviews])
      allow(setup).to receive(:parent_collection_documents) { [document_01, document_02] }
      expect(setup.parent_collection_join_ids).to eq %w[
        id-e113ce18-04ad-4a82-be7a-77e2bef328e8
        id-eba3d5dc-7f1c-41e4-8c0b-60c6ae9cb8aa
      ]
    end
  end

  context 'When a portal is not configured to be scoped to one or more collection' do
    it 'does not return parent collection ids' do
      setup = described_class.new
      allow(setup).to receive(:portal_local_ids).and_return(nil)
      expect(setup.parent_collection_join_ids).to be_nil
    end
  end
end
