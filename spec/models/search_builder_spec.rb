# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SearchBuilder do
  subject { search_builder_class.new(processor_chain, scope) }

  let(:search_builder_class) do
    Class.new(described_class)
  end

  let(:processor_chain) { [] }
  let(:ability) { double(agents: %w[foo bar]) }
  let(:blacklight_config) { CatalogController.blacklight_config.deep_copy }
  let(:context) { nil }

  # scope in BL7 is a SearchService instance
  let(:scope) do
    double blacklight_config:,
           search_state: nil,
           context:
  end

  describe '#truncate_long_queries' do
    let(:solr_parameters) { Blacklight::Solr::Request.new }
    let(:user_params) { {} }

    before do
      builder_with_params.add_query_to_solr(solr_parameters)
      builder_with_params.truncate_long_queries(solr_parameters)
    end

    context 'when it has a short query' do
      let(:short_query) do
        'one two three four five six'
      end
      let(:builder_with_params) { subject.with(q: short_query) }

      it 'does not truncate the query' do
        expect(solr_parameters[:q]).to eq('one two three four five six')
      end
    end

    context 'when it has a long query' do
      let(:long_query) do
        'one two three four five six seven eight nine ten eleven twelve ' \
          'thirteen fourteen fifteen sixteen seventeen eighteen nineteen ' \
          'twenty twentyone twentytwo twentythree twentyfour twentyfive ' \
          'twentysix twentyseven twentyeight'
      end
      let(:builder_with_params) { subject.with(q: long_query) }

      it 'truncates long queries' do
        expect(solr_parameters[:q]).to(
          eq('one two three four five six seven eight nine ten eleven twelve ' \
             'thirteen fourteen fifteen sixteen seventeen eighteen nineteen ' \
             'twenty twentyone twentytwo twentythree twentyfour twentyfive ' \
             'twentysix twen')
        )
      end
    end
  end

  describe '#include_only_collections' do
    it 'returns a filter query that limits results to collection documents' do
      expect(subject.include_only_collections({}))
        .to eq(['common_model_name_ssi:Collection'])
    end
  end

  describe '#include_only_items' do
    it 'returns a filter query that limits results to item documents' do
      expect(subject.include_only_items({}))
        .to eq(['common_model_name_ssi:Item'])
    end
  end

  describe '#include_only_items_or_collections' do
    it 'returns a filter query that limits results to items or collections' do
      expect(subject.include_only_items_or_collections({})).to(
        eq(['{!terms f=common_model_name_ssi method=booleanQuery}Item,Collection'])
      )
    end
  end

  describe '#include_only_multires_images' do
    it 'returns a filter query that limits results to objects that have a multires image' do
      expect(subject.include_only_multires_images({}))
        .to eq(['multires_image_file_path_ssi:[* TO *]'])
    end
  end

  describe '#filter_by_parent_collections' do
    context 'when querying within a single-collection portal (e.g., adaccess)' do
      let(:builder_with_params) { subject.with({ 'action' => 'index' }) }
      let(:context) do
        { current_ability: ability,
          parent_collection_join_ids: %w[id-c1ebafc5-946e-405a-a7a7-27d39b3fd9bb] }
      end

      it 'returns a filter query that limits results to documents with the listed parent document' do
        expect(builder_with_params.filter_by_parent_collections({}))
          .to eq({ fq: ['{!terms f=admin_policy_id_ssim method=booleanQuery}' \
                        'id-c1ebafc5-946e-405a-a7a7-27d39b3fd9bb'] })
      end
    end

    context 'when querying within a multi-collection portal (not yet used in practice)' do
      let(:builder_with_params) { subject.with({ 'action' => 'index' }) }
      let(:context) do
        { current_ability: ability,
          parent_collection_join_ids: %w[id-fcbf115c-6cb1-44a3-8276-3797f9e402a0
                                         id-d7f0bb61-e3fb-4066-950e-ec84d43e97af] }
      end

      it 'returns a filter query that limits results to documents with any of the listed parent documents' do
        expect(builder_with_params.filter_by_parent_collections({}))
          .to eq({ fq: ['{!terms f=admin_policy_id_ssim method=booleanQuery}' \
                        'id-fcbf115c-6cb1-44a3-8276-3797f9e402a0,id-d7f0bb61-e3fb-4066-950e-ec84d43e97af'] })
      end
    end

    context 'when querying within a portal defined by admin set (e.g., dc) rather than individual collections' do
      let(:builder_with_params) { subject.with({ 'action' => 'index' }) }
      let(:context) do
        { current_ability: ability,
          parent_collection_join_ids: nil }
      end

      it 'does not apply a filter query limiting results by collection membership' do
        expect(builder_with_params.filter_by_parent_collections({}))
          .to be_nil
      end
    end
  end

  describe '#exclude_components' do
    let(:builder_with_params) { subject.with({ show_components: 'true' }) }

    it 'returns a filter query that filters components from search results' do
      expect(subject.exclude_components({}))
        .to eq(['-common_model_name_ssi:Component'])
    end

    it 'does not apply the filter query if show_components param is true' do
      expect(builder_with_params.exclude_components({})).to be_nil
    end
  end

  describe '#filter_by_related_items' do
    let(:document) do
      SolrDocument.new(
        'id' => 'changeme:1',
        'isFormatOf_ssi' => ['ark:/99999/fk41n8cr9s', 'ark:/99999/fk4xw4q151']
      )
    end

    before { allow(SolrDocument).to receive(:find).with('changeme:1') { document } }

    context 'related_items_q is a applied' do
      let(:builder_with_params) { subject.with({ id_related_items: 'changeme:1|isFormatOf_ssi|permanent_id_ssi' }) }

      it 'applies the related items query' do
        expect(builder_with_params.filter_by_related_items({}))
          .to eq({ fq: ['{!terms f=permanent_id_ssi method=booleanQuery}' \
                        'ark:/99999/fk41n8cr9s,ark:/99999/fk4xw4q151'] })
      end
    end

    context 'related_items_q is not applied' do
      let(:builder_with_params) { subject.with({}) }

      it 'does not apply the related items query' do
        expect(builder_with_params.filter_by_related_items({}).to_s).to include('')
      end
    end
  end

  describe '#include_only_digital_collections' do
    let(:builder_with_params) { subject.with({ 'action' => 'index_portal' }) }

    it 'returns a filter query that exclude digital collections' do
      expect(builder_with_params.include_only_digital_collections({}))
        .to eq(['admin_set_title_ssi:"Digital Collections"'])
    end
  end

  describe '#exclude_digital_collections' do
    it 'returns a filter query that exclude digital collections' do
      expect(subject.exclude_digital_collections({}))
        .to eq(['-admin_set_title_ssi:"Digital Collections"'])
    end
  end

  describe '#add_boolean_terms_query' do
    let(:field) { 'local_id_ssi' }
    let(:values) { %w[one two three] }
    let(:builder_with_params) do
      subject.with({ term_field_name: field,
                     term_values: values })
    end

    it 'generates a boolean terms query based on supplied BL params' do
      expect(builder_with_params.add_boolean_terms_query({})).to eq(
        { fq: ['{!terms f=local_id_ssi method=booleanQuery}one,two,three'],
          q: '*:*' }
      )
    end
  end
end
