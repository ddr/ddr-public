# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RightsStatement, :aux do
  describe '.all' do
    subject { described_class.all }

    it { is_expected.to be_a Array }
    it { is_expected.to all(be_a(described_class)) }
  end

  describe '.call' do
    describe 'when the object has a rights statement URL' do
      describe 'and the rights statement is found' do
        subject { described_class.call(doc) }

        let(:doc) { SolrDocument.new('rights_ssim' => ['https://creativecommons.org/publicdomain/mark/1.0']) }

        it { is_expected.to be_a described_class }
      end

      describe 'and the rights statement is not found' do
        let(:doc) { SolrDocument.new('rights_ssim' => ['https://myspecialrights.org/']) }

        it 'raises an error' do
          expect { described_class.call(doc) }.to raise_error(Ddr::Public::Error)
        end
      end
    end

    describe 'when the document does not have a rights statement' do
      subject { described_class.call(doc) }

      let(:doc) { SolrDocument.new }

      it { is_expected.to be_nil }
    end
  end

  describe 'string representation' do
    subject { described_class.keystore.fetch('https://creativecommons.org/publicdomain/mark/1.0') }

    its(:to_s) { is_expected.to eq 'Creative Commons Public Domain Mark 1.0' }
  end
end
