# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Contact, :aux do
  describe '.all' do
    subject { described_class.all }

    it { is_expected.to be_a Array }
    it { is_expected.to all(be_a(described_class)) }
  end

  describe '.call' do
    describe 'when the slug is found' do
      subject { described_class.call('dvs') }

      it { is_expected.to be_a described_class }
    end

    describe 'when the slug is not found' do
      specify do
        expect { described_class.call('foo') }.to raise_error(Ddr::Public::Error)
      end
    end
  end

  describe 'string representation' do
    subject { described_class.call('dvs') }

    its(:to_s) { is_expected.to eq 'Center for Data and Visualization Sciences' }
  end
end
