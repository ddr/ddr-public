# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Structure do
  context 'Object has default structural metadata' do
    subject { described_class.new({ structure: }) }

    let(:structure) do
      YAML.safe_load(file_fixture('structures/flat_struct.yml').read)
    end
    let(:ids_exp) do
      YAML.safe_load(file_fixture('structures/flat_struct_ids_exp.yml').read)
    end

    it 'returns an empty array if the structure is absent' do
      expect(subject.images).to eq([])
    end

    it 'has some default ids' do
      expect(subject.default.ids).to eq(ids_exp)
    end
  end

  context 'Object has nested structural metadata with images' do
    subject { described_class.new({ structure: }) }

    let(:structure) do
      YAML.safe_load(file_fixture('structures/group_struct.yml').read)
    end

    it 'returns an empty array if the structure is absent' do
      expect(subject.default).to eq([])
    end

    it 'has some ids for images' do
      expect(subject.images.ids).to eq(%w[c8b0ddf1-812d-483e-a47b-c1b91b03d044
                                          928b37dd-6ae2-4069-9c1b-fb1f5fd51f6b
                                          8a2b6821-209e-4800-8de3-7cca33cbfdfc])
    end

    it 'has an id for the images group' do
      expect(subject.images.id).to eq('dukechapel_dcrst003606-images')
    end

    it 'has some ids for files' do
      expect(subject.files.ids).to eq(['c44230fe-7c2c-4650-a4f7-c5eb66247e5c'])
    end

    it 'has an id for the files group' do
      expect(subject.files.id).to eq('dukechapel_dcrst003606-documents')
    end
  end

  context 'Object has nested structural metadata with AV and documents' do
    subject { described_class.new({ structure: }) }

    let(:structure) do
      YAML.safe_load(file_fixture('structures/group_struct_av.yml').read)
    end
    let(:av_component_doc) do
      SolrDocument.new({
                         'id' => 'c8b0ddf1-812d-483e-a47b-c1b91b03d044',
                         'common_model_name_ssi' => 'Component'
                       })
    end

    it 'has some ids for media objects' do
      expect(subject.media.ids).to eq(%w[d89bd131-7903-4c96-94e3-5d6aa770de61
                                         e7c89277-1e43-4f26-be69-70abc030e861
                                         be406f95-2135-4c81-b8d3-9faade58c5ec])
    end

    it 'has an id for the media group' do
      expect(subject.media.id).to eq('dukechapel_dcrau001201-media')
    end

    it 'finds the first media object (e.g., for av thumb/poster)' do
      allow(SolrDocument).to receive(:find).with('d89bd131-7903-4c96-94e3-5d6aa770de61') { av_component_doc }
      expect(subject.first_media_doc).to eq(av_component_doc)
    end
  end

  context 'Object has directory structural metadata' do
    subject { described_class.new({ structure: }) }

    let(:structure) do
      YAML.safe_load(file_fixture('structures/dir_struct.yml').read)
    end

    it 'has directory structural metadata' do
      expect(subject.directories).to be_a Structure::Directory
    end
  end

  context 'Object does not have directory structural metadata' do
    subject { described_class.new({ structure: }) }

    let(:structure) do
      YAML.safe_load(file_fixture('structures/flat_struct.yml').read)
    end

    it 'returns an empty array' do
      expect(subject.directories).to eq([])
    end
  end
end
