# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FindingAid do
  subject(:finding_aid) { described_class.new(ead_id) }

  let(:ead_id) { 'appleberrydilmus' }

  # rubocop:disable RSpec/SubjectStub, Layout/LineLength
  # Stub the doc method to simulate the JSON response from our Arclight API
  let(:doc) do
    {
      id: 'appleberrydilmus',
      title_ssm: [
        'Dilmus J. Appleberry papers'
      ],
      normalized_title_ssm: [
        'Dilmus J. Appleberry papers, 1810-1927, bulk 1850-1896'
      ],
      repository_ssim: [
        'David M. Rubenstein Rare Book & Manuscript Library'
      ],
      normalized_date_ssm: [
        '1810-1927, bulk 1850-1896'
      ],
      unitid_ssm: [
        'RL.10098'
      ],
      extent_ssm: [
        '2.5 Linear Feet (5 boxes, 1,750 items)',
        'a second extent value'
      ],
      abstract_html_tesm: [
        "<abstract id=\"aspace_423c4102f9846cdea6dfb2a1d1704f7f\">Business, family, and legal correspondence, accounts, bills, invoices, indentures, land surveys, and other papers. Correspondents whose names appear most often are Pettit and Leake, a legal firm of Goochland Court House, Va., Altantic and Virginia Fertilizing Co. of Richmond, Va., and Appleberry's nephew, Thomas A. Bledsoe.</abstract>"
      ]
    }
  end

  before do
    allow(finding_aid).to receive(:doc).and_return(JSON.parse(doc.to_json))
  end
  # rubocop:enable RSpec/SubjectStub, Layout/LineLength

  describe '#url' do
    before { Ddr::Public.finding_aid_base_url = 'https://archives.lib.duke.edu' }

    it 'returns the correct URL' do
      expect(finding_aid.url).to eq('https://archives.lib.duke.edu/catalog/appleberrydilmus')
    end
  end

  describe '#title' do
    it 'returns the correct title' do
      expect(finding_aid.title).to eq('Dilmus J. Appleberry papers, 1810-1927, bulk 1850-1896')
    end
  end

  describe '#repository' do
    it 'returns the correct repository' do
      expect(finding_aid.repository).to eq('David M. Rubenstein Rare Book & Manuscript Library')
    end
  end

  describe '#collection_date_span' do
    it 'returns the correct collection date span' do
      expect(finding_aid.collection_date_span).to eq('1810-1927, bulk 1850-1896')
    end
  end

  describe '#collection_number' do
    it 'returns the correct collection number' do
      expect(finding_aid.collection_number).to eq('RL.10098')
    end
  end

  describe '#collection_title' do
    it 'returns the correct collection title' do
      expect(finding_aid.collection_title).to eq('Dilmus J. Appleberry papers')
    end
  end

  describe '#extent' do
    it 'returns the correct extent' do
      expect(finding_aid.extent).to eq('2.5 Linear Feet (5 boxes, 1,750 items); a second extent value')
    end
  end

  describe '#abstract' do
    it 'returns the correct abstract' do
      expect(finding_aid.abstract).to \
        eq('Business, family, and legal correspondence, accounts, bills, invoices, ' \
           'indentures, land surveys, and other papers. Correspondents whose names ' \
           'appear most often are Pettit and Leake, a legal firm of Goochland Court ' \
           'House, Va., Altantic and Virginia Fertilizing Co. of Richmond, Va., and ' \
           'Appleberry\'s nephew, Thomas A. Bledsoe.')
    end
  end
end
