# frozen_string_literal: true

require 'spec_helper'

RSpec.describe OaiItemSearchBuilder do
  subject { search_builder_class.new(processor_chain, {}) }

  let(:search_builder_class) do
    Class.new(described_class)
  end

  let(:processor_chain) { [] }

  describe '#processor_chain' do
    let(:sb) { search_builder_class.new(ItemsController.new) }

    it 'adds gated read to the processor chain to exclude metadata-only items' do
      expect(sb.processor_chain).to include(:apply_gated_read)
    end
  end
end
