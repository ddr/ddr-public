# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SolrDocument do
  subject(:document) { described_class.new(id:) }

  let(:id) { '87567572-b669-48a9-b478-a679725f85e6' }
  let(:basic_document) { described_class.new(id:) }

  let(:doc_config) do
    YAML.safe_load(file_fixture('portal_configs/portal_doc_config.yml').read)
  end

  let(:view_config) do
    YAML.safe_load(file_fixture('portal_configs/portal_view_config.yml').read, permitted_classes: [Symbol])
  end

  describe '#abstract' do
    subject { described_class.new('id' => id, 'abstract_tsim' => ['An Abstract']) }

    its(:abstract) { is_expected.to eq 'An Abstract' }
  end

  describe '#description' do
    let(:document) do
      described_class.new('id' => id,
                          'description_tsim' => ['A Description'])
    end

    it 'returns a description' do
      expect(document.description).to eq('A Description')
    end
  end

  describe '#rights_notes' do
    let(:document) do
      described_class.new('id' => id,
                          'rights_note_tsim' => ['A Right', 'Another Right'])
    end

    it 'returns rights_notes' do
      expect(document.rights_notes).to eq(['A Right', 'Another Right'])
    end
  end

  describe '#format' do
    let(:document) do
      described_class.new('id' => id,
                          'format_tsim' => ['A Format', 'Another Format'])
    end

    it 'returns format' do
      expect(document.format).to eq(['A Format', 'Another Format'])
    end
  end

  describe '#humanized_date' do
    let(:document) do
      described_class.new('id' => id,
                          'date_tsim' => ['1850~', '2001-21', 'not EDTF'])
    end

    it 'returns humanized forms of the dates' do
      expect(document.humanized_date).to eq(['circa 1850', 'spring 2001', 'not EDTF'])
    end
  end

  describe '#display_inline_pdf?' do
    let(:document) do
      described_class.new(id:)
    end

    context 'document includes a downloadable PDF' do
      let(:pdf_component_doc) do
        described_class.new(id: 'fb7c3ac9-42cb-474f-9baa-de989ed9365b',
                            workflow_state_ssi: 'published',
                            content_media_type_ssim: ['application/pdf'],
                            effective_download_ssim: ['public'])
      end

      before do
        allow(document).to receive(:first_downloadable_pdf_doc).and_return(:pdf_component_doc)
      end

      context 'with no display format' do
        it 'returns true' do
          expect(document.display_inline_pdf?).to be true
        end
      end

      context 'with an image display format' do
        let(:document) do
          described_class.new(id:,
                              display_format_ssi: 'image')
        end

        it 'returns false' do
          expect(document.display_inline_pdf?).to be false
        end
      end
    end

    context 'document does not include a downloadable PDF' do
      before do
        allow(document).to receive(:first_downloadable_pdf_doc).and_return nil
      end

      it 'returns false' do
        expect(document.display_inline_pdf?).to be false
      end
    end
  end

  describe '#sponsor' do
    let(:document) do
      described_class.new('id' => id,
                          'sponsor_tsim' => ['A Sponsor'])
    end

    it 'returns a sponsor' do
      expect(document.sponsor).to eq('A Sponsor')
    end
  end

  describe '#sponsor_display' do
    let(:document) do
      described_class.new('id' => id,
                          'sponsor_tsim' => ['A Sponsor'])
    end
    let(:parent) do
      described_class.new('id' => id,
                          'sponsor_tsim' => ['Parent Sponsor'])
    end
    let(:collection) do
      described_class.new('id' => id,
                          'sponsor_tsim' => ['Collection Sponsor'])
    end

    context 'document has a sponsor' do
      before { allow(document).to receive(:sponsor).and_return('A Sponsor') }

      it 'returns the sponsor' do
        expect(document.sponsor_display).to eq('A Sponsor')
      end
    end

    context 'no document sponsor but parent has a sponsor' do
      before do
        allow(document).to receive(:sponsor).and_return(nil)
        allow(document).to receive(:parent) { parent }
      end

      it 'returns the parent sponsor' do
        expect(document.sponsor_display).to eq('Parent Sponsor')
      end
    end

    context 'no document or parent sponsor but collection has a sponsor' do
      before do
        allow(document).to receive(:sponsor).and_return(nil)
        allow(document).to receive(:parent) { basic_document }
        allow(document).to receive(:collection) { collection }
      end

      it 'returns the collection sponsor' do
        expect(document.sponsor_display).to eq('Collection Sponsor')
      end
    end

    context 'no sponsor' do
      before do
        allow(document).to receive(:sponsor).and_return(nil)
        allow(document).to receive(:parent) { basic_document }
        allow(document).to receive(:collection) { basic_document }
      end

      it 'displays the default sponsor message' do
        expect(document.sponsor_display).to eq('Sponsor this Digital Collection')
      end
    end
  end

  describe '#relation' do
    let(:document) do
      described_class.new('id' => id,
                          'relation_tsim' => ['ark:/87924/r4cn6z979',
                                              'ark:/87924/r4cn6z777'])
    end

    it 'returns relations' do
      expect(document.relation).to eq(['ark:/87924/r4cn6z979',
                                       'ark:/87924/r4cn6z777'])
    end
  end

  describe '#thumbnail' do
    context 'has a thumbnail defined' do
      let(:adviews_doc) do
        described_class.new('id' => id, 'local_id_ssi' => 'adviews')
      end

      before { allow(adviews_doc).to receive(:portal_doc_config) { doc_config } }

      it 'returns the custom thumbnail info' do
        expect(adviews_doc.portal_thumbnail).to eq({ 'custom_image' => 'adviews-thumb.jpg' })
      end
    end

    context 'does not define a thumbnail' do
      let(:adaccess_doc) do
        described_class.new('id' => id, 'local_id_ssi' => 'adaccess_doc')
      end

      before { allow(adaccess_doc).to receive(:portal_doc_config) { doc_config } }

      it 'returns nil' do
        expect(adaccess_doc.portal_thumbnail).to be_nil
      end
    end
  end

  describe '#stream_url' do
    before { allow(basic_document).to receive(:streamable?).and_return(true) }

    xit 'returns a stream path' do
      expect(basic_document.stream_url).to eq('/stream/87567572-b669-48a9-b478-a679725f85e6')
    end
  end

  describe '#captions_url' do
    before { allow(basic_document).to receive(:captioned?).and_return(true) }

    xit 'returns a captions path' do
      expect(basic_document.captions_url).to eq('/captions/87567572-b669-48a9-b478-a679725f85e6')
    end
  end

  describe '#related_items' do
    before { allow(basic_document).to receive(:portal_view_config) { view_config } }

    it 'includes a RelatedItem object' do
      expect(basic_document.related_items.first).to be_a RelatedItem
    end

    it 'has the expected number of objects' do
      expect(basic_document.related_items.count).to eq 3
    end
  end

  describe '#public_controller' do
    context 'has a controller configured' do
      before do
        allow(basic_document).to receive(:effective_configs) { doc_config['collection_local_id']['adaccess'] }
      end

      it 'returns the configured controller name' do
        expect(basic_document.public_controller).to eq('digital_collections')
      end
    end

    context 'does not have a controller configured' do
      before { allow(basic_document).to receive(:effective_configs).and_return({}) }

      it 'returns the default controller name' do
        expect(basic_document.public_controller).to eq('catalog')
      end
    end
  end

  describe '#public_collection' do
    before do
      allow(basic_document).to receive(:effective_configs) { doc_config['collection_local_id']['adaccess'] }
    end

    it 'returns the collection name' do
      expect(basic_document.public_collection).to eq('adaccess')
    end
  end

  describe '#public_action' do
    context 'digital collection object with a local id' do
      let(:adviews_doc) do
        described_class.new('id' => id, 'local_id_ssi' => 'adviews')
      end

      before { allow(adviews_doc).to receive(:dc_collection?).and_return(true) }

      it 'uses the index action' do
        expect(adviews_doc.public_action).to eq('index')
      end
    end

    context 'not a digital collection object' do
      before { allow(basic_document).to receive(:dc_collection?).and_return(false) }

      it 'uses the show action' do
        expect(basic_document.public_action).to eq('show')
      end
    end
  end

  describe '#public_id' do
    context 'digital collection' do
      context 'without a local id' do
        before do
          allow(basic_document).to receive(:dc_collection?).and_return(true)
        end

        it 'does has a public id' do
          expect(basic_document.public_id).to eq(id)
        end
      end

      context 'with a local id' do
        let(:adviews_doc) do
          described_class.new('id' => id, 'local_id_ssi' => 'adviews')
        end

        it 'does not have a public id' do
          allow(adviews_doc).to receive(:dc_collection?).and_return(true)
          expect(adviews_doc.public_id).to be_nil
        end
      end
    end

    context 'digital collection item' do
      context 'with a local id' do
        let(:adviews_doc) do
          described_class.new('id' => id, 'local_id_ssi' => 'adviews')
        end

        before do
          allow(adviews_doc).to receive_messages(dc_collection?: false, dc_item?: true)
        end

        it 'uses the local id' do
          expect(adviews_doc.public_id).to eq('adviews')
        end
      end

      context 'without a local id' do
        before do
          allow(basic_document).to receive_messages(dc_collection?: false, dc_item?: true)
        end

        it 'uses the repository id' do
          expect(basic_document.public_id).to eq(id)
        end
      end
    end

    context 'not a digital collection object' do
      before do
        allow(basic_document).to receive_messages(dc_collection?: false, dc_item?: false)
      end

      it 'uses the repository id' do
        expect(basic_document.public_id).to eq(id)
      end
    end
  end

  describe '#structures' do
    let(:struct_map) do
      YAML.safe_load(file_fixture('structures/default_structure.yml').read)
    end

    before { allow(basic_document).to receive(:structure) { struct_map } }

    it 'is a Structure object' do
      expect(basic_document.structures).to be_a Structure
    end
  end

  describe '#techmd_file_size' do
    let(:document) do
      described_class.new('id' => id,
                          'techmd_file_size_lsi' => ['1233457698889'])
    end

    it 'returns humanized forms of the file size' do
      expect(document.techmd_file_size).to eq(['1.12 TB'])
    end
  end

  describe '#techmd_image_height' do
    let(:document) do
      described_class.new('id' => id,
                          'techmd_image_height_isim' => ['1200'])
    end

    it 'returns humanized forms of the image height' do
      expect(document.techmd_image_height).to eq(['1200px'])
    end
  end

  describe '#techmd_image_width' do
    let(:document) do
      described_class.new('id' => id,
                          'techmd_image_width_isim' => ['900'])
    end

    it 'returns humanized forms of the image width' do
      expect(document.techmd_image_width).to eq(['900px'])
    end
  end

  describe '#document_model' do
    context 'document is a collection' do
      let(:collection_document) do
        described_class.new('id' => id, 'common_model_name_ssi' => 'Collection')
      end

      it 'is an instance of DocumentModel::Collection' do
        expect(collection_document.send(:document_model)).to be_a DocumentModel::Collection
      end
    end

    context 'document is an item' do
      let(:collection_document) do
        described_class.new('id' => id, 'common_model_name_ssi' => 'Item')
      end

      it 'is an instance of DocumentModel::Item' do
        expect(collection_document.send(:document_model)).to be_a DocumentModel::Item
      end
    end

    context 'document is a component' do
      let(:collection_document) do
        described_class.new('id' => id, 'common_model_name_ssi' => 'Component')
      end

      it 'is an instance of DocumentModel::Component' do
        expect(collection_document.send(:document_model)).to be_a DocumentModel::Component
      end
    end
  end

  describe '#display_format_icon' do
    context 'display format is image' do
      let(:document) do
        described_class.new('id' => id, 'display_format_ssi' => 'image')
      end

      it 'uses the clone icon' do
        expect(document.display_format_icon).to eq(%w[far clone])
      end
    end

    context 'display format is folder' do
      let(:document) do
        described_class.new('id' => id, 'display_format_ssi' => 'folder')
      end

      it 'uses the folder icon' do
        expect(document.display_format_icon).to eq(%w[far folder-open])
      end
    end

    context 'display format is video' do
      let(:document) do
        described_class.new('id' => id, 'display_format_ssi' => 'video')
      end

      it 'uses the film icon' do
        expect(document.display_format_icon).to eq(%w[fas film])
      end
    end

    context 'display format is audio' do
      let(:document) do
        described_class.new('id' => id, 'display_format_ssi' => 'audio')
      end

      it 'uses the headphones icon' do
        expect(document.display_format_icon).to eq(%w[fas headphones])
      end
    end

    context 'display format is something else' do
      let(:document) do
        described_class.new('id' => id, 'display_format_ssi' => 'bees')
      end

      it 'uses the file icon' do
        expect(document.display_format_icon).to eq(%w[far file])
      end
    end
  end

  describe '#html_title' do
    before { allow(basic_document).to receive(:document_model) }

    it 'uses the default html title' do
      expect(basic_document.html_title).to eq('Duke Digital Repository')
    end
  end

  describe '#metadata_header' do
    before { allow(basic_document).to receive(:document_model) }

    it 'uses the default metadata header' do
      expect(basic_document.metadata_header).to eq('Item Info')
    end
  end

  describe '#ddr_oai_dc' do
    let(:oai_document) do
      described_class.new(YAML.safe_load(file_fixture('solr_documents/oai_document.yml').read,
                                         permitted_classes: [Symbol]))
    end

    let(:oai_exp) do
      file_fixture('oai_dc/oai_dc_exp.xml').read.chomp
    end

    before do
      allow(oai_document).to receive_messages(first_multires_image_file_path: 'path/to/image', oai_thumbnail: nil)
    end

    it 'returns the expected OAI XML response' do
      expect(oai_document.to_oai_dc).to eq(oai_exp)
    end
  end

  describe '#file_extension' do
    subject { described_class.new.file_extension(media_type) }

    describe 'with a media type that has a preferred extension' do
      let(:media_type) { 'audio/mpeg' }

      it { is_expected.to eq 'mp3' }
    end

    describe 'with a media type that does not have a preferred extension' do
      let(:media_type) { 'image/jpeg' }

      it { is_expected.to eq 'jpeg' }
    end
  end

  describe '#default_file_extension' do
    subject { described_class.new.default_file_extension(media_type) }

    describe 'with a registered media type' do
      let(:media_type) { 'image/jpeg' }

      it { is_expected.to eq 'jpeg' }
    end

    describe 'with an unregistered media type' do
      let(:media_type) { 'foo/bar' }

      it { is_expected.to eq 'bin' }
    end
  end
end
