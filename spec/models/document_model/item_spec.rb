# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DocumentModel::Item do
  subject(:item_model) { described_class.new(document: item) }

  let(:collection) do
    SolrDocument.new(YAML.safe_load(file_fixture('solr_documents/collection.yml').read, permitted_classes: [Symbol]))
  end
  let(:item) do
    SolrDocument.new(YAML.safe_load(file_fixture('solr_documents/item.yml').read, permitted_classes: [Symbol]))
  end
  let(:component) do
    SolrDocument.new(YAML.safe_load(file_fixture('solr_documents/component.yml').read, permitted_classes: [Symbol]))
  end

  it 'is an instance of DocumentModel::Item' do
    expect(item_model).to be_a described_class
  end

  it 'knows about its parent collection' do
    allow(item_model).to receive(:collection_search) { double('Collection Search', documents: [collection]) }
    expect(item_model.collection).to eq(collection)
  end

  it 'knows about its child components' do
    allow(item_model).to receive(:component_search) { double('Component Search', documents: [component]) }
    expect(item_model.components).to eq([component])
  end

  it 'knows how many child components it has' do
    allow(item_model).to receive(:component_search) { double('Component Search', total: 1) }
    expect(item_model.component_count).to eq(1)
  end

  it 'uses the folder metadata header if it is a folder' do
    expect(item_model.metadata_header).to eq('Folder Info')
  end

  it 'uses the html title for items' do
    allow(item_model).to receive(:html_title_qualifier).and_return('')
    allow(item_model).to receive(:collection) { collection }
    expect(item_model.html_title).to(
      eq('This is a bee item / This is a collection of bees / Duke Digital Repository')
    )
  end
end
