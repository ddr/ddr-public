# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DocumentModel::Collection do
  subject(:collection_model) { described_class.new(document: collection) }

  let(:collection) do
    SolrDocument.new(YAML.safe_load(file_fixture('solr_documents/collection.yml').read, permitted_classes: [Symbol]))
  end
  let(:item) do
    SolrDocument.new(YAML.safe_load(file_fixture('solr_documents/item.yml').read, permitted_classes: [Symbol]))
  end

  it 'is an instance of DocumentModel::Collection' do
    expect(collection_model).to be_a described_class
  end

  it 'collection that refers to itself' do
    expect(collection_model.collection).to eq(collection)
  end

  it 'knows about its children items' do
    allow(collection_model).to receive(:items_search) { double('Items Search', documents: [item]) }
    expect(collection_model.items).to eq([item])
  end

  it 'knows how many items it has' do
    allow(collection_model).to receive(:items_search) { double('Items Search', total: 1) }
    expect(collection_model.item_count).to eq(1)
  end

  it 'uses the metadata header for collections' do
    expect(collection_model.metadata_header).to eq('Collection Info')
  end

  it 'uses the html title for collections' do
    expect(collection_model.html_title).to(
      eq('This is a collection of bees / The Bee Keepers / Duke Digital Repository')
    )
  end
end
