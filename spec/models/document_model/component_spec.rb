# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DocumentModel::Component do
  subject(:component_model) { described_class.new(document: component) }

  let(:collection) do
    SolrDocument.new(YAML.safe_load(file_fixture('solr_documents/collection.yml').read, permitted_classes: [Symbol]))
  end
  let(:item) do
    SolrDocument.new(YAML.safe_load(file_fixture('solr_documents/item.yml').read, permitted_classes: [Symbol]))
  end
  let(:component) do
    SolrDocument.new(YAML.safe_load(file_fixture('solr_documents/component.yml').read, permitted_classes: [Symbol]))
  end

  it 'is an instance of DocumentModel::Component' do
    expect(component_model).to be_a described_class
  end

  it 'knows about its parent collection' do
    allow(component_model).to receive(:collection_search) { double('Collection Search', documents: [collection]) }
    expect(component_model.collection).to eq(collection)
  end

  it 'knows about its parent item' do
    allow(component_model).to receive(:item_search) { double('Item Search', documents: [item]) }
    expect(component_model.item).to eq(item)
  end

  it 'uses the metadata header for components' do
    expect(component_model.metadata_header).to eq('File Info')
  end

  it 'uses the html title for components' do
    allow(component_model).to receive(:html_title_qualifier).and_return('')
    allow(component_model).to receive(:item) { item }
    expect(component_model.html_title).to(
      eq('This is a component of bees / This is a bee item / Duke Digital Repository')
    )
  end
end
