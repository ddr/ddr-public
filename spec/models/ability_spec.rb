# frozen_string_literal: true

require 'rails_helper'
require 'cancan/matchers'

RSpec.describe Ability do
  subject(:ability) { described_class.new(user) }

  let(:user) { nil }
  let(:doc) { SolrDocument.new({ 'id' => doc_id, 'workflow_state_ssi' => 'published' }.merge(doc_attrs)) }
  let(:doc_id) { SecureRandom.uuid }
  let(:doc_attrs) { {} }

  before do
    allow(SolrDocument).to receive(:find).with(doc_id) { doc }
  end

  describe 'anonymous' do
    its(:superuser?) { is_expected.to be false }
  end

  describe 'authenticated, not superuser' do
    let(:user) { create(:user) }

    its(:superuser?) { is_expected.to be false }
  end

  describe 'authenticated, superuser' do
    let(:user) { create(:user, :superuser) }

    its(:superuser?) { is_expected.to be true }
  end

  describe 'permissions' do
    Ability::PERMISSIONS.each do |permission|
      describe permission.to_s do
        let(:doc_attrs) { { Ability::INDEX_FIELDS[permission] => index_value } }
        let(:index_value) { [] }

        describe 'anonyous permission' do
          it { is_expected.not_to be_able_to(permission, doc) }
          it { is_expected.not_to be_able_to(permission, doc_id) }
        end

        describe "'public' is listed in #{permission} index field" do
          let(:index_value) { ['public'] }

          it { is_expected.to be_able_to(permission, doc) }
          it { is_expected.to be_able_to(permission, doc_id) }
        end

        describe "authenticated user is listed in #{permission} index field" do
          let(:user) { create(:user) }
          let(:index_value) { [user.agent] }

          it "can #{permission} the document" do
            expect(subject).to be_able_to(permission, doc)
          end

          it { is_expected.to be_able_to(permission, doc_id) }
        end

        describe "authenticated user and other group is listed in #{permission} index field" do
          let(:index_value) { ['foobar'] }

          describe 'and the user is a member of the group' do
            let(:user) { build(:user, groups: ['foobar']) }

            it { is_expected.to be_able_to(permission, doc) }
            it { is_expected.to be_able_to(permission, doc_id) }
          end

          describe 'and user is not a member of the group' do
            let(:user) { build(:user) }

            it { is_expected.not_to be_able_to(permission, doc) }
            it { is_expected.not_to be_able_to(permission, doc_id) }
          end
        end

        describe 'embargoed resource' do
          let(:index_value) { ['public'] }

          before do
            doc_attrs.merge! 'embargo_dtsi' => '2035-04-30T05:00:00.000+00:00'
          end

          if permission == :discover
            it { is_expected.to be_able_to(permission, doc) }
            it { is_expected.to be_able_to(permission, doc_id) }
          else
            it { is_expected.not_to be_able_to(permission, doc) }
            it { is_expected.not_to be_able_to(permission, doc_id) }
          end
        end
      end
    end
  end

  describe 'file permissions' do
    Ability::FILE_PERMISSIONS.reject { |p| p == :download_content }.each do |permission|
      describe permission.to_s do
        case permission
        when :download_content
          describe 'can read, download doc' do
            before { ability.can %i[read download], doc }

            it { is_expected.to be_able_to(permission, doc) }
            it { is_expected.to be_able_to(permission, doc_id) }
          end

          describe 'can read, cannot download doc' do
            before do
              ability.can :read, doc
              ability.cannot :download, doc
            end

            it { is_expected.not_to be_able_to(permission, doc) }
            it { is_expected.not_to be_able_to(permission, doc_id) }
          end

        when :download_iiif_file
          describe 'can discover doc' do
            before { ability.can :discover, doc }

            it { is_expected.to be_able_to(permission, doc) }
            it { is_expected.to be_able_to(permission, doc_id) }
          end
          describe 'cannot discover doc' do
            before { ability.cannot :discover, doc }

            it { is_expected.not_to be_able_to(permission, doc) }
            it { is_expected.not_to be_able_to(permission, doc_id) }
          end

        else
          describe 'can read doc' do
            before { ability.can :read, doc }

            it { is_expected.to be_able_to(permission, doc) }
            it { is_expected.to be_able_to(permission, doc_id) }
          end

          describe 'cannot read doc' do
            before { ability.cannot :read, doc }

            it { is_expected.not_to be_able_to(permission, doc) }
            it { is_expected.not_to be_able_to(permission, doc_id) }
          end
        end
      end
    end
  end
end
