# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Structure::Group do
  subject { described_class.new({ structure:, type: 'Images' }) }

  let(:structure) do
    YAML.safe_load(file_fixture('structures/group_struct.yml').read)
  end
  let(:ids_exp) do
    YAML.safe_load(file_fixture('structures/group_struct_ids_exp.yml').read)
  end
  let(:doc_list_exp) do
    YAML.safe_load(file_fixture('structures/group_struct_doclist_exp.yml').read, permitted_classes: [Symbol])
  end
  let(:doc_list_nil_exp) do
    YAML.safe_load(file_fixture('structures/group_struct_nildoclist_exp.yml').read, permitted_classes: [Symbol])
  end

  its(:ids) { is_expected.to eq(ids_exp) }
  its(:label) { is_expected.to eq('Multires Images') }
  its(:labels) { is_expected.to eq([nil, 'Special Image', nil]) }
  its(:id) { is_expected.to eq('dukechapel_dcrst003606-images') }

  context 'all documents are present' do
    let(:document_ids) { ids_exp }

    before do
      double('ordered_documents')
      allow(subject).to receive(:ids) { document_ids }
      allow(subject).to receive(:ordered_documents).with(document_ids).and_return(document_ids)
    end

    it 'has a list of all the documents' do
      expect(subject.docs).to eq(ids_exp)
    end

    it 'has a list of all the documents with labels and order values' do
      expect(subject.docs_list).to eq(doc_list_exp)
    end
  end

  context 'some of the documents are nil' do
    let(:document_ids) { ['c8b0ddf1-812d-483e-a47b-c1b91b03d044', nil, '8a2b6821-209e-4800-8de3-7cca33cbfdfc'] }

    before do
      double('ordered_documents')
      allow(subject).to receive(:ids) { document_ids }
      allow(subject).to receive(:ordered_documents).with(document_ids).and_return(document_ids)
    end

    it 'has a list of all non-nil documents' do
      expect(subject.docs).to eq(%w[c8b0ddf1-812d-483e-a47b-c1b91b03d044 8a2b6821-209e-4800-8de3-7cca33cbfdfc])
    end

    it 'has a list of all non-nil documents with labels and order values' do
      expect(subject.docs_list).to eq(doc_list_nil_exp)
    end
  end
end
