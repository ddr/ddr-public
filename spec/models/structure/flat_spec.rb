# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Structure::Flat do
  subject { described_class.new({ structure:, type: 'default' }) }

  let(:structure) do
    YAML.safe_load(file_fixture('structures/flat_struct.yml').read)
  end
  let(:ids_exp) do
    YAML.safe_load(file_fixture('structures/flat_struct_ids_exp.yml').read)
  end
  let(:doc_list_exp) do
    YAML.safe_load(file_fixture('structures/flat_struct_doclist_exp.yml').read, permitted_classes: [Symbol])
  end
  let(:doc_list_nil_exp) do
    YAML.safe_load(file_fixture('structures/flat_struct_nildoclist_exp.yml').read, permitted_classes: [Symbol])
  end

  its(:ids) { is_expected.to eq(ids_exp) }
  its(:label) { is_expected.to be_nil }
  its(:labels) { is_expected.to eq([nil, nil, 'Special Thing', nil]) }
  its(:order) { is_expected.to eq(%w[1 2 3 4]) }

  context 'all documents are present' do
    let(:document_ids) { ids_exp }

    before do
      double('ordered_documents')
      allow(subject).to receive(:ids) { document_ids }
      allow(subject).to receive(:ordered_documents).with(document_ids).and_return(document_ids)
    end

    it 'has a list of all the documents' do
      expect(subject.docs).to eq(ids_exp)
    end

    it 'has a list of all the documents with labels and order values' do
      expect(subject.docs_list).to eq(doc_list_exp)
    end
  end

  context 'some of the documents are nil' do
    let(:document_ids) do
      ['9d547f74-7014-4ffd-b6dc-584898acad16',
       nil,
       '2d7c3bd2-8993-44ea-a4d5-96e84bf3fe34',
       nil]
    end

    before do
      double('ordered_documents')
      allow(subject).to receive(:ids) { document_ids }
      allow(subject).to receive(:ordered_documents).with(document_ids).and_return(document_ids)
    end

    it 'has a list of all non-nil documents' do
      expect(subject.docs).to(
        eq(%w[9d547f74-7014-4ffd-b6dc-584898acad16
              2d7c3bd2-8993-44ea-a4d5-96e84bf3fe34])
      )
    end

    it 'has a list of all non-nil documents with labels and order values' do
      expect(subject.docs_list).to eq(doc_list_nil_exp)
    end
  end
end
