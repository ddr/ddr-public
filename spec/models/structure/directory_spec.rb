# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Structure::Directory do
  subject { described_class.new({ structure: }) }

  let(:structure) do
    YAML.safe_load(file_fixture('structures/dir_struct.yml').read)
  end
  let(:item_id_lookup_expectation) do
    YAML.safe_load(file_fixture('structures/dir_struct_item_id_exp.yml').read, permitted_classes: [Symbol])
  end
  let(:directory_id_lookup_expectation) do
    YAML.safe_load(file_fixture('structures/dir_struct_dir_id_exp.yml').read, permitted_classes: [Symbol])
  end

  its(:item_id_lookup) { is_expected.to eq(item_id_lookup_expectation) }
  its(:directory_id_lookup) { is_expected.to eq(directory_id_lookup_expectation) }
end
