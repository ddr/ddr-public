# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User do
  subject { described_class.new(username:, email:) }

  let(:username) { 'foobar@duke.edu' }
  let(:email) { 'foo.bar.baz@duke.edu' }

  its(:to_s) { is_expected.to eq username }
  its(:agent) { is_expected.to eq username }
  its(:groups) { is_expected.to eq [] }

  describe '.from_omniauth' do
    subject { described_class.from_omniauth(auth) }

    let(:auth) do
      OmniAuth::AuthHash.new(
        uid: username,
        info: { email: },
        extra: {
          raw_info: {
            dukePrimaryAffiliation: 'staff',
            groups: ['staff']
          }
        }
      )
    end

    describe 'with an existing user' do
      before do
        @user = described_class.create!(username:, email:)
      end

      it { is_expected.to be_a described_class }
      its(:id) { is_expected.to eq @user.id }
      its(:username) { is_expected.to eq username }
      its(:email) { is_expected.to eq email }
      its(:groups) { is_expected.to contain_exactly('duke.staff', 'duke:policies:duke-digital-repository:staff') }
    end

    describe 'without an existing user' do
      it { is_expected.to be_a described_class }
      it { is_expected.to be_persisted }
      its(:username) { is_expected.to eq username }
      its(:email) { is_expected.to eq email }
      its(:groups) { is_expected.to contain_exactly('duke.staff', 'duke:policies:duke-digital-repository:staff') }
    end
  end
end
