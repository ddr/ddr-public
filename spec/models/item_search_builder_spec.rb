# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ItemSearchBuilder do
  subject { search_builder_class.new(processor_chain, {}) }

  let(:search_builder_class) do
    Class.new(described_class)
  end

  let(:processor_chain) { [] }

  describe '#processor_chain' do
    let(:sb) { search_builder_class.new(ItemsController.new) }

    it 'adds the include_only_items to the processor chain' do
      expect(sb.processor_chain).to include(:include_only_items)
    end

    it 'removes facetting from the processor chain' do
      expect(sb.processor_chain).not_to include(:add_facetting_to_solr)
    end

    it 'removes BL range limit stats from the processor chain' do
      expect(sb.processor_chain).not_to include(:add_range_limit_params)
    end
  end
end
