# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Thumbnail::RepositoryGenerated do
  describe 'multires item thumbnail' do
    let(:id) { SecureRandom.uuid }

    it 'has a repository thumbnail' do
      document = double('Document')
      allow(document).to receive(:thumbnail?).and_return(true)
      custom = described_class.new({ document: })
      expect(custom.thumbnail?).to be true
    end

    it 'has a repository thumbnail path' do
      document = double('Document')
      allow(document).to receive(:thumbnail?).and_return(true)
      allow(document).to receive(:id) { id }
      custom = described_class.new({ document: })
      expect(custom.thumbnail_path).to eq "/download/#{id}/thumbnail"
    end

    it 'does not have a repository thumbnail' do
      document = double('Document')
      allow(document).to receive(:thumbnail?).and_return(false)
      custom = described_class.new({ document: })
      expect(custom.thumbnail?).to be false
    end
  end
end
