# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Thumbnail::CustomCollection do
  describe 'custom thumbnail' do
    let(:document) do
      SolrDocument.new(id: SecureRandom.uuid,
                       common_model_name_ssi: 'Collection',
                       local_id_ssi: 'dukechapel')
    end

    it 'has a thumbnail if configured' do
      allow(document).to receive(:portal_thumbnail).and_return({ 'custom_image' => 'custom.jpg' })
      custom = described_class.new({ document: })
      expect(custom.thumbnail?).to be true
    end

    it 'has a thumbnail path if configured' do
      allow(document).to receive(:portal_thumbnail).and_return({ 'custom_image' => 'custom.jpg' })
      custom = described_class.new({ document: })
      expect(custom.thumbnail_path).to eq 'ddr-portals/dukechapel/custom.jpg'
    end

    it 'does not have a thumbnail if not configured' do
      allow(document).to receive(:portal_thumbnail).and_return(nil)
      custom = described_class.new({ document: })
      expect(custom.thumbnail?).to be false
    end
  end
end
