# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Thumbnail::AudiovisualItem do
  let(:id) { SecureRandom.uuid }
  let(:av_component_doc) do
    SolrDocument.new({
                       'id' => id,
                       'common_model_name_ssi' => 'Component'
                     })
  end

  describe 'audiovisual item thumbnail' do
    it "doesn't have display format set to audio or video" do
      document = double('Document')
      allow(document).to receive(:display_format_ssi).and_return('image')
      avthumb = described_class.new({ document: })
      expect(avthumb.thumbnail?).to be false
    end

    it 'has video display format and a media component with a repo thumb' do
      document = double('Document')
      allow(document).to receive(:display_format_ssi).and_return('video')
      allow(document).to receive(:first_media_doc) { av_component_doc }
      allow(av_component_doc).to receive(:thumbnail?).and_return(true)
      avthumb = described_class.new({ document: })
      expect(avthumb.thumbnail?).to be true
    end

    it 'has video display format but media component lacks a repo thumb' do
      document = double('Document')
      allow(document).to receive(:display_format_ssi).and_return('video')
      allow(document).to receive(:first_media_doc) { av_component_doc }
      allow(av_component_doc).to receive(:thumbnail?).and_return(false)
      avthumb = described_class.new({ document: })
      expect(avthumb.thumbnail?).to be false
    end

    it 'has audio or video display format and its first component has a repo thumb' do
      document = double('Document')
      allow(document).to receive(:first_media_doc) { av_component_doc }
      avthumb = described_class.new({ document: })
      allow(avthumb).to receive(:thumbnail?).and_return(true)
      expect(avthumb.thumbnail_path).to eq "/download/#{id}/thumbnail"
    end
  end
end
