# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Thumbnail::Default do
  let(:basic_document) do
    SolrDocument.new({
                       'id' => 'changeme:1',
                       'display_format_ssi' => 'none',
                       'common_model_name_ssi' => 'Item'
                     })
  end

  let(:multi_field_document) do
    SolrDocument.new({
                       'id' => 'changeme:1',
                       'display_format_ssi' => 'video',
                       'common_model_name_ssi' => 'Collection'
                     })
  end

  before { allow(basic_document).to receive(:components).and_return([]) }

  it 'has a thumbnail for any document' do
    default = described_class.new({ document: basic_document })
    expect(default.thumbnail?).to be true
  end

  it 'has a thumbnail path for any document' do
    default = described_class.new({ document: basic_document })
    expect(default.thumbnail_path).to end_with 'ddr-icons/default.png'
  end

  it 'favors display format over model' do
    new_document = described_class.new({ document: multi_field_document })
    expect(new_document.thumbnail_path).to end_with 'ddr-icons/video.png'
  end
end
