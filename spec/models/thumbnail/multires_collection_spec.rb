# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Thumbnail::MultiresCollection do
  describe 'multires collection thumbnail' do
    let(:document) do
      SolrDocument.new(id: SecureRandom.uuid,
                       common_model_name_ssi: 'Collection')
    end

    it 'does not have a thumbnail if not configured' do
      allow(document).to receive_messages(thumbnail: {}, items: nil)
      multires_collection = described_class.new({ document: })
      expect(multires_collection.thumbnail?).to be false
    end
  end
end
