# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Thumbnail::MultiresItem do
  describe 'multires item thumbnail' do
    let(:document) do
      SolrDocument.new(id: SecureRandom.uuid,
                       common_model_name_ssi: 'Item')
    end

    it 'has a multires image', skip: 'DDR-2032' do
      allow(document).to receive(:first_multires_image_file_path).and_return('/path/to/image/')
      custom = described_class.new({ document: })
      expect(custom.thumbnail?).to be true
    end

    it 'has a multires image path', skip: 'DDR-2032' do
      allow(document).to receive(:first_multires_image_file_path).and_return('/path/to/image')
      custom = described_class.new({ document: })
      expect(custom.thumbnail_path).to include '=/path/to/image/full/!350,350/0/default.jpg'
    end

    it 'does not have a multires image', skip: 'DDR-2032' do
      allow(document).to receive(:first_multires_image_file_path).and_return(nil)
      custom = described_class.new({ document: })
      expect(custom.thumbnail?).to be false
    end
  end
end
