# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Thumbnail::MultiresComponent do
  describe 'multires component thumbnail' do
    let(:document_with_image) do
      SolrDocument.new(id: SecureRandom.uuid,
                       common_model_name_ssi: 'Component',
                       multires_image_file_path_ssi: '/path/to/image')
    end

    let(:document_without_image) do
      SolrDocument.new(id: SecureRandom.uuid,
                       common_model_name_ssi: 'Component',
                       multires_image_file_path_ssi: '')
    end

    it 'has a multires image', skip: 'DDR-2032' do
      custom = described_class.new({ document: document_with_image })
      expect(custom.thumbnail?).to be true
    end

    it 'has a multires image path', skip: 'DDR-2032' do
      custom = described_class.new({ document: document_with_image })
      expect(custom.thumbnail_path).to include '=/path/to/image/full/!350,350/0/default.jpg'
    end

    it 'does not have a multires image' do
      custom = described_class.new({ document: document_without_image })
      expect(custom.thumbnail?).to be false
    end
  end
end
