require 'rails_helper'

RSpec.describe DdrFile, type: :model do
  let(:data) { {file_identifier:, media_type:, original_filename:} }
  let(:media_type) { 'image/jpeg' }
  let(:original_filename) { 'orange.jpg' }

  subject { described_class.new(data) }

  describe 'disk file' do
    let(:file_identifier) { {'id'=>'disk:///foo/bar'} }

    its(:disk?) { is_expected.to be true}
    its(:s3?) { is_expected.to be false }
    its(:null?) { is_expected.to be false }

    describe 'non-text media type' do
      let(:media_type) { 'application/pdf' }
      let(:file_identifier) { {'id'=>'disk://%s' % Rails.root.join('spec/fixtures/files/pdf/dul_reading_devil.pdf')} }
      let(:original_filename) { 'dul_reading_devil.pdf' }

      its(:text?) { is_expected.to be false }

      it 'outputs in binary encoding' do
        expect(subject.read.encoding.to_s).to eq 'ASCII-8BIT'
      end
    end

    describe 'text media type' do
      let(:media_type) { 'text/vtt' }
      let(:file_identifier) { {'id'=>'disk://%s' % Rails.root.join('spec/fixtures/files/vtt/abcd1234.vtt')} }
      let(:original_filename) { 'abcd1234.vtt' }

      its(:text?) { is_expected.to be true }

      it 'outputs in UTF-8 encoding' do
        expect(subject.read.encoding.to_s).to eq 'UTF-8'
      end
    end
  end

  describe 'S3 file' do
    let(:file_identifier) { {'id'=>'s3://foo/bar'} }

    its(:disk?) { is_expected.to be false }
    its(:s3?) { is_expected.to be true }

    # TODO: encoding - see disk file tests, above.
  end

  describe 'null file' do
    let(:data) { {} }

    its(:disk?) { is_expected.to be false }
    its(:s3?) { is_expected.to be false }
    its(:null?) { is_expected.to be true }
    its(:text?) { is_expected.to be false }
  end
end
