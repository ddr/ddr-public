# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'shared/_flash_messages.html.erb' do
  before do
    allow(Ddr::Public).to receive(:alert_message).and_return('An alert.')
  end

  it 'includes the active message(s)' do
    render 'shared/flash_messages'
    expect(rendered).to match(/An alert\./)
  end
end
