# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BlacklightHelper do
  describe '#document_or_object_url' do
    let(:doc_or_obj) do
      SolrDocument.new(
        'id' => 'changeme:2'
      )
    end

    before { allow(doc_or_obj).to receive(:public_action).and_return('show') }

    context 'document has a public_collection defined' do
      it 'returns a collection scoped url to a document' do
        allow(doc_or_obj).to receive_messages(public_controller: 'digital_collections', public_collection: 'wdukesons',
                                              public_id: 'dcci0101')
        expect(helper.document_or_object_url(doc_or_obj)).to include(url_for(controller: 'digital_collections',
                                                                             collection: 'wdukesons', action: 'show', id: 'dcci0101'))
      end
    end

    context 'document does not have a public_collection defined' do
      it 'returns a url for an show view without a collection path' do
        allow(doc_or_obj).to receive_messages(public_controller: 'catalog', public_collection: nil,
                                              public_id: 'changeme:2')
        expect(helper.document_or_object_url(doc_or_obj)).to include(url_for(controller: 'catalog', action: 'show',
                                                                             id: 'changeme:2'))
      end
    end
  end

  describe '#document_index_view_type' do
    it 'defaults to the default view' do
      allow(helper).to receive_messages(document_index_views: { a: 1, b: 2 }, default_document_index_view_type: :xyz)
      expect(helper.document_index_view_type).to eq :xyz
    end

    it 'uses the query parameter' do
      allow(helper).to receive(:document_index_views).and_return(a: 1, b: 2)
      expect(helper.document_index_view_type(view: :a)).to eq :a
    end

    it 'uses the default view if the requested view is not available' do
      allow(helper).to receive_messages(default_document_index_view_type: :xyz, document_index_views: { a: 1, b: 2 })
      expect(helper.document_index_view_type(view: :c)).to eq :xyz
    end

    context 'when they have a preferred view' do
      before do
        session[:preferred_view] = :b
      end

      context 'and no view is specified' do
        it 'uses the saved preference' do
          allow(helper).to receive(:document_index_views).and_return(a: 1, b: 2, c: 3)
          expect(helper.document_index_view_type).to eq :b
        end

        it 'uses the default view if the preference is not available' do
          allow(helper).to receive(:document_index_views).and_return(a: 1)
          expect(helper.document_index_view_type).to eq :a
        end
      end

      context 'and a view is specified' do
        it 'uses the query parameter' do
          allow(helper).to receive(:document_index_views).and_return(a: 1, b: 2, c: 3)
          expect(helper.document_index_view_type(view: :c)).to eq :c
        end
      end
    end

    context 'when the documents are rendered in a show view' do
      before do
        allow(controller).to receive(:action_name).and_return('show')
      end

      context 'and the list view is available' do
        it 'uses the list view' do
          allow(helper).to receive(:document_index_views).and_return(list: 1)
          expect(helper.document_index_view_type).to eq :list
        end
      end

      context 'and the list view is not available' do
        it 'uses the default view' do
          allow(helper).to receive(:document_index_views).and_return(a: 1, b: 2, c: 3)
          expect(helper.document_index_view_type).to eq :a
        end
      end
    end
  end
end
