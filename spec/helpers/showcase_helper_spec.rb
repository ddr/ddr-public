# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ShowcaseHelper do
  describe '#display_collection_showcase?' do
    let(:portal) { double('portal') }
    let(:current_ability) { double('current_ability') }

    before do
      allow(helper).to receive(:user_signed_in?).and_return(true)
      allow(helper).to receive(:current_ability) { double(user: 'current_user') }
    end

    context 'portal has custom images' do
      it 'returns true' do
        allow(portal).to receive(:collection) { double(id: 'first_id') }
        allow(portal).to receive(:showcase_custom_images).and_return(%w[one two])
        expect(helper.display_collection_showcase?({ portal: })).to be true
      end
    end

    context 'portal has showcase documents' do
      it 'returns true' do
        allow(portal).to receive(:collection) { double(id: 'second_id') }
        allow(portal).to receive(:showcase_custom_images).and_return([])
        allow(portal).to receive(:showcase) { double(documents: %w[one two]) }
        expect(helper.display_collection_showcase?({ portal: })).to be true
      end
    end

    context 'portal has neither showcase documents nor custom images' do
      it 'returns false' do
        allow(portal).to receive(:collection) { double(id: 'third_id') }
        allow(portal).to receive(:showcase_custom_images).and_return([])
        allow(portal).to receive(:showcase) { double(documents: []) }
        expect(helper.display_collection_showcase?({ portal: })).to be false
      end
    end
  end

  describe '#shuffle_and_select_showcase_images' do
    let(:images) do
      %w[one two three four five]
    end

    context 'user is signed in' do
      it 'returns one image if the count is one (default)' do
        allow(helper).to receive(:user_signed_in?).and_return(true)
        expect(helper.shuffle_and_select_showcase_images(images:).count).to eq(1)
      end
    end

    context 'user is not signed in' do
      before do
        allow(helper).to receive(:user_signed_in?).and_return(false)
      end

      it 'returns one image if count is one (default)' do
        expect(helper.shuffle_and_select_showcase_images(images:).count).to eq(1)
      end

      it 'returns three images if the count is three' do
        expect(helper.shuffle_and_select_showcase_images(images:, count: 3).count).to eq(3)
      end
    end
  end

  describe '#home_showcase_locals' do
    context 'the image is a filename' do
      let(:options) { { index: 1, image: 'chapel.png', showcase_layout: 'landscape' } }

      it 'returns a hash with a path to the image' do
        expect(helper.home_showcase_locals(options)).to eq({ index: 1, image: 'chapel.png',
                                                             size_class: 'col-md-8 col-sm-12' })
      end
    end

    context 'the image is a document' do
      let(:document) { SolrDocument.new({ 'id' => 'changeme:10' }) }
      let(:options) do
        { index: 1,
          image: document,
          showcase_layout: 'portrait' }
      end

      it 'returns a hash with a multires item document' do
        expect(helper.home_showcase_locals(options)).to eq({ index: 1, image: document, size_class: 'col-md-4 col-sm-6',
                                                             size: '600,', region: 'pct:5,5,90,90', caption_length: 50 })
      end
    end
  end
end
