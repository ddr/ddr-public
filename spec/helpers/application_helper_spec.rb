# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationHelper do
  describe '#has_search_parameters?' do
    context 'params contains a search parameter' do
      params = { q: 'foo' }
      it 'is true' do
        allow(helper).to receive(:params).and_return(params)
        expect(helper).to have_search_parameters
      end
    end

    context 'params does not contain a search parameter' do
      params = {}
      it 'is false' do
        allow(helper).to receive(:params).and_return(params)
        expect(helper).not_to have_search_parameters
      end
    end
  end

  describe '#research_help_contact' do
    context 'code exists' do
      it 'returns the name of the contact' do
        expect(helper.research_help_contact('rubenstein')).to eq('David M. Rubenstein Rare Book & Manuscript Library')
      end
    end

    context 'code does not exist' do
      it 'returns the code' do
        expect(helper.research_help_contact('brb')).to eq('brb')
      end
    end
  end
end
