# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MultiresImageHelper do
  describe '#image_export_button' do
    let(:doc) do
      instance_double(SolrDocument, id: SecureRandom.uuid)
    end

    it 'returns the PDF button markup' do
      expect(helper.image_export_button(doc, { label: 'PDF', format: 'pdf' }))
        .to match(%r{^<form action="/export_pdf/\h+})
        .and match(/<button name="button" type="submit" id="image-export-pdf"/)
    end

    it 'returns the ZIP button markup' do
      expect(helper.image_export_button(doc, { label: 'ZIP', format: 'zip' }))
        .to match(%r{^<form action="/export_zip/\h+})
        .and match(/<button name="button" type="submit" id="image-export-zip"/)
    end
  end

  describe '#image_item_aspectratio' do
    let(:parent_id) { SecureRandom.uuid }
    let(:child_doc) do
      SolrDocument.new('id' => SecureRandom.uuid,
                       'common_model_name_ssi' => 'Component',
                       'workflow_state_ssi' => 'published',
                       'multires_image_file_path_ssi' => '/data/test_ptif',
                       'techmd_image_height_isim' => [2000],
                       'techmd_image_width_isim' => [1000],
                       'parent_id_ssim' => "id-#{parent_id}")
    end
    let(:parent_doc) do
      SolrDocument.new('id' => parent_id)
    end

    before do
      allow(helper).to receive(:image_item_first_component_doc) { child_doc }
    end

    specify do
      expect(helper.image_item_aspectratio(parent_doc)).to eq 0.5
    end
  end
end
