# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EmbedHelper do
  describe '#embeddable?' do
    let(:document) { SolrDocument.new({ 'id' => 'changeme:10' }) }

    params = { controller: 'digital_collections' }
    context 'the digital collections item is an image' do
      it 'is embeddable' do
        document = double('document', display_format_ssi: 'image')
        allow(helper).to receive(:params).and_return(params)
        expect(helper.embeddable?(document)).to be true
      end
    end

    context 'the digital collections item has no display format' do
      it 'is not embeddable' do
        document = double('document', display_format_ssi: nil)
        allow(helper).to receive(:params).and_return(params)
        expect(helper.embeddable?(document)).to be false
      end
    end
  end

  describe 'purl_or_doclink' do
    let(:document) { SolrDocument.new({ 'id' => 'changeme:10' }) }

    context 'item has a permalink' do
      it 'returns its permalink' do
        document = double('document', permanent_url_ssi: 'http://idn.duke.edu/ark:/99999/fk4yyyyy')
        expect(helper.purl_or_doclink(document)).to eq('http://idn.duke.edu/ark:/99999/fk4yyyyy')
      end
    end

    context "item doesn't have a permalink" do
      it 'returns its full path' do
        document = double('document', permanent_url_ssi: nil)
        stub_const('ENV', { 'ROOT_URL' => 'https://repository.duke.edu' })
        allow(helper).to receive(:document_or_object_url).and_return('/catalog/changeme:10')
        expect(helper.purl_or_doclink(document)).to include('https://repository.duke.edu/catalog/changeme:10')
      end
    end
  end

  describe 'iframe_src_path' do
    let(:document) { SolrDocument.new({ 'id' => 'changeme:10' }) }

    context 'item has a permalink' do
      it 'returns an absolute URL to embedded view' do
        document = double('document', permanent_url_ssi: 'https://idn.duke.edu/ark:/99999/fk4yyyyy')
        expect(helper.iframe_src_path(document)).to eq('https://idn.duke.edu/ark:/99999/fk4yyyyy?embed=true')
      end
    end
  end

  describe '#has_caption_or_transcript?' do
    let(:item_doc) do
      SolrDocument.new(
        id: SecureRandom.uuid,
        'common_model_name_ssi' => 'Item',
        'display_format_ssi' => 'audio'
      )
    end
    let(:pdf_component_transcript_format) do
      SolrDocument.new(
        id: SecureRandom.uuid,
        'common_model_name_ssi' => 'Component',
        'title_ssi' => 'abc123.pdf',
        format_tsim: 'My transcript'
      )
    end
    let(:pdf_component_transcript_title) do
      SolrDocument.new(
        id: SecureRandom.uuid,
        'common_model_name_ssi' => 'Component',
        'title_ssi' => 'english-transcript-full.pdf'
      )
    end
    let(:pdf_component_misc) do
      SolrDocument.new(
        id: SecureRandom.uuid,
        'common_model_name_ssi' => 'Component',
        'title_ssi' => 'abc123.pdf'
      )
    end

    context 'item has neither captions nor transcript docs' do
      it "doesn't indicate caption/transcript" do
        allow(item_doc).to receive_message_chain('structures.files') do
          [{ doc: pdf_component_misc, label: nil, order: '1' }]
        end
        allow(item_doc).to receive(:captions_urls).and_return(nil)
        expect(helper.has_caption_or_transcript?(item_doc)).to be false
      end
    end

    context "item has a component with 'transcript' in format field" do
      it 'indicates caption/transcript' do
        allow(item_doc).to receive_message_chain('structures.files') do
          [{ doc: pdf_component_transcript_format, label: nil, order: '1' },
           { doc: pdf_component_misc, label: nil, order: '2' }]
        end
        allow(item_doc).to receive(:captions_urls).and_return(nil)
        expect(helper.has_caption_or_transcript?(item_doc)).to be true
      end
    end

    context "item has a component with 'transcript' in title field" do
      it 'indicates caption/transcript' do
        allow(item_doc).to receive_message_chain('structures.files') do
          [{ doc: pdf_component_transcript_title, label: nil, order: '1' }]
        end
        allow(item_doc).to receive(:captions_urls).and_return(nil)
        expect(helper.has_caption_or_transcript?(item_doc)).to be true
      end
    end

    context 'item has captions' do
      it 'indicates caption/transcript' do
        allow(item_doc).to receive_message_chain('structures.files') do
          []
        end
        allow(item_doc).to receive(:captions_urls).and_return(['https://repository.duke.edu/captions/8ef794c8-b8b8-42da-a735-e1a30b801004'])
        expect(helper.has_caption_or_transcript?(item_doc)).to be true
      end
    end
  end
end
