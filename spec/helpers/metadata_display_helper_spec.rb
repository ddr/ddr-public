# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MetadataDisplayHelper do
  describe '#permalink' do
    let(:permanent_url) { ['http://id.library.duke.edu/ark:/99999/fk4zzz'] }

    it 'returns a link to the permanent URL' do
      expect(helper.permalink({ value: permanent_url })).to(
        eq('<a href="http://id.library.duke.edu/ark:/99999/fk4zzz">' \
           'http://id.library.duke.edu/ark:/99999/fk4zzz</a>')
      )
    end
  end

  describe '#descendant_of' do
    let(:parent_id) { '87567572-b669-48a9-b478-a679725f85e6' }
    let(:documents) { [SolrDocument.new(id: parent_id)] }

    before do
      allow(helper).to receive(:find_solr_documents) { documents }
      double('link_to_document')
      allow(helper).to receive(:link_to_document)
    end

    context 'user can read parent' do
      before { allow(helper).to receive(:cached_read_current_ability).and_return(true) }

      it 'has a link to the parent' do
        expect(helper).to receive(:link_to_document)
        helper.descendant_of(value: ['87567572-b669-48a9-b478-a679725f85e6'])
      end
    end

    context 'user cannot read parent' do
      before { allow(helper).to receive(:cached_read_current_ability).and_return(false) }

      it 'displays the parent title but not have a link to the parent' do
        expect(helper).not_to receive(:link_to_document)
        helper.descendant_of(value: ['87567572-b669-48a9-b478-a679725f85e6'])
      end
    end
  end

  describe '#source_collection' do
    let(:document) { double('SolrDocument', finding_aid:) }
    let(:finding_aid) do
      double('FindingAid',
             title: 'Sample Title',
             collection_title: 'Sample Title',
             abstract: 'Basketball team posters are usually produced at the start of a new basketball season',
             collection_date_span: '1959-2013',
             collection_number: 'UA.01.15.0015',
             extent: '16 Linear Feet (2 oversize boxes, 5 oversize folders)',
             repository: 'Duke University Archives',
             url: '/sample_url',
             data?: true)
    end

    it 'returns a link to the source collection guide by title with a popover for more' do
      expect(helper.source_collection(document:)).to start_with('<a data-toggle="popover" data-boundary="viewport" data-placement="bottom" data-html="true" data-title="&lt;img class=&quot;collection-guide-icon&quot;')
      expect(helper.source_collection(document:)).to end_with('&gt;View this item in person at:&lt;br/&gt;Duke University Archives&lt;/div&gt;&lt;/div&gt;" href="/sample_url">Sample Title</a>')
    end
  end

  describe '#link_to_catalog' do
    context 'field contains an Alma MMS ID' do
      let(:bibsys_id_value) { ['99111111111118501'] }

      it 'returns a link to the record in the catalog' do
        expect(helper.link_to_catalog({ value: bibsys_id_value })).to(
          eq('<a href="https://find.library.duke.edu/catalog/DUKE99111111111118501">' \
             'https://find.library.duke.edu/catalog/DUKE99111111111118501</a>')
        )
      end
    end
  end

  describe '#display_more_info_options?' do
    context 'document has an EAD ID' do
      let(:document) { SolrDocument.new(id: SecureRandom.uuid, ead_id_ssi: 'uaduketaekwondo') }

      it 'returns true' do
        expect(helper.display_more_info_options?(document)).to be true
      end
    end

    context 'document has a Bib System ID' do
      let(:document) { SolrDocument.new(id: SecureRandom.uuid, bibsys_id_ssi: '99111111111118501') }

      it 'returns true' do
        expect(helper.display_more_info_options?(document)).to be true
      end
    end

    context 'document is a collection' do
      let(:document) { SolrDocument.new(id: SecureRandom.uuid, common_model_name_ssi: 'Collection') }

      it 'returns true' do
        expect(helper.display_more_info_options?(document)).to be true
      end
    end

    context 'document is not a collection and has no EAD, bib ID, nor sharable manifest' do
      let(:document) { SolrDocument.new(id: SecureRandom.uuid, common_model_name_ssi: 'Item') }

      before { allow(document).to receive(:display_iiif_link?).and_return(false) }

      it 'returns false' do
        expect(helper.display_more_info_options?(document)).to be false
      end
    end

    context 'document has a shareable IIIF manifest' do
      let(:document) { SolrDocument.new(id: SecureRandom.uuid) }

      before { allow(document).to receive(:display_iiif_link?).and_return(true) }

      it 'returns true' do
        expect(helper.display_more_info_options?(document)).to be true
      end
    end
  end

  describe '#pronom_identifier_link' do
    let(:pronom_id) { ['fmt/645'] }

    it 'returns links to the PRONOM ID info' do
      expect(helper.pronom_identifier_link({ value: pronom_id })).to(
        eq('<a href="https://www.nationalarchives.gov.uk/pronom/fmt/645">fmt/645</a>')
      )
    end
  end

  describe '#related_ark_prune' do
    context 'when the field has multiple values, one of which is an ark' do
      let(:relation) do
        ['ark:/99999/fk4aa0004',
         'https://library.duke.edu',
         'Some other related resource']
      end

      it 'prunes out the ARK but displays the other values w/links' do
        expect(helper.related_ark_prune({ value: relation })).to(
          eq('<a href="https://library.duke.edu">https://library.duke.edu</a><br/>Some other related resource')
        )
      end
    end

    context 'when the field only has an ARK' do
      let(:relation) { ['ark:/99999/fk4aa0004'] }

      it 'just says "see below" for the related resources' do
        expect(helper.related_ark_prune({ value: relation })).to(
          eq('See below')
        )
      end
    end
  end
end
