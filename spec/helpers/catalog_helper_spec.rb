# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CatalogHelper do
  let(:content_size) { '66.5 MB' }
  let(:permanent_url) { 'http://id.library.duke.edu/ark:/99999/fk4zzz' }

  describe '#has_two_or_more_multires_images?' do
    let(:document) { SolrDocument.new('id' => SecureRandom.uuid) }

    before { allow(document).to receive(:multires_image_file_paths) { file_paths } }

    context 'document does not have any multires images' do
      let(:file_paths) {}

      it 'returns false' do
        expect(helper).not_to have_two_or_more_multires_images(document)
      end
    end

    context 'document has a single multires image' do
      let(:file_paths) { ['path/to/image/1'] }

      it 'returns false' do
        expect(helper).not_to have_two_or_more_multires_images(document)
      end
    end

    context 'document has two multires images' do
      let(:file_paths) { ['path/to/image/1', 'path/to/image/2'] }

      it 'returns true' do
        expect(helper).to have_two_or_more_multires_images(document)
      end
    end
  end

  describe '#file_info' do
    let(:doc_id) { SecureRandom.uuid }
    let(:document) { SolrDocument.new('id' => doc_id) }

    context 'user can download the file' do
      before { allow(helper).to receive(:cached_download_current_ability).and_return(true) }

      it 'renders the download link and icon' do
        expect(helper).to receive(:render).with('download_link_and_icon').with(any_args)
        helper.file_info(document:)
      end
    end

    context 'user cannot download the file' do
      before { allow(helper).to receive(:cached_download_current_ability).and_return(false) }

      context 'and the user is logged in' do
        before { allow(helper).to receive(:user_signed_in?).and_return(true) }

        it "renders the 'unauthorized' warning" do
          expect(helper).to receive(:render).with(:download_not_authorized).with(any_args)
          helper.file_info(document:)
        end
      end

      context 'and the user is not logged in' do
        before do
          allow(helper).to receive(:user_signed_in?).and_return(false)
        end

        it 'renders a login-to-download button' do
          expect(helper).to receive(:render).with(:download_restricted).with(any_args)
          helper.file_info(document:)
        end
      end
    end
  end

  describe '#research_help_title' do
    context 'item has a research help name and url' do
      it 'returns a link to a research help page' do
        research_help = double('research_help', name: 'Rubenstein', url: 'http://library.duke.edu/rubenstein')
        expect(helper.research_help_title(research_help)).to include(link_to(research_help.name, research_help.url))
      end
    end

    context 'item has neither a research help name nor url' do
      it 'does not return a title nor a link' do
        research_help = double('research_help', name: nil, url: 'http://library.duke.edu/rubenstein')
        expect(helper.research_help_title(research_help)).to be_nil
      end
    end
  end

  describe '#read_not_authorized_contact_name' do
    context 'research help has a name' do
      it 'returns the name of research help' do
        research_help = double('research_help', name: 'Rubenstein Library')
        expect(helper.read_not_authorized_contact_name(research_help)).to eq 'Rubenstein Library'
      end
    end

    context 'research help does not have a name' do
      it 'returns the default value' do
        research_help = double('research_help', name: nil)
        expect(helper.read_not_authorized_contact_name(research_help)).to eq 'Duke University Libraries'
      end
    end
  end

  describe '#read_not_authorized_contact_url' do
    context 'research help has an ask url' do
      it 'returns the ask url' do
        research_help = double('research_help', ask: 'https://library.ask.edu', email: 'help@ddr.edu')
        expect(helper.read_not_authorized_contact_url(research_help)).to(
          eq 'https://library.ask.edu?&referrer=http://test.host'
        )
      end
    end

    context 'research help has an email address' do
      it 'returns the email address' do
        research_help = double('research_help', ask: nil, email: 'help@ddr.edu')
        expect(helper.read_not_authorized_contact_url(research_help)).to eq 'mailto:help@ddr.edu'
      end
    end

    context 'research help does not have a contact' do
      it 'returns the default contact url' do
        research_help = double('research_help', ask: nil, email: nil)
        expect(helper.read_not_authorized_contact_url(research_help)).to(
          eq 'https://duke.qualtrics.com/jfe/form/SV_eo1QIMoUtvmteQZ?Q_JFE=qdg'
        )
      end
    end
  end

  describe '#finding_aid_component_link' do
    before do
      allow(document).to receive_message_chain(:finding_aid, :url).and_return('https://archives.lib.duke.edu/catalog/uadupp')
    end

    context 'when the item has a prefixed aspace_id_ssi' do
      let(:document) { SolrDocument.new(aspace_id_ssi: 'aspace_123') }

      it 'returns the correct link' do
        expect(helper.finding_aid_component_link(document)).to eq('https://archives.lib.duke.edu/catalog/uadupp_aspace_123')
      end
    end

    context 'when the item has a non-prefixed aspace_id_ssi' do
      let(:document) { SolrDocument.new(aspace_id_ssi: '123') }

      it 'returns the correct link' do
        expect(helper.finding_aid_component_link(document)).to eq('https://archives.lib.duke.edu/catalog/uadupp_aspace_123')
      end
    end

    context 'when the item does not have an aspace_id_ssi' do
      let(:document) { SolrDocument.new }

      it 'returns nil' do
        expect(helper.finding_aid_component_link(document)).to be_nil
      end
    end
  end

  describe '#blog_post_thumb' do
    context 'blog post has a featured image thumb' do
      let(:post) do
        { 'url' => 'https://blogs.library.duke.edu/bitstreams/2014/10/24/collection-announcement/',
          'title' => 'Announcing My New Digital Collection', 'excerpt' => "<p>We have just launched an amazing new collection. &hellip; </p>\n", 'date' => '2014-10-24 16:29:48', 'author' => { 'slug' => 'abc123duke-edu', 'name' => 'John Doe' }, 'thumbnail_images' => { 'thumbnail' => { 'url' => 'dscsi033030010-150x150.jpg' } } }
      end

      it 'returns the thumbnail URL' do
        expect(helper.blog_post_thumb(post)).to match('dscsi033030010-150x150.jpg')
      end
    end

    context 'blog post has no featured image' do
      let(:post) do
        { 'url' => 'https://blogs.library.duke.edu/bitstreams/2014/10/24/collection-announcement/',
          'title' => 'Announcing My New Digital Collection', 'excerpt' => "<p>We have just launched an amazing new collection. &hellip; </p>\n", 'date' => '2014-10-24 16:29:48', 'author' => { 'slug' => 'abc123duke-edu', 'name' => 'John Doe' }, 'thumbnail_images' => [] }
      end

      it 'returns a default generic image URL' do
        expect(helper.blog_post_thumb(post)).to match(/devillogo-150-square.*\.jpg/)
      end
    end
  end

  describe '#link_to_admin_set' do
    let(:document) do
      SolrDocument.new(
        'id' => SecureRandom.uuid,
        'admin_set_title_ssi' => 'Title of Admin Set'
      )
    end

    it 'returns a link to a facet search' do
      allow(helper).to receive(:facet_search_url).and_return('?f%5Badmin_set_title_ssi%5D%5B%5D=Title of Admin Set')
      expect(helper.link_to_admin_set(document)).to eq('<a id="admin-set" href="?f%5Badmin_set_title_ssi%5D%5B%5D=Title of Admin Set">Title of Admin Set</a>')
    end
  end

  describe '#research_guide' do
    before(:context) { Ddr::Public.research_guides = 'guides.library.duke.edu' }

    context 'value is an empty array' do
      let(:values) { [] }

      it 'returns nil' do
        expect(helper.research_guide(values)).to be_nil
      end
    end

    context 'single value that is not a research guide' do
      let(:values) { ['ark:/99999/fk4bz6gb1w'] }

      it 'returns nil' do
        expect(helper.research_guide(values)).to be_nil
      end
    end

    context 'single value that is a research guide' do
      let(:values) { ['http://guides.library.duke.edu/baz'] }

      it 'returns the research guide URL' do
        expect(helper.research_guide(values)).to eq('http://guides.library.duke.edu/baz')
      end
    end

    context 'multiple values with more than one research guide' do
      let(:values) do
        ['http://guides.library.duke.edu/baz',
         'http://guides.library.duke.edu/foo']
      end

      it 'returns the first research guide URL' do
        expect(helper.research_guide(values)).to eq('http://guides.library.duke.edu/baz')
      end
    end

    context 'multiple values without any research guides' do
      let(:values) do
        ['ark:/99999/fk4bz6gb1w',
         'ark:/99999/hjkfdsieop']
      end

      it 'returns nil' do
        expect(helper.research_guide(values)).to be_nil
      end
    end
  end

  # rubocop:disable RSpec/IndexedLet
  describe '#document_dropdown_label' do
    let(:component1) do
      SolrDocument.new(
        id: 'changeme:1',
        format_tsim: 'transcript'
      )
    end
    let(:component2) do
      SolrDocument.new(
        id: 'changeme:2'
      )
    end
    let(:document) do
      SolrDocument.new(
        id: 'changeme:0'
      )
    end

    context 'there is a single document and a format' do
      it 'returns the value of format' do
        allow(document).to receive_message_chain('structures.files') do
          [{ doc: component1, label: nil, order: '1' }]
        end
        expect(helper.document_dropdown_label(document)).to eq('transcript')
      end
    end

    context 'there is a single document but no format' do
      it 'returns the default translation' do
        allow(document).to receive_message_chain('structures.files') do
          [{ doc: component2, label: nil, order: '1' }]
        end
        expect(helper.document_dropdown_label(document)).to eq('Document')
      end
    end

    context 'there is more than one document' do
      it 'returns the default translation' do
        allow(document).to receive_message_chain('structures.files') do
          [{ doc: component1, label: nil, order: '1' },
           { doc: component2, label: nil, order: '2' }]
        end
        expect(helper.document_dropdown_label(document)).to eq('Document')
      end
    end
  end
  # rubocop:enable RSpec/IndexedLet

  describe '#link_to_search_inside_full_text' do
    let(:doc) do
      SolrDocument.new(
        id: '2544d3be-83d0-44b5-a806-35035f70526f',
        contentdm_id_ssi: 'p15957coll13/id/15744'
      )
    end

    it 'generates the link to the contentDM image record' do
      expect(helper.link_to_search_inside_full_text(doc)).to(
        eq('<a class="dropdown-item" href="https://dukelibraries.contentdm.oclc.org/digital/collection/' \
           'p15957coll13/id/15744">View Full Text &amp; Images</a>')
      )
    end
  end

  describe '#link_to_search_inside_pdf' do
    let(:doc) do
      SolrDocument.new(
        id: '2544d3be-83d0-44b5-a806-35035f70526f',
        contentdm_id_ssi: 'p15957coll13/id/15744'
      )
    end

    it 'generates the link to the contentDM PDF' do
      expect(helper.link_to_search_inside_pdf(doc)).to(
        eq('<a class="dropdown-item" href="https://dukelibraries.contentdm.oclc.org/digital/' \
           'api/collection/p15957coll13/id/15744/page/0/inline/p15957coll13_15744_0">Searchable PDF</a>')
      )
    end
  end
end
