# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SearchScopeHelper do
  before do
    allow(helper).to receive_messages(
      search_scope_ddr_all: ['DDR', '/catalog', { data: { placeholder: 'digital things' } }],
      search_scope_dc_from_home: ['Digitized', '/dc', { data: { placeholder: 'photos etc' } }],
      search_scope_dc: ['Digital', '/dc', { data: { placeholder: 'photos etc' } }],
      search_scope_dc_this_collection: ['This Coll', '/dc/eaa', { data: { placeholder: 'this coll' } }],
      search_scope_acquired: ['Acquired', '/acquired_materials', { data: { placeholder: 'e-books etc' } }],
      search_scope_rdr: ['RDR', 'external',
                         { data: { placeholder: 'research data', baseurl: 'https://research.repository.duke.edu/etc' } }],
      search_scope_dspace: ['DSpace', 'external',
                            { data: { placeholder: 'Duke pubs', baseurl: 'https://dukespace.lib.duke.edu/etc' } }]
    )

    allow(helper).to receive(:render)
  end

  describe '#render_search_scope_dropdown_home' do
    let(:path) { '/' }
    let(:request) { double('request', path:) }

    before do
      allow(helper).to receive(:request).and_return(request)
    end

    context 'when on the DDR homepage' do
      # rubocop:disable RSpec/ExampleLength
      it 'renders the nested dropdown options' do
        expect(helper).to receive(:render)
          .with('catalog/search_scope_dropdown_homepage',
                {
                  search_scope_options:
                  {
                    'Duke Scholarship':
                    [
                      ['RDR', 'external', { data: {
                        baseurl: 'https://research.repository.duke.edu/etc',
                        placeholder: 'research data'
                      } }],
                      ['DSpace', 'external', { data: {
                        baseurl: 'https://dukespace.lib.duke.edu/etc',
                        placeholder: 'Duke pubs'
                      } }]
                    ],
                    'Library Collections':
                    [
                      ['DDR', '/catalog', { data: { placeholder: 'digital things' } }],
                      ['Digitized', '/dc', { data: { placeholder: 'photos etc' } }],
                      ['Acquired', '/acquired_materials',
                       { data: { placeholder: 'e-books etc' } }]
                    ]
                  }
                })
        helper.render_search_scope_dropdown_home
      end
      # rubocop:enable RSpec/ExampleLength
    end
  end

  describe '#render_search_scope_dropdown' do
    let(:request) { double('request', path:) }
    let(:params) {}

    before do
      allow(helper).to receive_messages(request:, params:)
    end

    context 'when on the digital collections home' do
      let(:path) { '/dc' }

      it 'renders the dropdown with DC & all DDR scopes' do
        expect(helper).to receive(:render)
          .with('search_scope_dropdown',
                {
                  active_search_scope_options:
                  [
                    ['Digital', '/dc', { data: { placeholder: 'photos etc' } }],
                    ['DDR', '/catalog', { data: { placeholder: 'digital things' } }]
                  ]
                })
        helper.render_search_scope_dropdown
      end
    end

    context 'when on or within a digital collection' do
      let(:path) { '/dc/eaa' }
      let(:params) { { collection: 'eaa' } }

      it 'renders the dropdown with this collection, DC, & all DDR scopes' do
        expect(helper).to receive(:render)
          .with('search_scope_dropdown',
                {
                  active_search_scope_options:
                  [
                    ['This Coll', '/dc/eaa', { data: { placeholder: 'this coll' } }],
                    ['Digital', '/dc', { data: { placeholder: 'photos etc' } }],
                    ['DDR', '/catalog', { data: { placeholder: 'digital things' } }]
                  ]
                })
        helper.render_search_scope_dropdown(params)
      end
    end

    context 'when within acquired materials' do
      let(:path) { '/acquired_materials' }
      let(:params) { { controller: 'acquired_materials' } }

      it 'renders the dropdown with acquired colls & all DDR scopes' do
        expect(helper).to receive(:render)
          .with('search_scope_dropdown',
                {
                  active_search_scope_options:
                  [
                    ['Acquired', '/acquired_materials', { data: { placeholder: 'e-books etc' } }],
                    ['DDR', '/catalog', { data: { placeholder: 'digital things' } }]
                  ]
                })
        helper.render_search_scope_dropdown(params)
      end
    end

    context 'when only the catalog search scope applies' do
      let(:path) { '/catalog' }

      it 'does not render a dropdown' do
        expect(helper).not_to receive(:render)
        helper.render_search_scope_dropdown
      end
    end
  end
end
