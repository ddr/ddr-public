# frozen_string_literal: true

require 'rails_helper'

RSpec.describe StreamController do
  describe '#show' do
    let(:document) { SolrDocument.new(id: SecureRandom.uuid) }

    before do
      allow(SolrDocument).to receive(:find).with(document.id) { document }
    end

    context "asset doesn't have streamable media" do
      before do
        allow(document).to receive(:streamable?).and_return(false)
      end

      it 'raises a 404 error' do
        get :show, format: 'js', xhr: true, params: { id: document.id }
        expect(response.response_code).to eq(404)
      end
    end

    context 'asset has streamable media' do
      let(:file_data) { {'file_identifier'=>{'id'=>'disk:///path/to/file'}, 'media_type'=>'audio/mpeg', 'original_filename'=>'foobar.mp3'} }

      before do
        allow(document).to receive(:streamable?).and_return(true)
        allow(document).to receive(:streamable_media).and_return(file_data)
      end

      context 'user has read permissions' do
        before do
          allow(controller.current_ability).to receive(:cannot?) { false }
        end

        it 'renders the media' do
          allow(controller).to receive(:send_file)
          get :show, format: 'js', xhr: true, params: { id: document.id }
          expect(controller).to have_received(:send_file)
        end
      end

      context "user doesn't have read permissions" do
        before do
          allow(controller.current_ability).to receive(:cannot?) { true }
        end

        it 'returns a 403 forbidden error' do
          get :show, format: 'js', xhr: true, params: { id: document.id }
          expect(response.response_code).to eq(403)
        end
      end
    end
  end
end
