# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AcquiredMaterialsController do
  describe 'search builder configuration' do
    it 'uses the acq_materials_search_builder' do
      expect(subject.blacklight_config.search_builder_class).to eq(AcqMaterialsSearchBuilder)
    end
  end
end
