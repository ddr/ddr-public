# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IiifImagesController, :iiif do
  let(:ark) { 'ark:/99999/fk4yyyyy' }
  let(:encoded_ark) { ark.gsub(%r{/}, '%2F').sub(%r{:}, '%3A') }
  let(:doc) do
    SolrDocument.new('id' => SecureRandom.uuid,
                     'permanent_id_ssi' => ark,
                     'workflow_state_ssi' => 'published',
                     'multires_image_file_path_ssi' => '/data/test_ptif')
  end
  let(:other_ark) { 'ark:/99999/fk4zzzzz' }

  shared_examples 'IIIF image requests' do |action|
    describe 'when the object is not found' do
      before do
        allow(controller).to receive(:image_doc_query).and_return(nil)
      end

      it 'is not found' do
        get(action, params:)
        expect(response.response_code).to eq 404
      end
    end

    describe 'when the object is found' do
      before do
        allow(controller).to receive(:image_doc_query) { doc }
      end

      describe 'and the image is viewable' do
        before do
          controller.current_ability.can(:read, SolrDocument)
        end

        it 'is successful' do
          get(action, params:)
          expect(response.response_code).to eq 200
        end
      end

      describe 'and the image is not viewable' do
        before do
          controller.current_ability.cannot(:read, SolrDocument)
        end

        it 'is unauthorized' do
          get(action, params:)
          expect(response.response_code).to eq 401
        end
      end
    end
  end

  describe '#info' do
    let(:params) { { identifier: encoded_ark } }

    include_examples 'IIIF image requests', :info
  end

  describe '#show' do
    let(:params) do
      { identifier: encoded_ark, region: 'full', size: 'full', rotation: '0', quality: 'default',
        format: 'jpg' }
    end

    include_examples 'IIIF image requests', :show
  end
end
