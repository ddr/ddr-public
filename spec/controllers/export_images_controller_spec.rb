# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ExportImagesController do
  let(:doc) do
    instance_double(SolrDocument,
                    id: '01234567-abcd-defa-bcde-890123456789',
                    derived_image_file_paths: ['spec/fixtures/files/images/imageA.jpg',
                                               'spec/fixtures/files/images/imageB.jpg'])
  end

  let(:begin_export_cookie) { "duke_ddr_export_begin_#{doc.id}" }

  before do
    allow(controller).to receive(:get_item_doc) { doc }
    controller.instance_variable_set(:@item_doc, doc)
    allow(doc).to receive(:public_id).and_return('CK0029')
    allow_any_instance_of(BlacklightHelper).to receive(:document_or_object_url).and_return('/dc/eaa/CK0029')
  end

  describe '#verification' do
    context "user hasn't signed in or verified a reCAPTCHA" do
      before do
        allow(controller).to receive(:verified_not_a_bot?).and_return(false)
        get :verification, params: { id: doc.id }
      end

      it 'renders a page w/options to prove they are human' do
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('export_images/unauthenticated')
      end
    end

    context 'user has already signed in or verified a reCAPTCHA' do
      before do
        allow(controller).to receive(:verified_not_a_bot?).and_return(true)
        get :verification, params: { id: doc.id }
      end

      it 'redirects to the item page URL' do
        expect(response).to redirect_to '/dc/eaa/CK0029'
        expect(response).not_to render_template('export_images/unauthenticated')
      end
    end
  end

  describe '#verify_export_recaptcha' do
    context 'when recaptcha was verified' do
      before do
        allow(controller).to receive(:verify_recaptcha).and_return(true)
        post :verify_export_recaptcha, params: { id: doc.id }
      end

      it 'stores a session variable indicating the recaptcha was verified' do
        expect(session[:verified_recaptcha]).to be true
      end

      it 'redirects to the item page' do
        expect(response).to redirect_to '/dc/eaa/CK0029'
      end

      it 'displays a flash message that recaptcha verification succeeded' do
        expect(flash[:notice]).to eq(I18n.t('ddr.public.image_export.verify.recaptcha_success'))
      end
    end

    context 'when recaptcha was not verified' do
      before do
        allow(controller).to receive(:verify_recaptcha).and_return(false)
        post :verify_export_recaptcha, params: { id: doc.id }
      end

      it 'does not store a session variable indicating the recaptcha was verified' do
        expect(session[:verified_recaptcha]).to be_nil
      end

      it 'renders the item page' do
        expect(response).to redirect_to '/dc/eaa/CK0029'
      end

      it 'displays a flash message that recaptcha verification failed' do
        expect(flash[:error]).to eq(I18n.t('ddr.public.image_export.verify.recaptcha_fail'))
      end
    end
  end

  shared_examples 'PDF or ZIP exports' do |action|
    describe 'anonymous user (not logged in)' do
      before do
        allow(controller).to receive(:user_signed_in?).and_return(false)
      end

      context 'when they have not yet completed reCAPTCHA verification' do
        before do
          allow(controller).to receive(:verified_recaptcha_for_session?).and_return(false)
        end

        it 'redirects them to verify' do
          post(action, params:)
          expect(response).to redirect_to verify_exportable_path
          expect(response.cookies[begin_export_cookie]).to be_nil
        end
      end

      context 'when they have completed reCAPTCHA verification' do
        before do
          allow(controller).to receive(:verified_recaptcha_for_session?).and_return(true)
          controller.current_ability.can(:read, doc)
        end

        it 'begins the export and sets a cookie' do
          post(action, params:)
          expect(response).to have_http_status(:success)
          expect(response.cookies[begin_export_cookie]).to be_truthy
        end
      end
    end

    describe 'logged in user' do
      before do
        allow(controller).to receive(:user_signed_in?).and_return(true)
      end

      context 'when they lack read permission on the item' do
        before do
          controller.current_ability.cannot(:read, doc)
        end

        it 'responds with unauthorized and prevents export' do
          post(action, params:)
          expect(response.response_code).to eq 401
          expect(response.cookies[begin_export_cookie]).to be_nil
        end
      end

      context 'when they have read permission on the item' do
        before do
          controller.current_ability.can(:read, doc)
        end

        it 'begins the export and sets a cookie' do
          post(action, params:)
          expect(response).to have_http_status(:success)
          expect(response.cookies[begin_export_cookie]).to be_truthy
        end
      end
    end

    describe 'item with no derived_images' do
      context 'when user is authorized but no derived image paths are present' do
        before do
          allow(controller).to receive(:verified_not_a_bot?).and_return(true)
          controller.current_ability.can(:read, doc)
          allow(doc).to receive(:derived_image_file_paths).and_return(nil)
        end

        it 'redirects to the item page with a flash error message' do
          get(action, params:)
          expect(flash[:error]).to eq(I18n.t('ddr.public.image_export.no_images'))
          expect(response).to redirect_to '/dc/eaa/CK0029'
        end
      end
    end
  end

  describe '#export_zip' do
    let(:params) { { id: doc.id } }

    include_examples 'PDF or ZIP exports', :export_zip
  end

  describe '#export_pdf' do
    let(:params) { { id: doc.id } }

    include_examples 'PDF or ZIP exports', :export_pdf
  end
end
