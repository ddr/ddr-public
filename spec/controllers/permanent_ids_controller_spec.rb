# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PermanentIdsController do
  describe '#show' do
    let!(:permanent_id) { 'ark:/99999/fk4yyyyy' }

    describe 'when the object does not exist' do
      before do
        allow(controller).to receive(:permanent_id_search) do
          [double(total: 0), []]
        end
      end

      # TODO: we don't have 404s routing to a custom error page
      it 'renders a 404 not found response' do
        get :show, params: { permanent_id: }
        expect(response.response_code).to eq(404)
        # expect(response).to render_template(file: "#{Rails.root}/public/404.html")
      end
    end

    describe 'when the object exists' do
      before do
        allow(controller).to receive(:permanent_id_search) do
          double(total: 1, documents: [{ id: 'e113ce18-04ad-4a82-be7a-77e2bef328e8' }])
        end
        double('document_or_object_url')
        allow(controller).to receive(:document_or_object_url).and_return('/catalog/e113ce18-04ad-4a82-be7a-77e2bef328e8')
      end

      it 'redirects to the catalog show view' do
        get :show, params: { permanent_id: }
        expect(response).to redirect_to('http://test.host/catalog/e113ce18-04ad-4a82-be7a-77e2bef328e8')
      end

      it 'redirects to the catalog show view with an allowable parameter' do
        get :show, params: { permanent_id:, embed: 'true' }
        expect(response).to redirect_to('http://test.host/catalog/e113ce18-04ad-4a82-be7a-77e2bef328e8?embed=true')
      end
    end
  end
end
