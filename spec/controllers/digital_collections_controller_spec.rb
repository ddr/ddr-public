# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DigitalCollectionsController do
  describe '#embed' do
    before do
      allow(controller).to receive(:is_embed?).and_return(true)
    end

    it 'does not have X-Frame-Options in the headers (else iframe embed is prevented)' do
      get :show, params: { id: 'BH0904', collection: 'adaccess', layout: 'embed' }
      expect(response.headers['X-Frame-Options']).to be_nil
    end
  end
end
