# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ItemsController do
  describe 'blacklight config for ItemsController' do
    it 'sets the search_builder_class' do
      expect(controller.blacklight_config.search_builder_class).to eq(OaiItemSearchBuilder)
    end

    it 'sets the document_model' do
      expect(controller.blacklight_config.document_model).to eq(SolrDocument)
    end

    it 'sets the oai document limit configuration' do
      expect(controller.blacklight_config.oai[:document][:limit]).to(
        eq(50)
      )
    end

    it 'sets the oai document set_fields' do
      expect(controller.blacklight_config.oai[:document][:set_fields]).to(
        eq([{ label: 'admin_set', solr_field: 'admin_set_title_ssi' },
            { label: 'collection', solr_field: 'is_member_of_collection_ssim' }])
      )
    end

    it 'sets the oai provider info' do
      expect(controller.blacklight_config.oai[:provider]).to(
        eq({ admin_email: 'repositoryhelp@duke.edu',
             record_prefix: 'oai:ddr',
             repository_name: 'Duke Digital Repository',
             repository_url: 'https://repository.duke.edu/items/oai',
             sample_id: '33073160-c9f8-4b9b-bf3c-6e61dc02dcf6' })
      )
    end
  end
end
