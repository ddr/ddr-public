# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationController do
  let(:user) { create(:user) }

  controller do
    def index
      head :ok
    end

    # rubocop:disable Lint/UselessMethodDefinition
    def after_sign_in_path_for(resource_or_scope)
      super
    end
    # rubocop:enable Lint/UselessMethodDefinition
  end

  describe 'after sign-in' do
    context 'a stored location does not exist' do
      it 'redirects to the root page' do
        expect(controller.after_sign_in_path_for(user)).to eq root_path
      end
    end

    context 'a stored location exists' do
      let(:stored_location) { '/some/stored/path' }

      it 'redirects to the stored location if it exists' do
        allow(controller).to receive(:stored_location_for) { stored_location }
        expect(controller.after_sign_in_path_for(user)).to eq stored_location
      end
    end

    context 'get request' do
      it "stores the user's location" do
        expect(controller).to receive(:store_user_location!)
        get :index
      end
    end

    context 'xhr request' do
      it "does not store the user's location" do
        expect(controller).not_to receive(:store_user_location!)
        get :index, xhr: true
      end
    end
  end
end
