# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DownloadsController do
  describe '#ddr_file_type' do
    describe 'request params has ddr_file_type' do
      before do
        allow(subject).to receive(:params).and_return({ ddr_file_type: 'fits' })
      end

      it 'returns the specified ddr file type' do
        expect(subject.send(:ddr_file_type)).to eq('fits')
      end
    end

    describe 'request params does not have ddr_file_type' do
      it 'returns a default ddr file type' do
        expect(subject.send(:ddr_file_type)).to eq('content')
      end
    end
  end

  describe '#ddr_file_name' do
    let(:resource_id) { SecureRandom.uuid }
    let(:file_data) do
      {
        'file_identifier' => { 'id' => 'disk:///path/to/file' },
        'media_type' => 'image/tiff',
        'original_filename' => 'test.tif'
      }
    end

    before do
      allow(subject).to receive(:asset) { resource }
      allow(subject).to receive(:ddr_file) { ddr_file }
    end

    describe 'ddr file original filename present' do
      let(:resource) { SolrDocument.new(id: resource_id) }
      let(:ddr_file) { DdrFile.new(file_data) }

      it 'returns the original filename' do
        expect(subject.send(:ddr_file_name)).to eq('test.tif')
      end
    end

    describe 'ddr file original filename not present' do
      let(:ddr_file) { DdrFile.new(file_data) }

      before do
        allow(subject).to receive(:ddr_file_type).and_return('content')
        allow(ddr_file).to receive(:original_filename).and_return(nil)
      end

      describe 'resource local id present' do
        let(:resource) { SolrDocument.new(id: resource_id, local_id_ssi: 'abc123') }

        it 'returns a concatentation of the local id and the default file extension' do
          expect(subject.send(:ddr_file_name)).to eq('abc123.tiff')
        end
      end

      describe 'resource local id not present' do
        let(:resource) { SolrDocument.new(id: resource_id) }

        it 'returns a concatentation of the resource ID, the file type, and the default file extension' do
          expect(subject.send(:ddr_file_name)).to eq("#{resource_id}_content.tiff")
        end
      end
    end
  end
end
