# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CatalogController do
  let(:user) { create(:user) }

  describe 'authorization failure' do
    before { allow(controller).to receive(:index).and_raise(CanCan::AccessDenied) }

    describe 'when user is signed in' do
      before { allow(controller).to receive(:user_signed_in?).and_return(true) }

      # TODO: we don't have 403s routing to a custom error page
      it 'returns an unauthorized response' do
        get :index
        expect(response.response_code).to eq(403)
        # expect(response).to render_template(file: "#{Rails.root}/public/403.html")
      end
    end

    describe 'when user is not signed in' do
      before { allow(controller).to receive(:user_signed_in?).and_return(false) }

      it 'forces the user to authenticate' do
        expect(controller).to receive(:authenticate_user!)
        get :index
      end
    end
  end

  describe 'oai configuration' do
    it 'sets the OAI configuration' do
      expect(controller.blacklight_config.oai.keys).to eq(%i[provider document])
    end
  end

  describe 'deep paging alert' do
    it 'sends the alert message' do
      get :index, params: { page: 250 }
      expect(flash[:alert]).to be_present
    end
  end

  describe 'deep paging error' do
    before { get :index, params: { page: 251 } }

    it 'sends the error message' do
      expect(flash[:error]).to be_present
    end

    it 'redirects to root' do
      expect(response).to redirect_to(root_path)
    end
  end

  describe 'deep facet paging alert' do
    it 'sends the alert message' do
      get :facet, params: { id: 'common_model_name_ssi', 'facet.page' => 50 }
      expect(flash[:alert]).to be_present
    end
  end

  describe 'deep facet paging error' do
    before { get :facet, params: { id: 'common_model_name_ssi', 'facet.page' => 51 } }

    it 'sends the error message' do
      expect(flash[:error]).to be_present
    end

    it 'redirects to root' do
      expect(response).to redirect_to(root_path)
    end
  end
end
