# frozen_string_literal: true

describe Ddr::Public::Controller::SolrQueryConstructor do
  subject { TestController.new }

  before do
    class TestController < ApplicationController
      include Ddr::Public::Controller::SolrQueryConstructor
    end
  end

  describe '#repository_id_query' do
    let(:ids) { %w[1111 2222] }

    it 'returns a terms query for the supplied repo ids' do
      expect(subject.repository_id_query(ids)).to eq(
        '{!terms f=id method=booleanQuery}1111,2222'
      )
    end
  end

  describe '#construct_boolean_terms_query' do
    let(:field) { 'a_solr_field_ssi' }
    let(:fields_values) { %w[one two three] }

    it 'returns a Solr terms query' do
      expect(subject.construct_boolean_terms_query(field, fields_values)).to eq(
        '{!terms f=a_solr_field_ssi method=booleanQuery}one,two,three'
      )
    end
  end
end
