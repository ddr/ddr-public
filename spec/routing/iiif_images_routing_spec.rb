# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'IIIF image routing', :iiif do
  let(:identifier) { 'ark:/87924/r41r6n637' }
  let(:encoded_identifier) { identifier.gsub(%r{/}, '%2F') }

  specify do
    expect(get("/iiif/#{encoded_identifier}/info.json"))
      .to route_to(controller: 'iiif_images', action: 'info', identifier:, format: 'json')
  end

  specify do
    expect(get("/iiif/#{encoded_identifier}/full/full/0/default.jpg"))
      .to route_to(controller: 'iiif_images', action: 'show', identifier:,
                   region: 'full', size: 'full', rotation: '0',
                   quality: 'default', format: 'jpg')
  end
end
