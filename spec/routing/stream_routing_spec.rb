# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Stream routing' do
  it 'has a stream route' do
    expect(get: '/stream/e113ce18-04ad-4a82-be7a-77e2bef328e8').to(
      route_to(controller: 'stream', action: 'show', id: 'e113ce18-04ad-4a82-be7a-77e2bef328e8')
    )
  end
end
