# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Export images to PDF & ZIP routing' do
  let(:uuid) { '01234567-abcd-defa-bcde-890123456789' }

  it 'has a verification route' do
    expect(get: '/export_images/01234567-abcd-defa-bcde-890123456789/verification')
      .to route_to(controller: 'export_images', action: 'verification', id: uuid)
  end

  it 'has a named route verify_exportable_path' do
    expect(get: verify_exportable_path(id: uuid))
      .to route_to(controller: 'export_images', action: 'verification', id: uuid)
  end

  it 'routes reCAPTCHA verifications' do
    expect(post: 'export_images/01234567-abcd-defa-bcde-890123456789/verify_export_recaptcha')
      .to route_to(controller: 'export_images', action: 'verify_export_recaptcha', id: uuid)
  end

  it 'routes POSTs for ZIP exports' do
    expect(post: '/export_zip/01234567-abcd-defa-bcde-890123456789')
      .to route_to(controller: 'export_images', action: 'export_zip', id: uuid)
  end

  it 'routes POSTs for PDF exports' do
    expect(post: '/export_pdf/01234567-abcd-defa-bcde-890123456789')
      .to route_to(controller: 'export_images', action: 'export_pdf', id: uuid)
  end
end
