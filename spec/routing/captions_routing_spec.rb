# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Captions routing' do
  it 'has a captions route' do
    expect(get: '/captions/e113ce18-04ad-4a82-be7a-77e2bef328e8').to(
      route_to(controller: 'captions',
               action: 'show',
               id: 'e113ce18-04ad-4a82-be7a-77e2bef328e8')
    )
  end
end
