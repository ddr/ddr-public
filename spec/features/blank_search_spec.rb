# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Blank search (no keywords)' do
  context 'from DDR homepage' do
    before { visit '/' }

    it 'has the correct title' do
      click_on 'Search'
      expect(page).to have_title('Browse Everything / Duke Digital Repository')
    end

    it 'has the matching collections box' do
      click_on 'Search'
      expect(page).to have_css('#matching-collections')
    end
  end

  context 'from DC homepage' do
    before { visit '/dc' }

    it 'has the correct title' do
      click_on 'Search'
      expect(page).to have_title('Browse Everything / Digital Collections / Duke Digital Repository')
    end

    it 'has the matching collections box' do
      click_on 'Search'
      expect(page).to have_css('#matching-collections')
    end
  end

  context 'from a DC collection homepage' do
    before { visit '/dc/adaccess' }

    it 'has the correct title' do
      click_on 'Search'
      expect(page).to have_title('Browse Everything / Ad*Access / Duke Digital Repository')
    end

    it 'does not show the matching collections box' do
      click_on 'Search'
      expect(page).to have_no_css('#matching-collections')
    end
  end
end
