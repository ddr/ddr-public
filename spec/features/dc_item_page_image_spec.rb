# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Digital Collections image item feature tests' do
  describe 'Single-image item' do
    before do
      visit '/dc/adaccess/BH0904'
    end

    context 'basic page features' do
      it 'has the correct title' do
        expect(page).to have_title('Keep your Beauty on duty! / Ad*Access / Duke Digital Repository')
      end

      it 'has the DC preservation sponsor message' do
        expect(page).to have_link('Lowell and Eileen Aptman Digital Preservation Fund',
                                  href: 'http://library.duke.edu/about/adopt-digital-collection')
      end

      it 'links to a IIIF manifest in the sidebar' do
        within('#sidebar') do
          expect(page).to have_link(href: %r{/manifest$})
        end
      end

      it 'links to a IIIF manifest in the Share menu' do
        within('#share-menu') do
          expect(page).to have_link(href: %r{/manifest$})
        end
      end
    end

    context 'with Javascript', :js do
      it 'renders the Mirador UI via javascript' do
        expect(page).to have_css('.mirador-viewer')
      end

      it 'provides image download links' do
        find_by_id('download-dropdown').click
        expect(page).to have_css('#download-menu .download-link-single', count: 4)
      end

      it 'does not provide ZIP nor PDF downloads' do
        find_by_id('download-dropdown').click
        expect(page).to have_no_button('#image-export-pdf')
        expect(page).to have_no_button('#image-export-zip')
      end
    end
  end

  describe 'Multi-image item' do
    before do
      visit '/dc/adaccess/TV0422'
    end

    context 'with Javascript', :js do
      it 'renders the Mirador UI via javascript' do
        expect(page).to have_css('.mirador-viewer')
      end

      it 'provides ZIP & PDF downloads' do
        find_by_id('download-dropdown').click
        expect(page).to have_css('#image-export-pdf')
        expect(page).to have_css('#image-export-zip')
      end

      it 'provides individual image download links' do
        find_by_id('download-dropdown').click
        expect(page).to have_css('#download-menu .download-link-single', count: 4)
      end

      it 'initializes with download links for the first image' do
        find_by_id('download-dropdown').click
        expect(page).to have_css('#download-menu .download-link-single[href="/iiif/ark:/87924/r4b854554/full/full/0/default.jpg"]')
      end

      it 'updates the download links to reflect the current image when advancing Mirador' do
        find('button.mirador-next-canvas-button').click
        find_by_id('download-dropdown').click
        expect(page).to have_css('#download-menu .download-link-single[href="/iiif/ark:/87924/r4g15tz80/full/full/0/default.jpg"]')
      end
    end
  end
end
