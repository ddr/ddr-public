# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Related items or collections' do
  describe 'Related items from item page (relation field)' do
    context 'when in DC admin set, unconfigured collection' do
      it 'has the related item(s) in a grid at the bottom' do
        visit '/dc/0001-dc-collection-basic/0008-dc-item-related'
        expect(page).to have_css('h2', text: 'Related Items')
        expect(page).to have_css('#documents.row.documents-gallery')
      end
    end

    context 'when outside of DC admin set' do
      it 'has the related item(s) in a grid at the bottom' do
        visit '/catalog/abcd0007-0000-0000-0000-000000000000'
        expect(page).to have_css('h2', text: 'Related Items')
        expect(page).to have_css('#documents.row.documents-gallery')
      end
    end
  end

  describe 'Related collections from collection page (relation field)' do
    context 'when in DC admin set, unconfigured collection' do
      before do
        visit '/dc/0010-dc-collection-related'
      end

      it 'has the related collection(s) in a grid at the bottom' do
        expect(page).to have_css('h2', text: 'Related Collections')
        expect(page).to have_css('#documents.row.documents-gallery')
      end

      it 'has a Research Guide link when a libguides URL is present' do
        expect(page).to have_link('Research Guide',
                                  href: 'https://guides.library.duke.edu/adaccess')
      end
    end

    context 'when outside of DC admin set' do
      before do
        visit '/catalog/abcd0009-0000-0000-0000-000000000000'
      end

      it 'has relation values in the metadata display, w/o the ARK' do
        expect(page).to have_css('#collection-info dd.blacklight-relation_tsim', text: /Some other related resource/)
        expect(page).to have_no_css('#collection-info dd.blacklight-relation_tsim', text: /ark/)
      end

      it 'has the related collection(s) in a grid at the bottom' do
        expect(page).to have_css('h2', text: 'Related Collections')
        expect(page).to have_css('#documents.row.documents-gallery')
      end
    end
  end
end
