# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Non-DC PDF component page feature tests' do
  describe 'Basic page features' do
    before do
      visit '/catalog/abcd0012-0000-0000-0000-000000000000'
    end

    it 'displays an inline PDF using the browser-native viewer' do
      expect(page).to have_css('object[type="application/pdf"]')
    end

    it 'does not link to a IIIF manifest' do
      within('#sidebar') do
        expect(page).to have_no_link(href: %r{/manifest$})
      end
    end
  end
end
