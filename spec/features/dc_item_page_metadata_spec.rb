# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Digital Collections item metadata feature tests' do
  describe 'Configured collection' do
    before do
      visit '/dc/adaccess/BH0904'
    end

    it 'has the correct metadata section header' do
      expect(page).to have_css('h2', text: 'Item Info')
    end

    it 'has fields linking to faceted results' do
      expect(page).to have_link('Soaps',
                                href: '/dc/adaccess?f%5Bsubject_facet_sim%5D%5B%5D=Soaps')
    end

    it 'has its auto_link fields rendering any URLs as links' do
      expect(page).to have_link('https://library.duke.edu/rubenstein/research/citations-and-permissions',
                                href: 'https://library.duke.edu/rubenstein/research/citations-and-permissions')
    end

    it 'does not use the BL default search session tracking for document links' do
      expect(page).to have_no_css('a#parent-collection[data-context-href *= "/track"]')
    end
  end
end
