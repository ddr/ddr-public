# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Non-DC collection page feature tests' do
  describe 'Basic page features' do
    before do
      visit '/catalog/abcd0004-0000-0000-0000-000000000000'
    end

    it 'has the correct title' do
      expect(page).to have_title('Test Non-DC Collection 0004 / Rubenstein Library / Duke Digital Repository')
    end

    it 'has a Browse all X Items button' do
      expect(page).to have_link(id: 'browse-all-items-btn',
                                text: /Browse all/)
    end

    it 'has a description' do
      expect(page).to have_css('#about-collection',
                               text: /^A generic test collection/)
    end

    it 'has a thumb grid view of some items in the collection' do
      expect(page).to have_css('#items-in-collection #documents.row.documents-gallery')
    end

    it 'has a list of metadata fields & values' do
      expect(page).to have_css('#collection-info dl')
    end

    it 'has a research help contact' do
      expect(page).to have_css('#research-help',
                               text: /David M. Rubenstein Rare Book & Manuscript Library/)
    end

    it 'links to the catalog record with an Alma MMS ID' do
      expect(page).to have_link('Catalog Record', href: 'https://find.library.duke.edu/catalog/DUKE99111111111118501')
    end

    it 'links to a IIIF manifest in the sidebar' do
      within('#sidebar') do
        expect(page).to have_link(href: %r{/manifest$})
      end
    end
  end

  describe 'JS-dependent page features', :js do
    before do
      visit '/catalog/abcd0004-0000-0000-0000-000000000000'
    end

    it 'presents research help links in a Contact dropdown' do
      find('#research-help .dropdown a.dropdown-toggle').click
      expect(page).to have_link(text: /Ask a Question/)
      expect(page).to have_link(href: 'tel:+1-919-660-5822')
    end
  end
end
