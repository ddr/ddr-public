# frozen_string_literal: true

# Duke Web Accessibility Guidelines specify a preference for
# WCAG 2.0 AA for most content and Section 508 for multimedia:
# https://web.accessibility.duke.edu/duke-guidelines

# Docs for using Axe Core RSpec:
# https://github.com/dequelabs/axe-core-gems/blob/develop/packages/axe-core-rspec/README.md

# See rule descriptions and tags:
# https://github.com/dequelabs/axe-core/blob/develop/doc/rule-descriptions.md

# For now, we'll use the default, which will check for WCAG 2.0 A/AA, 508, best practices
# and more. If in the future we need to target specific standards, we can use the
# tag clause (.according_to)

require 'rails_helper'
require 'axe-rspec'

RSpec.describe 'Accessibility (WCAG, 508, Best Practices)', :accessibility, :js do
  describe 'DDR homepage' do
    it 'is accessible' do
      visit '/'
      expect(page).to be_axe_clean
    end
  end

  describe 'DDR-wide search' do
    it 'is accessible' do
      visit '/catalog?utf8=✓&q=duke&search_field=all_fields'
      expect(page).to be_axe_clean
    end
  end

  describe 'Digital Collections portal page' do
    it 'is accessible' do
      visit '/dc'
      expect(page).to be_axe_clean.excluding '.chart_js'
    end
  end

  describe 'DC configured collection portal page' do
    it 'is accessible' do
      visit '/dc/adaccess'
      expect(page).to be_axe_clean
    end
  end

  describe 'DC image item page' do
    it 'is accessible' do
      visit '/dc/adaccess/BH0904'
      expect(page).to be_axe_clean
    end
  end

  describe 'DC image item embed view (iframe)' do
    it 'is accessible' do
      visit '/dc/adaccess/BH0904?embed=true'
      expect(page).to be_axe_clean
    end
  end

  describe 'DC collection browse items page' do
    it 'is accessible' do
      visit 'dc/adaccess?f%5Bcommon_model_name_ssi%5D%5B%5D=Item'
      expect(page).to be_axe_clean
    end
  end

  describe 'DC collection tabbed about page' do
    it 'is accessible' do
      visit '/dc/adaccess/about'
      expect(page).to be_axe_clean
    end
  end

  describe 'DC collection featured items page' do
    it 'is accessible' do
      visit '/dc/adaccess/featured'
      expect(page).to be_axe_clean
    end
  end

  describe 'Non-DC component page' do
    it 'is accessible' do
      visit '/catalog/abcd0006-0000-0000-0000-000000000000'
      expect(page).to be_axe_clean
    end
  end
end
