# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'DC collection page feature tests' do
  describe 'Basic page features' do
    before do
      # Note a DC collection URL uses Local ID
      visit '/dc/0001-dc-collection-basic'
    end

    it 'has the correct title' do
      expect(page).to have_title('Test DC Collection 0001 / Digital Collections / Duke Digital Repository')
    end

    it 'has a list of metadata fields & values' do
      expect(page).to have_css('#collection-info dl')
    end

    it 'links to the catalog record with an Aleph ID' do
      expect(page).to have_link('Catalog Record', href: 'https://find.library.duke.edu/catalog/DUKE006296494')
    end

    it 'links to a IIIF manifest for the item' do
      within('#sidebar') do
        expect(page).to have_link(href: %r{/manifest$})
      end
    end
  end
end
