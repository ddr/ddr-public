# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'DC Image component page feature tests' do
  describe 'Basic page features' do
    before do
      visit '/catalog/32d87ea3-df7c-444d-aa08-8a6c79849b27'
    end

    context 'basic page features' do
      it 'links to a IIIF manifest' do
        within('#sidebar') do
          expect(page).to have_link(href: %r{/manifest\?})
        end
      end
    end

    context 'with Javascript', :js do
      xit 'renders the Mirador UI via javascript' do
        expect(page).to have_css('.mirador-viewer')
      end
    end
  end
end
