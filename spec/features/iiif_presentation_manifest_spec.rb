# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'IIIF Presentation API manifests' do
  describe 'image item manifest' do
    let(:json) { JSON.parse(page.body) }

    before do
      visit '/iiif/ark:/87924/r4mk65v7f/manifest'
    end

    it 'has some basic manifest properties' do
      expect(json['@context']).to eq ['http://www.w3.org/ns/anno.jsonld',
                                      'http://iiif.io/api/presentation/3/context.json']
      expect(json['type']).to eq 'Manifest'
    end

    it 'lists its only component' do
      expect(json['items']).to be_an Array
      expect(json['items'].length).to eq 1
    end

    it 'has the correct component properties' do
      component = json['items'].first
      expect(component['type']).to eq 'Canvas'
      expect(component['id']).to end_with('/iiif/ark%3A%2F87924%2Fr4c53fn3p/canvas')
    end

    # rubocop:disable RSpec/ExampleLength, Layout/LineLength
    it 'renders metadata with ddr-public configuration' do
      expected_metadata = [
        {
          'label' => { 'en' => ['Title'] },
          'value' => { 'en' => ['Keep your Beauty on duty!'] }
        },
        {
          'label' => { 'en' => ['Permalink'] },
          'value' => {
            'en' => [
              '<a href="https://idn.duke.edu/ark:/87924/r4mk65v7f">https://idn.duke.edu/ark:/87924/r4mk65v7f</a>'
            ]
          }
        },
        {
          'label' => { 'en' => ['Date'] },
          'value' => { 'en' => ['1942'] }
        },
        {
          'label' => { 'en' => ['Type'] },
          'value' => { 'en' => ['Still Image'] }
        },
        {
          'label' => { 'en' => ['Identifier'] },
          'value' => {
            'en' => [
              'duke:334826',
              'BH0904',
              'ark:/87924/r4mk65v7f',
              '2896e5ec-af34-4f1d-90c5-da335474e211'
            ]
          }
        },
        {
          'label' => { 'en' => ['Collection'] },
          'value' => {
            'en' => [
              '<a href="https://idn.duke.edu/ark:/87924/r47d2qq45">Ad*Access</a>'
            ]
          }
        },
        {
          'label' => { 'en' => ['Subject'] },
          'value' => { 'en' => %w[Military Soaps] }
        },
        {
          'label' => { 'en' => ['Format'] },
          'value' => { 'en' => ['advertisements'] }
        },
        {
          'label' => { 'en' => ['Category'] },
          'value' => { 'en' => ['Beauty and Hygiene (1911-1956)'] }
        },
        {
          'label' => { 'en' => ['Rights'] },
          'value' => {
            'en' => [
              '<a href="http://rightsstatements.org/vocab/InC/1.0/">http://rightsstatements.org/vocab/InC/1.0/</a>'
            ]
          }
        },
        {
          'label' => { 'en' => ['Rights Note'] },
          'value' => {
            'en' => [
              'This material is made available for research, scholarship, and private study. Copyright in this material has not been transferred to Duke University. For reuses of this material beyond those permitted by fair use or otherwise allowed under the Copyright Act, please see our page on copyright and citations: <a href="https://library.duke.edu/rubenstein/research/citations-and-permissions">https://library.duke.edu/rubenstein/research/citations-and-permissions</a>.'
            ]
          }
        }
      ]

      expect(json['metadata']).to eq(expected_metadata)
    end
    # rubocop:enable RSpec/ExampleLength, Layout/LineLength

    it 'does not indicate a starting component canvas' do
      expect(json).not_to have_key('start')
    end
  end

  describe 'image component manifest', skip: 'Redirected' do
    let(:json) { JSON.parse(page.body) }

    before do
      visit '/iiif/ark:/87924/r4c53fn3p/manifest'
    end

    it 'has some basic manifest properties' do
      expect(json['@context']).to eq ['http://www.w3.org/ns/anno.jsonld',
                                      'http://iiif.io/api/presentation/3/context.json']
      expect(json['type']).to eq 'Manifest'
    end

    it 'has the correct component properties' do
      component = json['items'].first
      expect(component['type']).to eq 'Canvas'
      expect(component['id']).to end_with('/iiif/ark%3A%2F87924%2Fr4c53fn3p/canvas')
    end

    it 'indicates a starting component canvas' do
      expect(json).to have_key('start')
      expect(json['start']['id']).to end_with('/iiif/ark%3A%2F87924%2Fr4c53fn3p/canvas')
      expect(json['start']['type']).to eq 'Canvas'
    end
  end
end
