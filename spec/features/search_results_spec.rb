# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Search results pages', :js do
  context 'blank everything search' do
    before do
      visit '/'
      click_on 'Search'
    end

    it 'renders the Year facet with a blacklight_range_limit slider' do
      expect(page).to have_css('#facet-year_facet_iim')
      expect(page).to have_css('#facet-year_facet_iim .slider-horizontal')
    end

    it 'does not use BL default search session tracking' do
      expect(page).to have_no_css('a[data-context-href *= "/track"]')
    end
  end
end
