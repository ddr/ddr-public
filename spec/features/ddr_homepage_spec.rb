# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'DDR homepage feature tests' do
  describe 'DDR homepage' do
    before do
      visit '/'
    end

    it 'has the correct title' do
      expect(page).to have_title('Duke Digital Repository')
    end

    it 'has an h1 with the main heading' do
      expect(page).to have_css('h1', text: 'Digital Repositories at Duke')
    end
  end
end
