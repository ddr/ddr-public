# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Digital Collections homepage feature tests' do
  describe 'DC portal' do
    before do
      visit '/dc'
    end

    it 'has the correct title' do
      expect(page).to have_title('Digital Collections / Duke Digital Repository')
    end

    it 'has the DC preservation sponsor message' do
      expect(page).to have_link('Lowell and Eileen Aptman Digital Preservation Fund',
                                href: 'http://library.duke.edu/about/adopt-digital-collection')
    end
  end
end
