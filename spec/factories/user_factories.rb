# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "person#{n}@example.com" }
    email(&:username)
    password { 'foobar' }
    password_confirmation { 'foobar' }

    trait :duke do
      sequence(:username) { |n| "person#{n}@duke.edu" }
    end

    trait :superuser do
      groups { [Ability::SUPERUSER_GROUP] }
    end
  end
end
