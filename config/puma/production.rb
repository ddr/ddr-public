# frozen_string_literal: true

#
# N.B. This file may be overwritten by the Helm chart.
#
threads_count = ENV.fetch('RAILS_MAX_THREADS', 5)
threads threads_count, threads_count
workers ENV.fetch('WEB_CONCURRENCY', 2)
preload_app!
bind 'tcp://0.0.0.0:3000'
