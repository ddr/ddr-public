includes:
  local_ids:
    - brownfrankclyde
showcase_images:
  custom_images:
    - brownfrankclyde-showcase-car-1.jpg
    - brownfrankclyde-showcase-car-2.jpg
    - brownfrankclyde-showcase-clareleighton-all-day-singing-0201.jpg
    - brownfrankclyde-showcase-clareleighton-cypress-knees-0303_409.jpg
    - brownfrankclyde-showcase-clareleighton-hatteras-wreck-0205_660.jpg
    - brownfrankclyde-showcase-clareleighton-the-baptizing-0103_226.jpg
    - brownfrankclyde-showcase-clareleighton-wind-and-pine-0204_432.jpg
    - brownfrankclyde-showcase-porch.jpg
  layout: landscape
blog_posts: https://blogs.library.duke.edu/bitstreams/wp-json/ddr/v1/posts?tag_slug=brownfrankclyde

item_relators:
  - name: "Recordings contained on object"
    field: "hasPart_tsim"
    id_field: "permanent_id_ssi"
  - name: "Complete phonorecording"
    field: "isPartOf_tsim"
    id_field: "permanent_id_ssi"

configure_blacklight:
  add_facet_field:
    - field: "creator_facet_sim"
      label: Performer
      limit: 5
      collapse: false
    - field: "category_facet_sim"
      label: Instrumentation
      collapse: false
      limit: 5
    - field: "year_facet_iim"
      label: Year
      limit: 9999
      collapse: false
      range:
        num_segments: 6
        segments: true
    - field: "spatial_facet_sim"
      label: Location
      limit: 5
      collapse: false
    - field: "format_facet_sim"
      label: Format
      limit: 5
    - field: "subject_facet_sim"
      label: Subject
      limit: 5
    - field: "series_facet_sim"
      label: Series
      limit: 5
    - field: "publication_facet_sim"
      label: Publication
      limit: 5
    - field: "common_model_name_ssi"
      label: Browse
      show: false
    - field: "contributor_facet_sim"
      label: Contributor
      limit: 5
      show: false
    - field: "aspace_id_ssi"
      label: ArchivesSpace ID
      show: false
    - field: "is_part_of_ssim"
      label: "Part of"
      show: false

  add_index_field:
    - field: "content_media_type_ssim"
      label: "File"
      helper_method: file_info
    - field: "creator_tsim"
      separator: "; "
      label: "Performer"
    - field: "date_tsim"
      separator: "; "
      label: "Date"
      accessor: :humanized_date
    - field: "temporal_tsim"
      separator: "; "
      label: "Temporal"
    - field: "format_tsim"
      separator: "; "
      label: "Format"
    - field: "permanent_url_ssi"
      label: "Permalink"
      helper_method: "permalink"
    - field: "is_part_of_ssim"
      label: "Part of"
      helper_method: "descendant_of"
    - field: "is_member_of_collection_ssim"
      label: "Collection"
      helper_method: "descendant_of"
  add_show_field:
    - field: "content_media_type_ssim"
      label: "File"
      helper_method: file_info
    - field: "title_ssi"
      separator: "; "
      label: "Title"
    - field: "alternative_tsim"
      separator: "; "
      label: "Alternative Title"
    - field: "date_tsim"
      separator: "; "
      label: "Date"
      accessor: :humanized_date
    - field: "creator_tsim"
      separator: "; "
      label: "Performer"
      link_to_facet: "creator_facet_sim"
    - field: "category_tsim"
      separator: "; "
      label: "Instrumentation"
      link_to_facet: "category_facet_sim"
    - field: "contributor_tsim"
      separator: "; "
      label: "Contributor"
      link_to_facet: "contributor_facet_sim"
    - field: "description_tsim"
      label: "Description"
      simple_format: true
      auto_link: true
    - field: "abstract_tsim"
      label: "Abstract"
      simple_format: true
      auto_link: true
    - field: "spatial_tsim"
      separator: "; "
      label: "Location"
      link_to_facet: "spatial_facet_sim"
    - field: "subject_tsim"
      separator: "; "
      label: "Subject"
      link_to_facet: "subject_facet_sim"
    - field: "format_tsim"
      separator: "; "
      label: "Format"
      link_to_facet: "format_facet_sim"
    - field: "publisher_tsim"
      separator: "; "
      label: "Publisher"
    - field: "publication_tsim"
      separator: "; "
      label: "Publication"
      link_to_facet: "publication_facet_sim"
    - field: "language_name_tesim"
      separator: "; "
      label: "Language"
      link_to_facet: "language_facet_sim"
    - field: "extent_tsim"
      separator: "; "
      label: "Extent"
    - field: "is_part_of_ssim"
      label: "Part of"
      helper_method: descendant_of
    - field: "is_member_of_collection_ssim"
      label: "Digital Collection"
      helper_method: descendant_of
    - field: "bibsys_id_ssi"
      label: "Catalog Record"
      helper_method: link_to_catalog
    - field: "ead_id_ssi"
      separator: "; "
      label: "Source Collection"
      helper_method: source_collection
    - field: "series_tsim"
      separator: "; "
      label: "Series"
      link_to_facet: "series_facet_sim"
    - field: "folder_tsim"
      separator: "; "
      label: "Folder"
    - field: "relation_tsim"
      label: "Related Resources"
      helper_method: related_ark_prune
    - field: "provenance_tsim"
      separator: "; "
      label: "Provenance"
      auto_link: true
    - field: "isReferencedBy_tsim"
      separator: "; "
      label: "Referenced In"
      auto_link: true
    - field: "rights_tsim"
      separator: "; "
      label: "Rights"
      helper_method: "rights_display"
    - field: "rights_note_tsim"
      separator: "; "
      label: "Rights Note"
      auto_link: true
    - field: "hasVersion_tsim"
      separator: "; "
      label: "Other Version"
    - field: "identifier_all_ssim"
      separator: "; "
      label: "Identifier"
    - field: "permanent_url_ssi"
      label: "Permalink"
      helper_method: permalink
    - field: "bibliographicCitation_tsim"
      separator: "; "
      label: "Citation"
    - field: 'sponsor_display'
      separator: "; "
      label: "Sponsor"
      accessor: 'sponsor_display'
      helper_method: collection_sponsor
