# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  concern :range_searchable, BlacklightRangeLimit::Routes::RangeSearchable.new
  concern :oai_provider, BlacklightOaiProvider::Routes.new

  def uuid_constraint
    /\h{8}-\h{4}-\h{4}-\h{4}-\h{12}/
  end

  # Allows for both UUID and Local ID patterns
  def id_constraint
    /\h{8}-\h{4}-\h{4}-\h{4}-\h{12}|[A-Za-z0-9\-._~]*/
  end

  mount Blacklight::Engine => '/'
  root to: 'pages#homepage'

  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks', sessions: 'users/sessions' }

  concern :searchable, Blacklight::Routes::Searchable.new

  resource :catalog, only: [:index], as: 'catalog', path: '/catalog', controller: 'catalog' do
    concerns :oai_provider

    concerns :searchable
    concerns :range_searchable
  end

  resource :acquired_materials, only: [:index], as: 'acquired_materials', path: '/acquired_materials',
                                controller: 'acquired_materials' do
    concerns :searchable
    concerns :range_searchable
  end

  resource :items, only: [:index], as: 'items', path: '/items', controller: 'items' do
    concerns :oai_provider

    concerns :searchable
    concerns :range_searchable
  end

  concern :exportable, Blacklight::Routes::Exportable.new

  resources :solr_documents, only: [:show], path: '/catalog', controller: 'catalog' do
    concerns :exportable
  end

  resources :bookmarks do
    concerns :exportable

    collection do
      delete 'clear'
    end
  end

  constraints(id: uuid_constraint) do
    get 'directory_tree/:id/:directory_id', to: 'directory_tree#show', as: 'directory_tree_node'
    defaults directory_id: 'root' do
      get 'directory_tree/:id', to: 'directory_tree#show', as: 'directory_tree_root'
    end
  end

  # Range Limit and Facet routes for DC
  get 'dc/range_limit/', to: 'digital_collections#range_limit'
  get 'dc/facet/:id', to: 'digital_collections#facet', as: 'digital_collections_facet'

  # DC Collection scoped routes
  constraints(id: id_constraint, collection: id_constraint) do
    get 'dc/:collection/range_limit/', to: 'digital_collections#range_limit'
    get 'dc/:collection/facet/:id', to: 'digital_collections#facet'
    get 'dc/:collection/featured', to: 'digital_collections#featured', as: 'featured_items'
    get 'dc/:collection/about', to: 'digital_collections#about', as: 'digital_collections_about'
    get 'dc/:collection/:id/media', to: 'digital_collections#media', constraints: { format: 'json' }
    get 'dc/:collection/:id', to: 'digital_collections#show'
    get 'dc/:collection', to: 'digital_collections#index', as: 'digital_collections'
  end

  # Special named route just for DC Portal to distinguish
  # from DC collection portals
  get 'dc' => 'digital_collections#index_portal', as: 'digital_collections_index_portal'

  # Must exist for facets to work from DC portal
  get 'dc' => 'digital_collections#index'

  # Avoids this error: "Unable to find track_digital_collections_path route helper."
  post 'digital_collections/:id/track', to: 'digital_collections#track', as: :track_digital_collections

  # Range Limit and Facet routes for DC
  get 'portal/range_limit/', to: 'portal#range_limit'
  get 'portal/facet/:id', to: 'portal#facet', as: 'portal_facet'

  # DC Collection scoped routes
  constraints(id: id_constraint, collection: id_constraint) do
    get 'portal/:collection/about', to: 'portal#about', as: 'portal_about'
    get 'portal/:collection/:id', to: 'portal#show'
    get 'portal/:collection', to: 'portal#index', as: 'portal'
  end

  # Special named route just for DC Portal to distinguish
  # from DC collection portals
  get 'portal' => 'portal#index_portal', as: 'portal_index_portal'

  # Must exist for facets to work from DC portal
  get 'portal' => 'portal#index'

  # Downloads
  get 'download/:id(/:ddr_file_type)' => 'downloads#show', :constraints => { id: uuid_constraint }, as: 'download'

  # Image Item Exports (PDF or ZIP)
  post 'export_zip/:id', to: 'export_images#export_zip', as: 'export_zip',
                         constraints: { id: uuid_constraint }
  post 'export_pdf/:id', to: 'export_images#export_pdf', as: 'export_pdf',
                         constraints: { id: uuid_constraint }

  post 'export_images/:id/verify_export_recaptcha', to: 'export_images#verify_export_recaptcha',
                                                    constraints: { id: uuid_constraint }

  get 'export_images/:id/verification', to: 'export_images#verification', as: 'verify_exportable',
                                        constraints: { id: uuid_constraint }

  # Streamable Media
  get 'stream/:id' => 'stream#show', as: 'stream', constraints: { id: id_constraint }

  # Captions
  get 'captions/:id/txt' => 'captions#text', as: 'captions_txt', constraints: { id: id_constraint }
  get 'captions/:id/pdf' => 'captions#pdf', as: 'captions_pdf', constraints: { id: id_constraint }
  get 'captions/:id' => 'captions#show', as: 'captions', constraints: { id: id_constraint }

  # Permanent IDs
  get 'id/*permanent_id' => 'permanent_ids#show'

  # Static Pages
  get '/homepage' => 'pages#homepage'

  # IIIF Image API
  get '/iiif/*identifier/info', to: 'iiif_images#info', defaults: { format: 'json' }
  get '/iiif/*identifier/:region/:size/:rotation/:quality', to: 'iiif_images#show'

  # IIIF Presentation API
  get '/iiif/*id/manifest', to: 'iiif_presentation#manifest', as: 'iiif_presentation', format: false
  get '/iiif/*id/manifest.json', to: redirect('/iiif/%{id}/manifest'), format: false

  # Default health check as of Rails 7.1
  get 'up', to: 'rails/health#show', as: :rails_health_check
  get 'healthz', to: 'rails/health#show', constraints: lambda { |_request|
    Rails.logger.warn "DEPRECATION WARNING: The custom 'healthz' route is deprecated. Please use the Rails default 'up' instead."
    true
  }
end
