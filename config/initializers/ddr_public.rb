# frozen_string_literal: true

require 'ddr/public'

# Configure paging defaults
# Set max paging links to that set in Ddr::Public config (default 250).
# Set outer window to 0 to prevent direct access to deep pages.
Kaminari.configure do |config|
  config.max_pages = Ddr::Public.paging_limit_results.to_i
  config.outer_window = 0
end
