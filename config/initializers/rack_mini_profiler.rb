# frozen_string_literal: true

require 'rack-mini-profiler'

if Rails.env.production?
  # Uses MEMCACHE_SERVERS var - see config/environments/production.rb
  Rack::MiniProfiler.config.storage = Rack::MiniProfiler::MemcacheStore
end

Rack::MiniProfiler.config.assets_url = lambda { |name, _version, _env|
  ActionController::Base.helpers.asset_path(name)
}
