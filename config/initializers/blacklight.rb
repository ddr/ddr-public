# frozen_string_literal: true

ddr_defaults = {
  http_method: ENV.fetch('BLACKLIGHT_HTTP_METHOD', 'post').to_sym,
  solr_path: ENV.fetch('BLACKLIGHT_SOLR_PATH', 'published'),
  document_solr_path: ENV.fetch('BLACKLIGHT_DOCUMENT_SOLR_PATH', 'get/published'),
}.compact

Blacklight::Configuration.default_values.merge!(ddr_defaults)
