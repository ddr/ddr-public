{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "ddr-public.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "ddr-public.labels" -}}
helm.sh/chart: {{ include "ddr-public.chart" . }}
{{ include "ddr-public.selectorLabels" . }}
{{ include "ddr-public.chartLabels" . }}
{{- end }}

{{/*
Chart labels
*/}}
{{- define "ddr-public.chartLabels" -}}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "ddr-public.selectorLabels" -}}
app.kubernetes.io/name: app
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "ddr-public.appImage" -}}
image-registry.openshift-image-registry.svc:5000/{{ .Release.Namespace }}/app:{{ required "image.tag" .Values.image.tag }}
{{- end }}