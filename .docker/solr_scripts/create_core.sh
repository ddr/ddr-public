#!/bin/bash

core="ddr-public"
cd /var/solr/data
mkdir -p ${core}/conf
touch ${core}/core.properties
cp -r /solr_config/* ${core}/conf/
