#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

compose_opts="-p ddr-public-test -f docker-compose.yml -f docker-compose.test.yml"

run_app() {
    docker compose ${compose_opts} run \
		   -v "$(git rev-parse --show-toplevel):/opt/app-root" \
		   app "$@"
}

init() {
    run_app ./bin/rails db:setup ddr_public:destroy_index_docs ddr_public:seed:fixtures db:test:prepare
}

case $1 in
    test)
	init
	run_app /bin/bash -c 'bundle exec rake'
	docker compose ${compose_opts} down --remove-orphans
	;;
    shell)
	init
	run_app /bin/bash
	docker compose ${compose_opts} down --remove-orphans
	;;
    console)
	init
	run_app ./bin/rails c
	docker compose ${compose_opts} down --remove-orphans
	;;
    *)
	docker compose ${compose_opts} "$@"
	;;
esac
