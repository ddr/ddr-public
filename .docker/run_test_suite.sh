#!/bin/bash
cd "$(dirname ${BASH_SOURCE[0]})"
./test.sh run --rm app bundle exec rake db:reset spec
code=$?
./test.sh down
exit $code
