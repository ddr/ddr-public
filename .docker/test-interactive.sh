#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

./test.sh -f docker-compose.test-interactive.yml up -d
./test.sh -f docker-compose.test-interactive.yml exec app bin/rails db:setup ddr_public:destroy_index_docs ddr_public:seed:fixtures db:test:prepare
./test.sh -f docker-compose.test-interactive.yml exec app bash
./test.sh down
