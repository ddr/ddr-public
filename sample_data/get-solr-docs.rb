#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'
require_relative 'year_facet'

SAMPLE_DATA_FILES_BASE_PATH = '/sample_data/files'

puts 'Extracting Solr documents from Solr response JSON...'

object_types = %w[collections items components]
item_id_localid_mapping = {}

# Make a collections.json, items.json, & components.json file
# with only the docs we need in Solr.

# rubocop:disable Metrics/BlockLength
object_types.each do |obj|
  query_files = Dir.glob(File.join(File.expand_path(__dir__), "data/#{obj}-query*.json"))

  # Delete the existing collections.json, items.json, components.json files
  file_path = File.expand_path("./data/#{obj}.json", __dir__)
  FileUtils.rm_f(file_path)

  query_files.each do |f|
    solr_docs = JSON.parse(File.read(f)).dig('response', 'docs')

    # Clean up each Solr doc so it'll work as sample data
    solr_docs.each do |doc|
      # Remove fields that cause problems with indexing if they are already present
      doc.delete('_version_')
      doc.delete('score')

      id = doc['id']
      local_id = doc['local_id_ssi'] || id

      # Generate the year facet data from the date field for sample data.
      # Note this replicates logic from ddr-admin. See:
      # https://gitlab.oit.duke.edu/ddr/ddr-admin/-/blob/main/app/indexers/ddr/year_facet.rb
      doc['sample_data_year_isim'] = Ddr::YearFacet.call(doc['date_tsim']) if doc.key?('date_tsim')

      # ===========
      # COLLECTIONS
      # ===========
      # We don't want to list thousands of items in our structural metadata
      # for sample collections so let's truncate it to 100.
      if obj == 'collections' && doc.key?('structure_ss')
        structure = JSON.parse(doc.fetch('structure_ss'))
        structure_child_docs = structure.dig('default', 'contents')
        structure_child_docs&.slice!(100..)
        doc['structure_ss'] = structure.to_json
      end

      # =====
      # ITEMS
      # =====

      if obj == 'items'
        # Build up a uuid to local_id mapping for item objects; we'll need that later for
        # component documents b/c they do not encode their parent item's local_id
        # yet need it for their sample_data file paths (to match the export paths)
        item_id_localid_mapping[id] = local_id

        # Remove the item thumbnail data; it's not intentionally used by ddr-public
        # but can lead to missing images in the sample data set if present
        doc.delete('thumbnail_tsim') if doc.key?('thumbnail_tsim')

        # Fix up the IIIF manifest file path to match its location in sample_data/files.
        # NOTE ddr-admin currently cannot export IIIF manifests via Bulk Export, so
        # these have been manually saved and copied to the sample_data/files directory.
        if doc.key?('iiif_file_tsim')
          iiif_file_info = JSON.parse(doc['iiif_file_tsim'].first.sub(/^serialized-/, ''))

          iiif_file_path_new = ['disk://', SAMPLE_DATA_FILES_BASE_PATH, local_id,
                                "#{id}_iiif_manifest.json"].join('/')
          iiif_file_info['file_identifier']['id'] = iiif_file_path_new

          iiif_file_tsim_new = ['serialized-', iiif_file_info.to_json].join
          doc['iiif_file_tsim'][0] = iiif_file_tsim_new
        end
      end

      next unless obj == 'components'

      # ==========
      # COMPONENTS
      # ==========
      # Fix up file paths so they match the {item local_id}/{uuid}_{file}.{ext}
      # format that DDR-Admin batch exports conform to.

      parent_id = doc.fetch('is_part_of_ssim')&.first
      item_local_id = item_id_localid_mapping.fetch(parent_id, id)

      if doc.key?('multires_image_file_path_ssi')
        ptif_path_new = [SAMPLE_DATA_FILES_BASE_PATH, item_local_id, "#{id}_multires_image.tiff"].join('/')
        doc['multires_image_file_path_ssi'] = ptif_path_new
      end

      if doc.key?('derived_image_file_path_ssi')
        derived_image_path_new = [SAMPLE_DATA_FILES_BASE_PATH, item_local_id, "#{id}_derived_image.jpg"].join('/')
        doc['derived_image_file_path_ssi'] = derived_image_path_new
      end

      if doc.key?('multires_image_tsim')
        multires_image_file_info = JSON.parse(doc['multires_image_tsim'].first.sub(/^serialized-/, ''))

        multires_image_file_path_new = ['disk://', SAMPLE_DATA_FILES_BASE_PATH, item_local_id,
                                        "#{id}_multires_image.tiff"].join('/')
        multires_image_file_info['file_identifier']['id'] = multires_image_file_path_new

        multires_image_tsim_new = ['serialized-', multires_image_file_info.to_json].join
        doc['multires_image_tsim'][0] = multires_image_tsim_new
      end

      if doc.key?('derived_image_tsim')
        derived_image_file_info = JSON.parse(doc['derived_image_tsim'].first.sub(/^serialized-/, ''))

        derived_image_file_path_new = [SAMPLE_DATA_FILES_BASE_PATH, item_local_id,
                                       "#{id}_derived_image.jpg"].join('/')
        derived_image_file_info['file_identifier']['id'] = derived_image_file_path_new

        derived_image_tsim_new = ['serialized-', derived_image_file_info.to_json].join
        doc['derived_image_tsim'][0] = derived_image_tsim_new
      end

      if doc.key?('streamable_media_tsim')
        streamable_file_info = JSON.parse(doc['streamable_media_tsim'].first.sub(/^serialized-/, ''))

        media_type = streamable_file_info['media_type'] || ''
        extension = media_type.start_with?('video') ? 'mp4' : 'mp3'
        streamable_file_path_new = ['disk://', SAMPLE_DATA_FILES_BASE_PATH, item_local_id,
                                    "#{id}_streamable_media.#{extension}"].join('/')
        streamable_file_info['file_identifier']['id'] = streamable_file_path_new

        streamable_media_tsim_new = ['serialized-', streamable_file_info.to_json].join
        doc['streamable_media_tsim'][0] = streamable_media_tsim_new
      end

      if doc.key?('caption_tsim')
        caption_file_info = JSON.parse(doc['caption_tsim'].first.sub(/^serialized-/, ''))

        caption_file_path_new = ['disk://', SAMPLE_DATA_FILES_BASE_PATH, item_local_id,
                                 "#{id}_caption.vtt"].join('/')
        caption_file_info['file_identifier']['id'] = caption_file_path_new

        caption_tsim_new = ['serialized-', caption_file_info.to_json].join
        doc['caption_tsim'][0] = caption_tsim_new
      end

      if doc.key?('thumbnail_tsim')
        thumbnail_file_info = JSON.parse(doc['thumbnail_tsim'].first.sub(/^serialized-/, ''))

        media_type = thumbnail_file_info['media_type'] || ''
        extension = media_type.start_with?('image/png') ? 'png' : 'jpg'
        thumbnail_file_path_new = ['disk://', SAMPLE_DATA_FILES_BASE_PATH, item_local_id,
                                   "#{id}_thumbnail.#{extension}"].join('/')
        thumbnail_file_info['file_identifier']['id'] = thumbnail_file_path_new

        thumbnail_tsim_new = ['serialized-', thumbnail_file_info.to_json].join
        doc['thumbnail_tsim'][0] = thumbnail_tsim_new
      end

      next unless doc.key?('content_tsim')

      content_file_info = JSON.parse(doc['content_tsim'].first.sub(/^serialized-/, ''))

      # We'll use a fake content file for all components by default. This is
      # to keep the sample data footprint as small as possible. Most stuff in
      # DDR-Public doesn't use the content file anyway.

      # BUT, we do make a few exceptions here for PDF files that we want to
      # test in our sample data:
      components_with_real_content_files = %w[
        d57dae86-73ae-403e-bb7d-d22d1325743b
        e6f0f139-83a5-47e0-ba58-fc5ad639d79e
        f6f87dd7-48e5-4945-9dce-52855c4c9f6a
      ]

      if components_with_real_content_files.include?(id)
        content_file_path_new = ['disk://', SAMPLE_DATA_FILES_BASE_PATH, item_local_id,
                                 "#{id}_content.pdf"].join('/')
        content_file_info['file_identifier']['id'] = content_file_path_new

      else
        # Just use a generic small text file for the content file
        content_file_path_new = ['disk://', SAMPLE_DATA_FILES_BASE_PATH, 'sample_file.txt'].join('/')
        content_file_info['file_identifier']['id'] = content_file_path_new
        content_file_info['real_original_filename'] = content_file_info['original_filename']
        content_file_info['original_filename'] = 'sample_file.txt'
      end

      content_tsim_new = ['serialized-', content_file_info.to_json].join
      doc['content_tsim'][0] = content_tsim_new
    end

    # Create the collections.json, items.json, or components.json file (or append to it if it exists)
    file_exists = File.exist?(File.expand_path("./data/#{obj}.json", __dir__))
    File.write(File.expand_path("./data/#{obj}.json", __dir__), JSON.pretty_generate(solr_docs), mode: 'a')
    puts(file_exists ? "Added more records to #{obj}.json" : "Created #{obj}.json")
  end
end
# rubocop:enable Metrics/BlockLength
