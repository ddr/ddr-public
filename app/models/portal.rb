# frozen_string_literal: true

class Portal
  include Blacklight::Configurable
  include Blacklight::Searchable
  include DocumentModel::Searcher

  attr_accessor :local_id, :controller_name, :controller_scope

  def initialize(args = {})
    @local_id         = args.fetch(:local_id, nil)
    @controller_name  = args.fetch(:controller_name, nil)
    @controller_scope = args.fetch(:controller_scope, nil)
  end

  private

  def item_or_collection_documents(local_ids)
    local_ids ? item_or_collection_documents_search(local_ids).documents : []
  end

  def parent_collection_join_ids
    @parent_collection_join_ids ||= parent_collection_documents.map { |doc| "id-#{doc.id}" }
  end

  def parent_collection_document
    @parent_collection_document ||= parent_collection_documents.first if parent_collection_documents.count == 1
  end

  def parent_collection_documents
    @parent_collection_documents ||= parent_collections_search.documents || []
  end

  def multiple_parent_collections?
    parent_collections_count > 1
  end

  def parent_collections_count
    @parent_collections_count ||= parent_collections_search.total
  end

  def parent_collections_search
    query = search_builder.with(parent_collections_query).merge(rows: 10)
    @parent_collections_search ||= search_service.repository.search(query)
  end

  def parent_collections_query
    @parent_collections_query ||= if portal_local_ids
                                    { term_field_name: 'local_id_ssi',
                                      term_values: portal_local_ids }
                                  else
                                    { term_field_name: 'admin_set_ssi',
                                      term_values: portal_admin_sets }
                                  end
  end

  def child_item_documents
    @child_item_documents ||= child_items_search.documents || []
  end

  def child_items_count
    @child_items_count ||= child_items_search.total
  end

  def child_items_search
    query = if !local_id && controller_name == 'digital_collections'
              child_items_dc_query
            else
              child_items_query
            end
    @child_items_search ||= search_service.repository.search(query)
  end

  def child_items_query
    search_builder.with({ term_field_name: 'admin_policy_id_ssim',
                          term_values: parent_collection_join_ids })
                  .append(:include_only_items)
                  .merge(rows: '100', sort: solr_sort)
  end

  def child_items_dc_query
    search_builder.with({ term_field_name: 'admin_set_title_ssi',
                          term_values: ['Digital Collections'] })
                  .append(:include_only_items)
                  .merge(rows: '100', sort: solr_sort)
  end

  def item_or_collection_documents_search(local_ids)
    query = search_builder.with({ term_field_name: 'local_id_ssi',
                                  term_values: local_ids })
                          .append(:include_only_items_or_collections)
                          .merge(rows: '100', sort: solr_sort)
    search_service.repository.search(query)
  end

  def solr_sort
    'score desc,date_sort_si asc,title_ssi asc'
  end

  def search_builder
    @search_builder ||= SearchBuilder.new(query_processor_chain, @controller_scope)
  end

  def query_processor_chain
    %i[add_query_to_solr
       apply_gated_discovery
       add_boolean_terms_query]
  end

  def portal_admin_sets
    portal_view_config.try(:[], 'includes').try(:[], 'admin_sets')
  end

  def portal_local_ids
    portal_view_config.try(:[], 'includes').try(:[], 'local_ids')
  end

  def portal_view_config
    [local_id_configuration, dc_generic_view_config, controller_name_configuration].compact.first
  end

  def local_id_configuration
    portal_configuration.try(:[], local_id)
  end

  def controller_name_configuration
    portal_configuration.try(:[], controller_name)
  end

  def portal_configuration
    Rails.application.config.portal.try(:[], 'controllers')
  end

  def dc_generic_view_config
    { 'includes' => { 'local_ids' => [local_id] } } if local_id && controller_name == 'digital_collections'
  end
end
