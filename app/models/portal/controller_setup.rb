# frozen_string_literal: true

class Portal
  class ControllerSetup < Portal
    def view_path
      "app/views/ddr-portals/#{local_id || controller_name}"
    end

    # TODO: evaluate whether we should remove code supporting multi-collection portals
    # that aren't already defined by admin sets (e.g., dc). As of 2024, this feature
    # has not yet been used in practice.
    def parent_collection_join_ids
      return unless portal_local_ids

      @parent_collection_join_ids ||= parent_collection_documents.map { |doc| "id-#{doc.id}" }
    end
  end
end
