# frozen_string_literal: true

class User < ApplicationRecord
  # Connects this user object to Blacklights Bookmarks -- DEPRECATED?
  include Blacklight::User

  # N.B. Schema has uniqueness constraints on username and email.
  validates :username, uniqueness: { case_sensitive: false }
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/ }

  devise :database_authenticatable,
         :timeoutable,
         :omniauthable,
         omniauth_providers: [:openid_connect]

  # Duke policy group space
  POLICY_SPACE = 'duke:policies:duke-digital-repository'

  def self.from_omniauth(auth)
    find_or_initialize_by(username: auth.uid).tap do |user|
      user.password = Devise.friendly_token if user.new_record?
      user.email = auth.info.email
      user.groups = groups_from_auth(auth)
      user.save! if user.changed?
    end
  end

  def self.groups_from_auth(auth)
    groups = auth.extra.raw_info.groups || []
    groups.map! { |g| policy_group(g) }
    if (affiliation = auth.extra.raw_info.dukePrimaryAffiliation)
      groups << "duke.#{affiliation}"
    end
    groups.sort
  end

  def self.policy_group(group)
    "#{POLICY_SPACE}:#{group}"
  end

  def to_s
    # Changed from `#email` to `#username`.
    # The email attribute is not required!
    username
  end

  def agent
    username
  end
end
