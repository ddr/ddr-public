# frozen_string_literal: true

class Structure
  module StructureBehavior
    include Ddr::Public::Controller::SolrQueryConstructor

    attr_accessor :structure, :type

    def initialize(args = {})
      @structure = args[:structure]
      @type      = args[:type]
      super(docs_list)
    end

    def docs_list
      zip_docs_labels_and_order.select { |doc_list_item| doc_list_item[:doc].present? }
    end

    def docs
      if ids.present?
        ordered_documents(ids).compact
      else
        []
      end
    end

    def local_ids
      docs.map(&:local_id) if ids.present? || []
    end

    private

    REQUEST_ROWS_LIMIT = 50

    def zip_docs_labels_and_order
      ordered_documents(ids).zip(labels, order).map { |h| { doc: h[0], label: h[1], order: h[2] } }
    end

    # TODO: Is this used anywhere? Delete?
    def find_multires_image_file_paths
      docs.filter_map(&:multires_image_file_path) if docs.present?
    end

    # TODO: Is this used anywhere? Delete?
    def find_first_multires_image_file_path
      SolrDocument.find(ids.first).multires_image_file_path if ids.present?
    end

    def ordered_documents(ids)
      solr_documents = response_to_solr_docs(ids)
      ids.map { |pid| solr_documents.find { |doc| doc['id'] == pid } }
    end

    def response_to_solr_docs(ids)
      merged_response_docs(ids).map { |doc| SolrDocument.new(doc) }
    end

    def merged_response_docs(ids)
      ids_searches(ids).map { |response| response['response']['docs'] }.flatten
    end

    def ids_searches(ids)
      ids_queries(ids).map { |query| ids_search(query) }
    end

    def ids_queries(ids)
      sliced_ids(ids).map do |id_group|
        repository_id_query(id_group)
      end
    end

    # NOTE: Dividing long array of ids into multiple smaller
    #       groups of ids so as not to exceed request size limits.
    def sliced_ids(ids)
      ids.each_slice(REQUEST_ROWS_LIMIT).to_a
    end

    def ids_search(query)
      # FIXME: Is this really how we have to do this?
      conn = RSolr.connect(url: Blacklight.connection_config[:url])
      solr_path = Blacklight.default_configuration[:solr_path]

      conn.post(solr_path, params: {fq: query, q: '*:*', rows: REQUEST_ROWS_LIMIT})
    end

    def group_content(type = 'default')
      root_content.select { |c| c['type'] == type }.pluck('contents').flatten
    end

    def root_content(type = 'default')
      type_root_node(type).present? && type_root_node(type)['contents'] ? type_root_node(type)['contents'] : {}
    end

    def type_root_node(type = 'default')
      @structure ? @structure[type] : {}
    end
  end
end
