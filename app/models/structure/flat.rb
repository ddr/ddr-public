# frozen_string_literal: true

class Structure
  class Flat < SimpleDelegator
    include Structure::StructureBehavior

    def ids
      order_attribute_value('contents').flatten.filter_map { |component| component['repo_id'] }
    end

    def labels
      order_attribute_value('label')
    end

    def order
      order_attribute_value('order')
    end

    def label
      root_attribute_value('label')
    end

    private

    def order_attribute_value(attribute)
      root_content(@type).pluck(attribute)
    end

    def root_attribute_value(attribute)
      type_root_node(type)[attribute]
    end
  end
end
