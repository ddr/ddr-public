# frozen_string_literal: true

class Structure
  class Group < SimpleDelegator
    include Structure::StructureBehavior

    def ids
      type_group_content('contents').flatten.pluck('repo_id')
    end

    def labels
      type_group_content('label')
    end

    def order
      type_group_content('order')
    end

    def label
      attribute_value('label')
    end

    def id
      attribute_value('id')
    end

    private

    def type_group_content(attribute)
      group_content(@type).pluck(attribute)
    end

    def attribute_value(attribute)
      root_content.find { |node| node['type'] == @type }[attribute]
    end
  end
end
