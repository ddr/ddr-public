# frozen_string_literal: true

require 'mime/types'

# rubocop:disable Metrics/ClassLength
class SolrDocument
  include DocumentModel
  include Blacklight::Solr::Document
  include BlacklightOaiProvider::SolrDocument
  include DdrOaiDc

  # enable url_for method to get stream_url
  include Rails.application.routes.url_helpers

  extend Forwardable

  def_delegators :structures,
                 :caption_files,
                 :captions_paths,
                 :captions_urls,
                 :derivative_ids,
                 :derived_image_file_paths,
                 :first_downloadable_pdf_doc,
                 :first_media_doc,
                 :first_multires_image_file_ark,
                 :first_multires_image_file_path,
                 :first_readable_multires_image_doc,
                 :media_download_urls,
                 :media_paths,
                 :multires_image_file_arks,
                 :multires_image_file_paths,
                 :ordered_component_docs

  # Email uses the semantic field mappings below to generate the body of an email.
  # SolrDocument.use_extension(Blacklight::Document::Email)

  # SMS uses the semantic field mappings below to generate the body of an SMS email.
  # SolrDocument.use_extension(Blacklight::Document::Sms)

  # DublinCore uses the semantic field mappings below to assemble an OAI-compliant Dublin Core document
  # Semantic mappings of solr stored fields. Fields may be multi or
  # single valued. See Blacklight::Solr::Document::ExtendableClassMethods#field_semantics
  # and Blacklight::Solr::Document#to_semantic_values
  # Recommendation: Use field names from Dublin Core
  use_extension(Blacklight::Document::DublinCore)

  class NotFound < Ddr::Public::Error; end

  #
  # N.B. Class method '.find(id)' is inherited from Blacklight::Document::ActiveModelShim.
  #

  def has?(name)
    key?(name)
  end

  # Enables data access via calls like `doc.title_ssi`
  # Raises an exception if the called field doesn't exist unless an argument
  # providing a default value is given. This behavior is useful in detecting
  # errors in code calling SolrDocument, but we may need to change it to
  # simply log the error later on.
  # rubocop:disable Style/MissingRespondToMissing
  def method_missing(name, *args)
    return self[name] if key?(name)
    raise NoMethodError, "No such method '#{name}' on SolrDocument #{self['id']}" unless args

    args.first
  end
  # rubocop:enable Style/MissingRespondToMissing

  def file_extension(media_type)
    Ddr.preferred_file_extensions[media_type] || default_file_extension(media_type)
  end

  def default_file_extension(media_type)
    mimetypes = MIME::Types[media_type]

    mimetypes.first&.preferred_extension || 'bin'
  end

  # Data access methods

  def abstract
    Array(self['abstract_tsim']).first
  end

  def admin_policy
    @admin_policy ||= self.class.find(self['admin_policy_id_ssi'].gsub(/^id-/, '')) if admin_policy?
  end

  def admin_policy?
    key?('admin_policy_id_ssi')
  end

  def caption
    @caption ||= deserialize('caption_tsim')
  end

  def captioned?
    key?('caption_tsim')
  end

  def caption_type
    caption&.fetch('media_type')
  end

  def caption_extension
    return unless captioned?

    file_extension(caption_type)
  end

  def caption_path
    return unless captioned?

    file_path(caption['file_identifier']['id'])
  end

  def captions_url
    return unless captioned?

    url_for(id:, controller: 'captions', action: 'show', only_path: true)
  end

  def common_model_name
    self['common_model_name_ssi']
  end

  def content
    DdrFile.new(deserialize('content_tsim'))
  end

  def content_mime_type
    self['content_media_type_ssim'].first
  end

  def controller_scope
    ApplicationController.try(:current)
  end

  def description
    Array(self['description_tsim']).first
  end

  def embargo
    @embargo ||= datetime('embargo_dtsi')
  end

  def embargoed?
    !embargo.nil? && embargo > DateTime.now
  end

  def finding_aid
    return unless key?('ead_id_ssi')

    FindingAid.new(self['ead_id_ssi'])
  end

  def format
    Array(self['format_tsim'])
  end

  def humanized_date
    Array(self['date_tsim']).map do |date|
      if (edtf_date = Date.edtf(date))
        edtf_date.humanize
      else
        date
      end
    end
  end

  def iiif_file
    DdrFile.new(deserialize('iiif_file_tsim'))
  rescue StandardError
    nil
  end

  def multires_image_file_path
    self['multires_image_file_path_ssi']
  end

  def parent
    return unless parent?

    @parent ||= self.class.find(parent_id)
  end

  def parent_id
    self['is_part_of_ssim'] || self['is_member_of_collection_ssim']
  end

  def parent?
    parent_id.present?
  end

  # TODO: This is called in a couple of places, but is probably obsolete
  def pid
    self['id']
  end

  def portal_thumbnail
    portal_doc_config.try(:[], 'collection_local_id').try(:[], local_id_ssi).try(:[], 'thumbnail_image')
  end

  def public_controller
    public_controller = effective_configs.try(:[], 'controller')
    public_controller || 'catalog'
  end

  def public_collection
    effective_configs.try(:[], 'collection')
  end

  def public_action
    if dc_collection? && local_id_ssi
      'index'
    else
      'show'
    end
  end

  def public_id
    if dc_collection?
      local_id_ssi ? nil : id
    elsif dc_item?
      local_id_ssi || id
    else
      id
    end
  end

  def related_items
    item_relator_config ? item_relator_config.map { |config| RelatedItem.new({ document: self, config: }) } : []
  end

  def relation
    Array(self['relation_tsim'])
  end

  def research_help
    if self['research_help_contact_ssi']
      Contact.call(self['research_help_contact_ssi'])
    else
      inherited_research_help_contact
    end
  end

  def rights
    Array(self['rights_ssim'])
  end

  def rights_notes
    Array(self['rights_note_tsim'])
  end

  def rights_statement
    @rights_statement ||= RightsStatement.call(self)
  end

  def display_inline_pdf?
    first_downloadable_pdf_doc.present? && self['display_format_ssi'].blank?
  end

  def display_iiif_link?
    return true if common_model_name == 'Collection'

    @display_iiif_link ||= first_readable_multires_image_doc.present?
  end

  def sponsor
    Array(self['sponsor_tsim']).first
  end

  def sponsor_display
    @sponsor_display ||= sponsor ||
                         parent&.sponsor ||
                         collection&.sponsor ||
                         I18n.t('ddr.public.call_to_sponsor', default: 'Sponsor this Digital Collection')
  end

  def stream_url
    return unless streamable?

    file = DdrFile.new(streamable_media)

    if u = file.stream_url
      return u
    end

    url_for(id:, controller: 'stream', action: 'show', only_path: true)
  end

  # HACK
  def media_download_url
    return unless streamable?

    file = DdrFile.new(streamable_media)

    if u = file.download_url
      return u
    end

    url_for(id:, controller: 'stream', action: 'show', only_path: true)
  end

  def streamable_media
    @streamable_media ||= deserialize('streamable_media_tsim')
  end

  def streamable?
    key?('streamable_media_tsim')
  end

  def streamable_media_extension
    return unless streamable?

    file_extension(streamable_media_type)
  end

  def streamable_media_type
    streamable_media&.fetch('media_type')
  end

  def streamable_media_path
    return unless streamable?

    file_path(streamable_media['file_identifier']['id'])
  end

  def structure
    JSON.parse(self['structure_ss'])
  rescue StandardError
    nil
  end

  def default_struct_map
    structure['default'] || structure.values.first
  end

  def structures
    @structures ||= Structure.new(structure:, id:)
  end

  def techmd_file_size
    Array(self['techmd_file_size_lsi']).map do |file_size|
      ActiveSupport::NumberHelper.number_to_human_size(file_size)
    end
  end

  def techmd_image_height
    Array(self['techmd_image_height_isim']).map do |height|
      "#{height}px"
    end
  end

  def techmd_image_width
    Array(self['techmd_image_width_isim']).map do |width|
      "#{width}px"
    end
  end

  def thumbnail
    DdrFile.new(deserialize('thumbnail_tsim'))
  end

  def thumbnail?
    key?('thumbnail_tsim')
  end

  private

  def item_relator_config
    portal_view_config&.dig('item_relators') || generic_item_relators_config
  end

  def effective_configs
    [collection_id_configuration, dc_generic_configuration, admin_set_configuration].compact.first
  end

  def admin_set_configuration
    portal_doc_config.try(:[], 'admin_sets').try(:[], admin_policy_admin_set)
  end

  def collection_id_configuration
    portal_doc_config.try(:[], 'portals').try(:[], 'collection_local_id').try(:[], admin_policy_local_id)
  end

  def dc_generic_configuration
    return unless admin_policy_local_id && admin_policy_admin_set == 'dc'

    { 'controller' => 'digital_collections', 'collection' => admin_policy_local_id, 'item_id_field' => 'local_id' }
  end

  def dc_collection?
    admin_policy_admin_set == 'dc' && common_model_name == 'Collection'
  end

  def dc_item?
    admin_policy_admin_set == 'dc' && common_model_name == 'Item'
  end

  def portal_doc_config
    Rails.application.config.portal.try(:[], 'portals')
  end

  def portal_view_config
    Rails.application.config.portal.try(:[], 'controllers').try(:[], admin_policy_local_id)
  end

  def admin_policy_admin_set
    Rails.cache.fetch("admin_policy_admin_set/#{admin_policy_id_ssi}", expires_in: 1.day) do
      admin_policy&.admin_set_ssi
    end
  end

  def admin_policy_local_id
    Rails.cache.fetch("admin_policy_local_id/#{admin_policy_id_ssi}", expires_in: 1.day) do
      admin_policy&.local_id_ssi
    end
  end

  def generic_item_relators_config
    Rails.application.config.portal.dig('controllers', 'catalog', 'item_relators')
  end

  def inherited_research_help_contact
    return if common_model_name == 'Collection'

    admin_policy&.research_help
  end

  # Methods for getting Valkyrie-serialized values out of Solr
  def deserialize(field)
    return unless key?(field)

    value = Array(self[field]).first
    JSON.parse(value.gsub(/^serialized-/, ''))
  end

  def datetime(field)
    return unless key?(field)

    value = Array(self[field]).first.gsub(/^datetime-/, '')
    DateTime.parse(value)
  end

  # Valkyrie prefixes file paths with 'disk://', so that has to be stripped
  def file_path(id)
    id.to_s.gsub(%r{^disk://}, '')
  end
end
# rubocop:enable Metrics/ClassLength
