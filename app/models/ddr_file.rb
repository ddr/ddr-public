# frozen_string_literal: true

require 'aws-sdk-s3'

class DdrFile
  attr_reader :data

  # @param data [Hash]
  def initialize(data = {})
    @data = data&.deep_symbolize_keys
  end

  # @return
  def file_identifier
    return nil if null?

    data.fetch(:file_identifier)
  end

  # @return [String] media type
  def media_type
    return nil if null?

    data.fetch(:media_type, nil) # media_type is optional
  end

  # @return [String]
  # @raise [KeyError]
  def original_filename
    return nil if null?

    data.fetch(:original_filename)
  end

  # @return [String]
  def id
    file_identifier&.fetch(:id)
  end

  def file
    warn "[DEPRECATION] DdrFile#file is deprecated; use #io, #read, or #stream instead."

    io # This is still bad, but keeping to not break things ...
  end

  def read
    return nil if null?

    io { |f| f.read }
  end

  def io(&)
    return nil if null?

    io_proxy.io(&)
  end

  def stream
    if block_given?
      io_proxy.each { |chunk| yield(chunk) }
    else
      io_proxy.to_enum
    end
  end

  def file_path
    io_proxy.path if io_proxy.respond_to?(:path)
  end

  def file_size
    io_proxy.size
  end

  def stream_url
    io_proxy.stream_url if io_proxy.respond_to?(:stream_url)
  end

  def download_url
    io_proxy.download_url if io_proxy.respond_to?(:download_url)
  end

  def null?
    data.blank?
  end

  def disk?
    !null? && DiskIOProxy.handles?(id)
  end

  def s3?
    !null? && S3IOProxy.handles?(id)
  end

  def text?
    !null? && io_proxy.text?
  end

  private

  def io_proxy
    @io_proxy ||= io_proxy_class.new(id, media_type, original_filename)
  end

  def io_proxy_class
    [ DiskIOProxy, S3IOProxy ].each do |kls|
      return kls if kls.handles?(id)
    end

    raise "Unexpected file identifier: #{id}"
  end

  # @abstract
  class IOProxy
    attr_reader :id

    class_attribute :prefix

    def self.handles?(id)
      id.start_with?(prefix)
    end

    def initialize(id, media_type, filename)
      @id = id
      @media_type = media_type
      @filename = filename
    end

    def media_type
      @media_type || 'application/octet-stream'
    end

    def text?
      media_type.start_with?('text/')
    end

    def filename
      @filename || default_filename
    end

    def suffix
      @suffix ||= id.sub(prefix, '')
    end

    def io(&)
      raise NotImplementedError
    end

    def size
      raise NotImplementedError
    end

    def each(&)
      raise NotImplementedError
    end

    private

    def default_filename
      File.basename(suffix)
    end
  end

  class DiskIOProxy < IOProxy
    self.prefix = 'disk://'

    def path
      suffix
    end

    def size
      File.size(path)
    end

    def io(&)
      mode = text? ? 'r' : 'rb'

      unless block_given?
        warn "Calling #{self.class}#io without a block requires the caller to close the stream."

        return File.new(path, mode)
      end

      File.open(path, mode) { |f| yield(f) }
    end

    def each(&)
      io do |f|
        while f.gets(16.megabytes)
          yield $_
        end
      end
    end
  end

  class S3IOProxy < IOProxy
    self.prefix = 's3://'

    attr_reader :bucket_name, :object_key

    def initialize(*)
      super

      @bucket_name, @object_key = suffix.split('/', 2)
    end

    def each(&)
      get { |chunk, _headers| yield(chunk) }
    end

    def io(&)
      _io = get.body

      if block_given?
        yield _io
      else
        _io
      end
    end

    def size
      s3_object.size
    end

    def stream_url
      url(expires_in: 86400, disposition: 'inline')
    end

    def download_url
      url(expires_in: 7200, disposition: 'attachment')
    end

    private

    def default_filename
      object_key.gsub(/\//, '__')
    end

    def url(expires_in:, disposition:)
      s3_object.presigned_url(
        :get,
        expires_in:,
        response_content_disposition: "#{disposition}; filename=#{filename.inspect}",
        response_content_type: media_type
      )
    end

    def get(options={}, &)
      s3_object.get(options, &)
    end

    def s3_resource
      @s3_resource ||= Aws::S3::Resource.new
    end

    def s3_object
      @s3_object ||= bucket.object(object_key)
    end

    def bucket
      @bucket ||= s3_resource.bucket(bucket_name)
    end
  end
end
