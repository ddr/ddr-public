# frozen_string_literal: true

module DdrOaiDc
  def to_oai_dc
    xml = Builder::XmlMarkup.new
    xml.tag!('oai_dc:dc',
             'xmlns:oai_dc' => 'http://www.openarchives.org/OAI/2.0/oai_dc/',
             'xmlns:dc' => 'http://purl.org/dc/elements/1.1/',
             'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
             'xsi:schemaLocation' => %(http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd)) do
      oai_dc_hash.select { |field, _values| dublin_core_field_name? field }.each do |field, values|
        Array.wrap(values).each do |v|
          xml.tag! "dc:#{field}", v
        end
      end
    end
    xml.target!
  end

  private

  def oai_dc_hash
    @oai_dc_hash ||= oai_dc_mapping.transform_values do |value|
      [*call_or_fetch_value(value)].reject(&:empty?)
    end.compact_blank
  end

  def oai_dc_mapping
    @oai_dc_mapping ||= {
      contributor: 'contributor_tsim',
      coverage: 'spatial_tsim',
      creator: proc { oai_dc_creator },
      date: proc { oai_dc_date },
      description: 'description_tsim',
      format: 'format_tsim',
      identifier: proc { oai_identifier },
      language: 'language_tsim',
      publisher: 'publisher_tsim',
      # relation:,
      rights: proc { oai_dc_rights },
      source: 'collection_title_ssi',
      subject: proc { oai_dc_subject },
      title: proc { oai_dc_title },
      type: 'type_tsim'
    }
  end

  def oai_identifier
    if oai_thumbnail.present?
      Array(self['permanent_url_tsim']).concat(Array(
                                                 "#{Ddr::Public.root_url}#{oai_thumbnail}"
                                               ))
    else
      Array(self['permanent_url_tsim'])
    end
  end

  def oai_thumbnail
    @oai_thumbnail ||= Thumbnail::MultiresItem.new({ document: self }).public_thumbnail_path
  end

  def oai_dc_date
    Array(self['date_tsim']).map do |date|
      if (edtf_date = Date.edtf(date))
        edtf_date.humanize
      else
        date
      end
    end
  end

  def oai_dc_creator
    Array(self['creator_tsim']).concat(
      Array(self['company_tsim'])
    )
  end

  def oai_dc_rights
    Array(self['rights_tsim']).concat(
      Array(self['rights_note_tsim'])
    )
  end

  def oai_dc_subject
    Array(self['subject_tsim']).concat(
      Array(self['product_tsim'])
    )
  end

  def oai_dc_title
    Array(self['title_ssi']).concat(
      Array(self['alternative_tsim'])
    )
  end

  # used primarily by document extensions for fetching values
  # in field mappings where the mapped value is either a
  # SolrDocument field or a proc.
  def call_or_fetch_value(value)
    value.respond_to?(:call) ? value.call : fetch(value, '')
  end
end
