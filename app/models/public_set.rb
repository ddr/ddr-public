# frozen_string_literal: true

class PublicSet < BlacklightOaiProvider::SolrSet
  def description
    if label && value
      "This set includes works in the #{value} set."
    else
      'No description available.'
    end
  end

  def name
    "#{@label.titleize}: #{@value}"
  end
end
