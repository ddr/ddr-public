# frozen_string_literal: true

class AcqMaterialsSearchBuilder < SearchBuilder
  self.default_processor_chain += %i[exclude_digital_collections]
end
