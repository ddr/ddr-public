# frozen_string_literal: true

require 'cancan'

class Ability
  include CanCan::Ability

  # DUPLICATION: Relevant permissions from legacy module Ddr::Auth::Permissions
  PERMISSIONS = %i[discover read download].freeze

  # Built-in groups
  # N.B. 'registered' and 'duke.all' are effectively equivalent
  # since we only authenticate Duke users.
  PUBLIC_GROUP = 'public'
  REGISTERED_GROUP = 'registered'
  DUKE_GROUP = 'duke.all'
  DEFAULT_GROUPS = [PUBLIC_GROUP, REGISTERED_GROUP, DUKE_GROUP].freeze
  SUPERUSER_GROUP = ENV.fetch('SUPERUSER_GROUP', 'duke:policies:duke-digital-repository:superusers')

  # DUPLICATION: Ddr::Resource::FILE_FIELDS
  # We should be able to get this from the API schema
  FILE_TYPES = %i[ caption
                   content
                   derived_image
                   extracted_text
                   fits_file
                   iiif_file
                   intermediate_file
                   multires_image
                   streamable_media
                   struct_metadata
                   thumbnail].freeze

  FILE_PERMISSIONS = FILE_TYPES.map { |t| :"download_#{t}" }.freeze

  # Mapping of permissions to index fields
  INDEX_FIELDS = {
    discover: 'effective_role_ssim',
    read: 'effective_read_ssim',
    download: 'effective_download_ssim'
  }.freeze

  attr_reader :user

  # @param user [User]
  def initialize(user = nil)
    @user = user
    apply_permissions
  end

  def agents
    groups.dup.tap do |a|
      a << user.agent if authenticated?
    end
  end

  def groups
    if authenticated?
      user.groups + DEFAULT_GROUPS
    else # anonymous
      [PUBLIC_GROUP]
    end
  end

  def anonymous?
    user.nil?
  end

  def authenticated?
    !anonymous?
  end

  def superuser?
    groups.include?(SUPERUSER_GROUP)
  end

  def apply_permissions
    #
    # Grant each permission if any of the ability's agents
    # (i.e., the user plus groups) is listed in the permission
    # index field.
    #
    PERMISSIONS.each do |permission|
      can permission, SolrDocument do |doc|
        index_field = INDEX_FIELDS.fetch(permission)
        having_permission = doc.fetch(index_field, [])
        agents.any? { |a| having_permission.include?(a) }
      end

      can permission, String do |doc_id|
        warn '[DEPRECATION] Authorization by document ID is deprecated. Use the SolrDocument instance instead.'
        doc = SolrDocument.find(doc_id)
        can? permission, doc
      rescue SolrDocument::NotFound => _e
        # N.B. https://duldev.atlassian.net/browse/DDR-2537
        # We silence the exception, as it should be caught elsewhere
        # and could be confusing to raise it here.
        # The important point is to deny authorization?
        false
      end
    end

    # DDK-342 IIIF manifest download only requires discover/metadata-only permission.
    can :download_iiif_file, [SolrDocument, String] do |doc|
      can?(:discover, doc)
    end

    # Can download files if can read doc
    # (except content, see below)
    can FILE_PERMISSIONS, [SolrDocument, String] do |doc|
      can?(:read, doc)
    end

    # Deny download of content if cannot download doc
    cannot :download_content, [SolrDocument, String] do |doc|
      cannot?(:download, doc)
    end

    # Deny read, download to embargoed resources
    cannot %i[read download], SolrDocument, embargoed?: true
  end
end
