# frozen_string_literal: true

class RelatedItem
  module RelatedItemBehavior
    extend ActiveSupport::Concern

    included do
      include Blacklight::Configurable
      include Blacklight::Searchable
      include DocumentModel::Searcher
      include Ddr::Public::Controller::SolrQueryConstructor
    end

    def initialize(args = {})
      @document = args[:document]
      @config   = args[:config]
    end

    def name
      @config['name']
    end

    def related_documents
      @related_documents ||= title_sorted_documents
    end

    def related_documents_count
      @related_documents_count ||= document_count
    end

    private

    def sort
      'title_ssi asc'
    end
  end
end
