# frozen_string_literal: true

class RelatedItem
  class IdReference
    include RelatedItem::RelatedItemBehavior

    def solr_query
      return if solr_query_values.blank?

      { 'id_related_items' => "#{@document.id}|#{source_solr_field}|#{target_solr_field}",
        'sort' => sort,
        'q' => '',
        'search_field' => 'all_fields' }
    end

    private

    def source_solr_field
      @source_solr_field ||= @config['field']
    end

    def target_solr_field
      @target_solr_field ||= @config['id_field']
    end

    def solr_query_values
      @solr_query_values ||= document_field_values.join(',').to_s if document_field_values.present?
    end

    def title_sorted_documents
      solr_response&.documents
    end

    def document_count
      solr_response.total.to_i if solr_response
    end

    def solr_response
      return if document_field_values.blank?

      @solr_response ||= search_service.repository.search(
        searcher.with({ term_field_name: target_solr_field,
                        term_values: document_field_values })
                .merge(sort:)
      )
    end

    def document_field_values
      @document[source_solr_field]
    end
  end
end
