# frozen_string_literal: true

class RelatedItem
  extend Forwardable

  def_delegators :@related_item,
                 :name,
                 :related_documents,
                 :related_documents_count,
                 :solr_query

  def initialize(args = {})
    @document     = args[:document]
    @config       = args[:config]
    @related_item = RelatedItem::IdReference.new({ document: @document, config: @config })
  end
end
