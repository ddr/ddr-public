# frozen_string_literal: true

# We need to exclude metadata-only items from OAI-PMH harvests.
class OaiItemSearchBuilder < ItemSearchBuilder
  self.default_processor_chain += %i[apply_gated_read]
end
