# frozen_string_literal: true

class Current < ActiveSupport::CurrentAttributes
  attribute :current_ability
end
