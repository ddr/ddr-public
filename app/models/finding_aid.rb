# frozen_string_literal: true

class FindingAid
  attr_reader :ead_id

  def initialize(ead_id)
    @ead_id = ead_id
  end

  # TODO: use permalinks in the future when all finding aids have ARKs
  def url
    [Ddr::Public.finding_aid_base_url, 'catalog', ead_id].join('/')
  end

  def data?
    doc.present?
  end

  def title
    doc&.fetch('normalized_title_ssm', [])&.first
  end

  def repository
    doc&.fetch('repository_ssim', [])&.first
  end

  def collection_date_span
    doc&.fetch('normalized_date_ssm', [])&.first
  end

  def collection_number
    doc&.fetch('unitid_ssm', [])&.first
  end

  def collection_title
    doc&.fetch('title_ssm', [])&.first
  end

  def extent
    doc&.fetch('extent_ssm', [])&.join('; ')
  end

  def abstract
    first_abstract = doc&.fetch('abstract_html_tesm', [])&.first
    ActionController::Base.helpers.strip_tags(first_abstract)
  end

  private

  def doc
    @doc ||= Rails.cache.fetch("arclight_collection_data_#{ead_id}", expires_in: 1.day) do
      Timeout.timeout(5) do
        JSON.parse(URI.parse(arclight_collection_data_url).open.read)
      end
    rescue Timeout::Error
      Rails.logger.error("Timeout fetching data from #{arclight_collection_data_url}")
      return nil
    rescue StandardError => e
      Rails.logger.error("Error fetching data from #{arclight_collection_data_url}: #{e.message}")
      return nil
    end
  end

  def arclight_collection_data_url
    "#{url}/raw.json"
  end
end
