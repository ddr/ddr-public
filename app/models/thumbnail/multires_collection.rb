# frozen_string_literal: true

class Thumbnail
  class MultiresCollection
    include Rails.application.routes.url_helpers
    include Blacklight::Configurable
    include Blacklight::Searchable
    include Ddr::Public::Controller::IiifImagePaths

    attr_accessor :document, :size, :region

    def initialize(args)
      @document = args.fetch(:document, nil)
      @size     = args.fetch(:size, '!350,350')
      @region   = args.fetch(:region, 'full')
    end

    def thumbnail?
      return false unless document['common_model_name_ssi'] == 'Collection'

      collection_multires_image_file_ark?
    end

    def thumbnail_path
      return unless collection_multires_image_file_ark?

      iiif_image_path(collection_multires_image_file_ark, { size:, region: })
    end

    private

    def collection_multires_image_file_ark?
      collection_multires_image_file_ark.present?
    end

    def collection_multires_image_file_ark
      return unless item_document && cached_read_public_ability(item_document)

      item_document.first_multires_image_file_ark
    end

    def item_document
      @item_document ||=
        if configured_local_id
          search_service = Blacklight::SearchService.new(config: blacklight_config,
                                                         user_params: { q: "local_id_ssi:#{configured_local_id}" },
                                                         search_builder_class: ::ItemSearchBuilder)
          (_response, doc_list) = search_service.search_results
          doc_list.first
        elsif document.items
          document.items.first
        end
    end

    def configured_local_id
      document.portal_thumbnail.try(:[], 'local_id')
    end

    def controller_scope
      @controller_scope ||= CatalogController.new
    end

    def cached_read_public_ability(doc)
      cache_key = "public_read_ability_#{doc.id}"
      Rails.cache.fetch(cache_key, expires_in: 5.minutes) do
        public_user.can?(:read, doc)
      end
    end

    def public_user
      @public_user ||= Ability.new
    end
  end
end
