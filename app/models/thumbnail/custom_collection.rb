# frozen_string_literal: true

class Thumbnail
  class CustomCollection
    attr_accessor :document

    def initialize(args)
      @document = args.fetch(:document, nil)
    end

    def thumbnail?
      return false unless document.common_model_name_ssi == 'Collection'

      custom_thumbnail_file_name?
    end

    def thumbnail_path
      "ddr-portals/#{document.local_id_ssi}/#{custom_thumbnail_file_name}" if custom_thumbnail_file_name?
    end

    private

    def custom_thumbnail_file_name?
      custom_thumbnail_file_name ? true : false
    end

    def custom_thumbnail_file_name
      document.portal_thumbnail.try(:[], 'custom_image')
    end
  end
end
