# frozen_string_literal: true

class Thumbnail
  class RepositoryGenerated
    attr_accessor :document

    def initialize(args)
      @document = args.fetch(:document, nil)
    end

    def thumbnail?
      repository_generated_thumbnail?
    end

    def thumbnail_path
      return unless repository_generated_thumbnail?

      Rails.application
           .routes
           .url_helpers
           .download_path(id: document.id, ddr_file_type: 'thumbnail')
    end

    private

    def repository_generated_thumbnail?
      document&.thumbnail?
    end
  end
end
