# frozen_string_literal: true

class Thumbnail
  class MultiresComponent
    include Rails.application.routes.url_helpers
    include Ddr::Public::Controller::IiifImagePaths

    attr_accessor :document, :size, :region

    def initialize(args)
      @document = args.fetch(:document, nil)
      @size     = args.fetch(:size, '!350,350')
      @region   = args.fetch(:region, 'full')
    end

    def thumbnail?
      return false unless document['common_model_name_ssi'] == 'Component'

      component_multires_image_file_ark?
    end

    def thumbnail_path
      return unless component_multires_image_file_ark?

      iiif_image_path(component_multires_image_file_ark, { size:, region: })
    end

    private

    def component_multires_image_file_ark?
      component_multires_image_file_ark.present?
    end

    def component_multires_image_file_ark
      document.permanent_id_ssi if document.multires_image_file_path_ssi
    end
  end
end
