# frozen_string_literal: true

class Thumbnail
  class AudiovisualItem
    attr_accessor :document

    def initialize(args)
      @document = args.fetch(:document, nil)
    end

    def thumbnail?
      document.display_format_ssi =~ /audio|video/ && repository_generated_thumbnail? ? true : false # rubocop:disable Style/IfWithBooleanLiteralBranches
    end

    def thumbnail_path
      return unless thumbnail?

      Rails.application
           .routes
           .url_helpers
           .download_path(id: first_media_doc_id, ddr_file_type: 'thumbnail')
    end

    private

    def repository_generated_thumbnail?
      document.first_media_doc&.thumbnail?
    end

    def first_media_doc_id
      document.first_media_doc.id
    end
  end
end
