# frozen_string_literal: true

class Thumbnail
  class MultiresItem
    include Rails.application.routes.url_helpers
    include Ddr::Public::Controller::IiifImagePaths

    attr_accessor :document, :size, :region, :public_read_permission

    def initialize(args)
      @document = args.fetch(:document, nil)
      @size     = args.fetch(:size, '!350,350')
      @region   = args.fetch(:region, 'full')
      @public_read_permission = args.fetch(:public_read_permission, false)
    end

    def thumbnail?
      return false unless document['common_model_name_ssi'] == 'Item'

      item_multires_image_file_ark?
    end

    def thumbnail_path
      return unless item_multires_image_file_ark?

      iiif_image_path(item_multires_image_file_ark, { size:, region: })
    end

    alias public_thumbnail_path thumbnail_path

    private

    def item_multires_image_file_ark?
      item_multires_image_file_ark.present?
    end

    def item_multires_image_file_ark
      document.first_multires_image_file_ark
    end
  end
end
