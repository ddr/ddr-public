# frozen_string_literal: true

class Thumbnail
  class Default
    attr_accessor :document

    def initialize(args)
      @document = args.fetch(:document, nil)
    end

    def thumbnail?
      default_thumbnail?
    end

    def thumbnail_path
      default_thumbnail if default_thumbnail?
    end

    private

    def default_thumbnail?
      default_thumbnail ? true : false
    end

    def default_thumbnail
      thumbnails.compact.first
    end

    def thumbnails
      [single_component_item_thumbnail,
       mime_type_thumbnail,
       display_format_thumbnail,
       common_model_name_thumbnail,
       fallback_thumbnail]
    end

    def single_component_item_thumbnail
      mime_type_thumbnail(document.components.first) if document.components && document.components.length == 1
    end

    def mime_type_thumbnail(doc = document)
      return unless doc.common_model_name_ssi == 'Component'

      case doc.content_mime_type
      when /^image/
        'ddr-icons/image.png'
      when /^video/
        'ddr-icons/video.png'
      when /^audio/
        'ddr-icons/audio.png'
      when %r{^(multipart|application)/(x-)?g?zip.?}
        'ddr-icons/zip.png'
      when 'application/rtf'
        'ddr-icons/rtf.png'
      when 'application/postscript'
        'ddr-icons/postscript.png'
      when 'application/vnd.google-earth.kmz'
        'ddr-icons/kml.png'
      when 'application/vnd.google-earth.kml+xml'
        'ddr-icons/kml-xml.png'
      when 'application/x-sas'
        'ddr-icons/x-sas.png'
      when %r{^application/(x-|vnd.)?(ms)?-?excel}
        'ddr-icons/xls.png'
      when %r{application/vnd\.openxmlformats-officedocument\.spreadsheetml.?}
        'ddr-icons/xls.png'
      when %r{^application/(x-|vnd.)?(ms)?-?powerpoint}
        'ddr-icons/ppt.png'
      when %r{application/vnd\.openxmlformats-officedocument\.presentationml.?}
        'ddr-icons/ppt.png'
      when %r{^application/msword}
        'ddr-icons/doc.png'
      when %r{application/vnd\.openxmlformats-officedocument\.wordprocessingml.?}
        'ddr-icons/doc.png'
      when %r{^application/(x-)?pdf}
        'ddr-icons/pdf.png'
      when /^application/
        'ddr-icons/binary.png'
      when 'message/rfc822'
        'ddr-icons/email.png'
      when 'text/html'
        'ddr-icons/html.png'
      when %r{^text/(comma-separated-values|csv)}
        'ddr-icons/csv.png'
      when %r{^text/.?}
        'ddr-icons/txt.png'
      end
    end

    def display_format_thumbnail
      return unless document.has?('display_format_ssi')

      case document.display_format_ssi
      when 'image'
        'ddr-icons/image.png'
      when 'video'
        'ddr-icons/video.png'
      when 'audio'
        'ddr-icons/audio.png'
      when 'collection'
        'ddr-icons/multiple.png'
      end
    end

    def common_model_name_thumbnail
      case document.common_model_name_ssi
      when 'Collection'
        'ddr-icons/multiple.png'
      end
    end

    def fallback_thumbnail
      'ddr-icons/default.png'
    end
  end
end
