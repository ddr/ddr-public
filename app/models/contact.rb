# frozen_string_literal: true

Contact = Struct.new(:slug, :name, :short_name, :url, :phone, :email, :ask, :partial, keyword_init: true) do
  def self.config
    @config ||= YAML.load_file(File.expand_path('../../config/aux/contact.yml', __dir__))
  end

  def self.keystore
    @keystore ||= config.to_h { |entry| [entry['slug'], new(entry).freeze] }.freeze
  end

  def self.all
    keystore.values
  end

  def self.call(slug)
    keystore.fetch(slug)
  rescue KeyError => _e
    raise Ddr::Public::Error, "Contact slug '#{slug}' not found."
  end

  def self.keys
    keystore.keys
  end

  def to_s
    name
  end
end
