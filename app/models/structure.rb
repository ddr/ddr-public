# frozen_string_literal: true

# rubocop:disable Metrics/ClassLength
class Structure
  def initialize(args = {})
    @structure = args[:structure]
    @id        = args[:id]
  end

  def default
    @default ||= Structure::Flat.new(structure: @structure, type: 'default')
  end

  def images
    @images ||= Structure::Group.new(structure: @structure, type: 'Images')
  end

  def files
    @files ||= Structure::Group.new(structure: @structure, type: 'Documents')
  end

  def media
    @media ||= Structure::Group.new(structure: @structure, type: 'Media')
  end

  def directories
    @directories ||= Structure::Directory.new(structure: @structure)
  end

  def multires_image_file_arks
    @multires_image_file_arks ||=
      select_multires_image_file_paths.filter_map(&:permanent_id_ssi)
  end

  def derived_image_file_paths
    @derived_image_file_paths ||=
      select_multires_image_file_paths.filter_map(&:derived_image_file_path_ssi)
  end

  def first_multires_image_file_ark
    @first_multires_image_file_ark ||=
      (first_readable_multires_image_doc.permanent_id_ssi if first_readable_multires_image_doc.present?)
  end

  def multires_image_file_paths
    @multires_image_file_paths ||=
      select_multires_image_file_paths.filter_map(&:multires_image_file_path)
  end

  def first_multires_image_file_path
    @first_multires_image_file_path ||=
      (first_readable_multires_image_doc.multires_image_file_path if first_readable_multires_image_doc.present?)
  end

  def media_paths
    @media_paths ||= select_media_paths.filter_map(&:stream_url)
  end

  def media_download_urls
    @media_download_urls ||= select_media_paths.filter_map(&:media_download_url)
  end

  def first_media_doc
    @first_media_doc ||= SolrDocument.find(select_first_media_doc_id.first) if select_first_media_doc_id.present?
  rescue Blacklight::Exceptions::RecordNotFound => _e
    nil
  end

  # used by AV player, loaded aynchronously
  def captions_urls
    @captions_urls ||= select_caption_docs.filter_map(&:captions_url)
  end

  # used by interactive transcript parser
  def captions_paths
    @captions_paths ||= select_caption_docs.filter_map(&:caption_path)
  end

  def caption_files
    @caption_files ||= select_caption_docs.select(&:captioned?).map { |doc| DdrFile.new(doc.caption) }
  end

  def ordered_component_docs
    @ordered_component_docs ||= files.docs
  end

  def derivative_ids
    @derivative_ids ||= default.local_ids.presence
  end

  private

  def select_multires_image_file_paths
    @select_multires_image_file_paths ||=
      if default.docs.any?
        default.docs
      elsif images.docs.any?
        images.docs
      else
        []
      end
  end

  def select_caption_docs
    @select_caption_docs ||=
      if default.docs.any?
        default.docs
      elsif media.docs.any?
        media.docs
      else
        [] << find_solr_document # for a component
      end
  end

  def select_first_media_doc_id
    @select_first_media_doc_id ||=
      if default.ids.any?
        default.ids
      elsif media.ids.any?
        media.ids
      else
        []
      end
  end

  def select_media_paths
    @select_media_paths ||=
      if default.docs.any?
        default.docs
      elsif media.docs.any?
        media.docs
      else
        []
      end
  end

  def first_readable_multires_image_doc
    @first_readable_multires_image_doc ||= select_potential_multires_image_ids[0..9].filter_map do |id|
      doc = SolrDocument.find(id)
      if doc['multires_image_file_path_ssi'].present? &&
         Current.current_ability.can?(:read, doc)
        break doc
      end
    rescue SolrDocument::NotFound, Blacklight::Exceptions::RecordNotFound => e
      Rails.logger.error(e)
      nil
    end
  end

  def select_potential_multires_image_ids
    if default.ids.any?
      default.ids
    elsif images.ids.any?
      images.ids
    else
      # If structural metadata lacks ids, use the current
      # document's id. This will especially apply to components.
      [] << @id
    end
  end

  # Find the first component with a PDF file that the user can download
  def first_downloadable_pdf_doc
    @first_downloadable_pdf_doc ||= select_potential_pdf_ids[0..9].filter_map do |id|
      doc = SolrDocument.find(id)
      if doc['content_media_type_ssim'] == ['application/pdf'] &&
         Current.current_ability.can?(:download, doc)
        break [doc]
      end
    rescue SolrDocument::NotFound, Blacklight::Exceptions::RecordNotFound => e
      Rails.logger.error(e)
      nil
    end.first
  end

  def select_potential_pdf_ids
    if default.ids.any?
      default.ids
    elsif files.ids.any?
      files.ids
    else
      # If structural metadata lacks ids, use the current
      # document's id. This will especially apply to components.
      [] << @id
    end
  end

  def find_solr_document
    @find_solr_document ||= SolrDocument.find(@id)
  end
end
# rubocop:enable Metrics/ClassLength
