# frozen_string_literal: true

class DcSearchBuilder < SearchBuilder
  self.default_processor_chain += %i[exclude_components
                                     filter_by_parent_collections
                                     include_only_digital_collections]
end
