# frozen_string_literal: true

class SearchBuilder < Blacklight::SearchBuilder
  include Blacklight::Solr::SearchBuilderBehavior
  include BlacklightRangeLimit::RangeLimitBuilder

  self.default_processor_chain += %i[ filter_by_related_items apply_gated_discovery truncate_long_queries ]

  def truncate_long_queries(solr_parameters)
    return unless solr_parameters[:q] && solr_parameters[:q].length > 200

    solr_parameters[:q] = solr_parameters[:q].truncate(200, omission: '')
  end

  def include_only_collections(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << 'common_model_name_ssi:Collection'
  end

  def include_only_items(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << 'common_model_name_ssi:Item'
  end

  def include_only_components(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << 'common_model_name_ssi:Component'
  end

  def include_only_items_or_collections(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << '{!terms f=common_model_name_ssi ' \
                            'method=booleanQuery}Item,Collection'
  end

  def exclude_components(solr_parameters)
    return if blacklight_params[:show_components] == 'true'

    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << '-common_model_name_ssi:Component'
  end

  def include_only_multires_images(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << 'multires_image_file_path_ssi:[* TO *]'
  end

  # TODO: evaluate whether we should remove code supporting multi-collection portals
  # that aren't already defined by admin sets (e.g., dc). As of 2024, this feature
  # has not yet been used in practice.
  def filter_by_parent_collections(solr_parameters)
    return unless scope.context[:parent_collection_join_ids]
    return unless blacklight_params.fetch('action', nil) == 'index' ||
                  blacklight_params.fetch('action', nil) == 'facet'

    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << '{!terms f=admin_policy_id_ssim ' \
                            'method=booleanQuery}' \
                            "#{scope.context[:parent_collection_join_ids]&.join(',')}"
    solr_parameters
  end

  def filter_by_related_items(solr_parameters)
    return if blacklight_params[:id_related_items].blank?

    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << "{!terms f=#{related_item_id_target_field} " \
                            "method=booleanQuery}#{related_item_ids.join(',')}"
    solr_parameters
  end

  def include_only_digital_collections(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << 'admin_set_title_ssi:"Digital Collections"'
  end

  def exclude_digital_collections(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << '-admin_set_title_ssi:"Digital Collections"'
  end

  def add_boolean_terms_query(solr_parameters)
    return unless blacklight_params.key?(:term_field_name) &&
                  blacklight_params.key?(:term_values) &&
                  blacklight_params[:term_values].respond_to?(:join)

    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << "{!terms f=#{blacklight_params[:term_field_name]} " \
                            "method=booleanQuery}#{blacklight_params[:term_values].join(',')}"
    solr_parameters[:q] = '*:*'
    solr_parameters
  end

  def apply_gated_discovery(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << gated_discovery_filters
  end

  # This will exclude "metadata-only" records
  def apply_gated_read(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << gated_read_filters
  end

  def include_only_first_result(solr_parameters)
    solr_parameters[:rows] = 1
  end

  def include_all_rows(solr_parameters)
    solr_parameters[:rows] = 999_999_999
  end

  def sort_by_title(solr_parameters)
    solr_parameters[:sort] = 'title_ssi ASC'
  end

  # Mimic how ddr-admin sorts an item's components in its structural metadata
  def sort_by_sequence(solr_parameters)
    solr_parameters[:sort] = 'local_id_ssi ASC,' \
                             'title_ssi ASC,' \
                             'original_filename_ssi ASC,' \
                             'ingestion_date_dtsi ASC,' \
                             'created_at_dtsi ASC'
  end

  private

  def related_item_ids
    doc = SolrDocument.find(related_item_source_id)
    doc[related_item_id_source_field]
  rescue SolrDocument::NotFound
    []
  end

  def related_item_source_id
    split_related_item_values.first
  end

  def related_item_id_source_field
    split_related_item_values[1]
  end

  def related_item_id_target_field
    split_related_item_values.last
  end

  def split_related_item_values
    @split_related_item_values ||= blacklight_params[:id_related_items].split('|')
  end

  def gated_discovery_filters
    # https://solr.apache.org/guide/solr/latest/query-guide/other-parsers.html#terms-query-parser
    "{!terms f=effective_role_ssim method=booleanQuery}#{Current.current_ability.agents.join(',')}"
  end

  def gated_read_filters
    # https://solr.apache.org/guide/solr/latest/query-guide/other-parsers.html#terms-query-parser
    "{!terms f=effective_read_ssim method=booleanQuery}#{Current.current_ability.agents.join(',')}"
  end
end
