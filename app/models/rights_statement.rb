# frozen_string_literal: true

RightsStatement = Struct.new(:title, :url, :short_title, :feature, :reuse_text, keyword_init: true) do
  def self.config
    @config ||= YAML.load_file(File.expand_path('../../config/aux/rights_statement.yml', __dir__))
  end

  def self.keystore
    @keystore ||= config.to_h { |entry| [entry['url'], new(entry).freeze] }.freeze
  end

  def self.all
    keystore.values
  end

  def self.call(doc)
    return if doc.rights.empty?

    keystore.fetch(doc.rights.first)
  rescue KeyError => _e
    raise Ddr::Public::Error, "Rights statement '#{doc.rights.first}' not found."
  end

  def self.keys
    keystore.keys
  end

  def to_s
    title
  end
end
