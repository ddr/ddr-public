# frozen_string_literal: true

module DocumentModel
  class Component
    include Blacklight::Configurable
    include Blacklight::Searchable
    include DocumentModel::Searcher
    include DocumentModel::HtmlTitle

    attr_accessor :document

    def initialize(args = {})
      @document = args[:document]
    end

    def collection
      collection_search.documents.first
    end

    def item
      item_search.documents.first
    end

    def metadata_header
      I18n.t('ddr.public.metadata_header.component')
    end

    def html_title
      html_title = []
      html_title += [document&.title_ssi&.truncate(150) +
        html_title_qualifier(document.title_ssi,
                             document.permanent_id_ssi || document.public_id),
                     item&.title_ssi&.truncate(100),
                     I18n.t('blacklight.application_name')]
      html_title.join(' / ')
    end


    private

    def collection_search
      query = searcher.with({ term_field_name: 'id',
                              term_values: [document.collection_id_ssim] })
      search_service.repository.search(query)
    end

    def item_search
      query = searcher.with({ term_field_name: 'id',
                              term_values: [document.is_part_of_ssim] })
      search_service.repository.search(query)
    end
  end
end
