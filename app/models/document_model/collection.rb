# frozen_string_literal: true

module DocumentModel
  class Collection
    include Blacklight::Configurable
    include Blacklight::Searchable
    include DocumentModel::Searcher
    include DocumentModel::HtmlTitle

    attr_accessor :document

    def initialize(args = {})
      @document = args[:document]
    end

    def collection
      document
    end

    def items
      items_search.documents
    end

    def item_count
      items_search.total
    end

    def metadata_header
      I18n.t('ddr.public.metadata_header.collection')
    end

    def html_title
      html_title = []
      html_title += [document.title_ssi.truncate(150)]
      html_title += [document['admin_set_title_ssi'].truncate(100)] if document['admin_set_title_ssi'].present?
      html_title += [I18n.t('blacklight.application_name')]
      html_title.join(' / ')
    end

    private

    def items_search
      query = searcher.with({ term_field_name: 'is_member_of_collection_ssim',
                              term_values: [document.id] })
      search_service.repository.search(query)
    end
  end
end
