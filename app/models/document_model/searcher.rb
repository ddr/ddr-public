# frozen_string_literal: true

module DocumentModel
  module Searcher
    def searcher
      if @document.controller_scope
        @search_builder ||= SearchBuilder.new(query_processor_chain, @document.controller_scope)
      else
        search_builder
      end
    end

    attr_reader :params

    def query_processor_chain
      %i[add_query_to_solr
         apply_gated_discovery
         add_boolean_terms_query]
    end

    private

    def search_state
      @search_state ||= search_state_class.new(params, blacklight_config, self)
    end

    def search_state_class
      Blacklight::SearchState
    end
  end
end
