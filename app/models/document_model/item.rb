# frozen_string_literal: true

module DocumentModel
  class Item
    include Blacklight::Configurable
    include Blacklight::Searchable
    include DocumentModel::Searcher
    include DocumentModel::HtmlTitle

    attr_accessor :document

    def initialize(args = {})
      @document = args[:document]
    end

    def collection
      collection_search.documents.first
    end

    def components
      component_search.documents
    end

    def component_count
      component_search.total
    end

    def metadata_header
      case document.display_format_ssi
      when 'folder'
        I18n.t('ddr.public.metadata_header.folder_item')
      end
    end

    def parent_directories
      dir_and_sibling_hash_from_collection[:parent_directories] if dir_and_sibling_hash_from_collection
    end

    def directory_siblings
      dir_sibling_search.documents.sort_by(&:title_ssi) if dir_and_sibling_hash_from_collection
    end

    def html_title
      html_title = []
      html_title += [document&.title_ssi&.truncate(150) +
        html_title_qualifier(document.title_ssi,
                             document.permanent_id_ssi || document.public_id_ssi),
                     collection&.title_ssi&.truncate(100),
                     I18n.t('blacklight.application_name')]
      html_title.join(' / ')
    end

    private

    def dir_and_sibling_hash_from_collection
      collection&.structures&.directories&.item_id_lookup&.fetch(document.id, {})
    end

    def directory_sibling_ids
      dir_and_sibling_hash_from_collection[:files] if dir_and_sibling_hash_from_collection
    end

    def dir_sibling_search
      query = searcher.with({ term_field_name: 'id',
                              term_values: directory_sibling_ids })
                      .merge(rows: '1000')
      search_service.repository.search(query)
    end

    def collection_search
      query = searcher.with({ term_field_name: 'id',
                              term_values: [document.is_member_of_collection_ssim] })
                      .merge(rows: '2000')
      search_service.repository.search(query)
    end

    def component_search
      query = searcher.with({ term_field_name: 'is_part_of_ssim',
                              term_values: [document.id] })
                      .merge(rows: '2000')
      search_service.repository.search(query)
    end
  end
end
