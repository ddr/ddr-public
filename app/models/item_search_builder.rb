# frozen_string_literal: true

class ItemSearchBuilder < SearchBuilder
  self.default_processor_chain -= %i[add_facetting_to_solr] # remove facets
  self.default_processor_chain -= %i[add_range_limit_params]
  self.default_processor_chain += %i[include_only_items]
end
