# frozen_string_literal: true

class Thumbnail
  attr_accessor :document,
                :size,
                :region,
                :read_permission,
                :public_read_permission

  def initialize(args)
    @document               = args.fetch(:document, nil)
    @size                   = args.fetch(:size, '!350,350')
    @region                 = args.fetch(:region, 'full')
    @read_permission        = args.fetch(:read_permission, false)
    @public_read_permission = args.fetch(:public_read_permission, false)
  end

  def thumbnail_path
    thumbnail&.thumbnail_path
  end

  private

  def thumbnail
    thumbnail_classes.each do |klass|
      thumbnail_klass = klass.new({ document:,
                                    size:,
                                    region:,
                                    public_read_permission: })
      return thumbnail_klass if thumbnail_klass.thumbnail?
    end
    nil
  end

  def thumbnail_classes
    read_permission? ? configured_thumbnails : Array(default_thumbnails)
  end

  def configured_thumbnails
    thumbnails = thumbnail_precedence.map do |thumbnail|
      "Thumbnail::#{thumbnail}".safe_constantize
    end
    thumbnails.delete(Thumbnail::MultiresItem) unless document['common_model_name_ssi'] == 'Item'
    thumbnails.push default_thumbnails
  end

  def thumbnail_precedence
    %w[CustomCollection
       MultiresItem
       MultiresComponent
       MultiresCollection
       RepositoryGenerated
       AudiovisualItem
       Default]
  end

  def default_thumbnails
    Thumbnail::Default
  end

  def read_permission?
    read_permission
  end
end
