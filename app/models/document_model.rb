# frozen_string_literal: true

module DocumentModel
  def collection
    @collection ||= document_model.try(:collection)
  end

  def item
    @item ||= document_model.try(:item)
  end

  def items
    @items ||= document_model.try(:items)
  end

  def item_count
    @item_count ||= document_model.try(:item_count)
  end

  def components
    @components ||= document_model.try(:components)
  end

  def component_count
    @component_count ||= document_model.try(:component_count)
  end

  # Returns an array used by the FontAwesome icon helper
  # [type, icon]
  # Some types (far: regular, fas: solid, fab: brands, etc.) for some
  # icons are not available for us in the free version of FontAwesome
  def display_format_icon
    case display_format_ssi
    when 'image'
      %w[far clone]
    when 'folder'
      %w[far folder-open]
    when 'video'
      %w[fas film]
    when 'audio'
      %w[fas headphones]
    else
      %w[far file]
    end
  end

  def parent_directories
    @parent_directories ||= document_model.try(:parent_directories)
  end

  def directory_siblings
    @directory_siblings ||= document_model.try(:directory_siblings)
  end

  def html_title
    document_model.try(:html_title) || I18n.t('blacklight.application_name')
  end

  def metadata_header
    document_model.try(:metadata_header) || I18n.t('ddr.public.metadata_header.default')
  end

  private

  def document_model
    document_model_class.new({ document: self })
  end

  def document_model_class
    "DocumentModel::#{current_document_model}".safe_constantize
  end

  def current_document_model
    document_models.find { |model| model == common_model_name }
  end

  def document_models
    %w[Collection Item Component]
  end
end
