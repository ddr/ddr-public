// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require activestorage
//
// Required by Blacklight
//= require popper
//= require bootstrap
//= require blacklight/blacklight
//= require 'blacklight_range_limit_custom'
//= require 'blacklight_range_limit'

// Don't include AV & file-tree JS in the global application.js
//= stub av
//= stub file-trees

//= require_tree .
