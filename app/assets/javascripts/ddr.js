// Trigger Bootstrap UI actions
// See http://getbootstrap.com/javascript/

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover({ trigger: 'hover'});
    $('[data-toggle="popover-click"]').popover({ trigger: 'focus'});
    $('[data-toggle="popover-click"]').on('click', function(e) {e.preventDefault(); return true;});

    $('[data-toggle="fade-element"]').on('focus blur mouseenter mouseleave', function(e){
        $(('#')+$(this).attr('data-target-id')).fadeToggle(100);
    });


    // Activate tabs
    $('.nav-tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    // Link to activate tab from in page, e.g., another tab's content
    $('a[data-toggle="tab"]:not(.nav-tabs *)').click(function (e) {
        e.preventDefault();
        $('a[href="' + $(this).attr("href") + '"]').tab('show');
    });

    // Bookmarkable tabs
    $(document).ready(function() {
        // show active tab on reload
        if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

        // remember the hash in the URL without jumping
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
           if(history.pushState) {
                history.pushState(null, null, '#'+$(e.target).attr('href').substr(1));
           } else {
                location.hash = '#'+$(e.target).attr('href').substr(1);
           }
        });
    });

    // Make js tree links active
    $('.directory-tree').on('activate_node.jstree', function(e,data){
        window.location.href = data.node.a_attr.href;
    });

    // Make js tree links keyboard-navigable for accessibility
    // For some reason they are NOT by default.
    $('.directory-tree').on('ready.jstree after_open.jstree', function(e,data){
        $('.jstree-anchor').attr('tabindex', 0);
    });

    /* Check if a user has a touchscreen device and has touched it */
    window.addEventListener('touchstart', function onFirstTouch() {

      /* Turn off tooltips */
      $('[data-toggle="tooltip"]').tooltip('destroy');

      window.removeEventListener('touchstart', onFirstTouch, false);
    }, false);

});

$( document ).ready(function() {

  // Change the search form's action URL & placeholder for the selected dropdown option
  function setSearchBoxScope() {
    var action = $('#search_scope').val();
    if(action) {
      $('#mast-search').attr('action', action);
      $('#search_box').attr('placeholder', $('#search_scope').find(':selected').data('placeholder'));
    } else {
      $('#search_box').attr('placeholder', $('#search_box').data('default-placeholder'));
    }
  }

  // Make the search scope select dropdown only as wide as the currently selected option.
  // This is some imperfect math, but seems to work OK for now.
  function setSearchBoxSelectWidth() {
    option_width = $('select#search_scope option:selected').text().length;
    $('select#search_scope').css('width', (option_width * 0.5 + 1.5) + 'rem');
  }

  // Set the scope both on initial pageload and when a different option is selected.
  setSearchBoxScope();
  setSearchBoxSelectWidth();

  $('#search_scope').change(function() {
    setSearchBoxScope();
    setSearchBoxSelectWidth();
  });

  // Disable the empty "Search In:" prompt value
  $('#search_scope option').filter('[value=""],:not([value])').attr('disabled', 1);

  // Re-route the search if it's an external-to-DDR search scope
  $('#mast-search').submit(function(e) {
    var action = $(this).attr('action');
    if(action == 'external') {
      e.preventDefault();
      e.returnValue = false;
      var $baseUrl = $('#search_scope').find(':selected').data('baseurl');
      var $mySearchTerm = $( "#search_box" ).val();
      location.href = [$baseUrl, $mySearchTerm].join('');
    }
  });

  /* DUL masthead primary navigation menu toggle */
  $('a#full-menu-toggle').on('click', function(e) {
      e.preventDefault();
      $(this).toggleClass('menu-active');
      $('#dul-masthead-region-megamenu').slideToggle();
  });

  /* Date range facet hack: don't show this facet unless the current    */
  /* result set has at least one year item. There doesn't appear to     */
  /* be a way to configure this in Blacklight Range Limit plugin though */
  /* it seems sensible. */

  $('#facets .blacklight-year_facet_iim').each(function(){
    if (!($(this).find('.distribution').length)) {
      $(this).hide();
    }
  });
});
