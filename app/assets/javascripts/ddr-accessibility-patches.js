/* ============================================================ */
/* Accessibility: Apply some band-aids on a11y issues.          */
/* NOTE: These changes fix some issues client-side that can't   */
/* easily be addressed otherwise.                               */
/* ============================================================ */

$(function () {

  /* Fix duplicate "documents" id present in search results pages that   */
  /* include Matching Collections (Blacklight Gallery plugin doesn't     */
  /* anticipate being used more than once on a page, thus provides no    */
  /* way to use an ID other than "documents").                           */
  $('#matching-collections #documents').attr('id', 'collection-documents');

  /* Fix blacklight-range-limit histogram accessibility issue */
  /* https://dequeuniversity.com/rules/axe/4.8/aria-hidden-focus */
  $('#facet-year_facet_iim .chart_js').attr('aria-hidden', 'false');
});
