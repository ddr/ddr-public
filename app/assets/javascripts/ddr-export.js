$(document).ready(function() {
  /* Listen for PDF/ZIP image export status when clicking the buttons */
  $('#image-export-pdf, #image-export-zip').click(function(e) {
    // Set the button in its loading state
    $(this).button('loading');
    var item_id = $(this).data('item');
    var cookie_name = 'duke_ddr_export_begin_' + item_id;

    // Check every 250ms for the session cookie that indicates the export has begun
    var export_listener = window.setInterval(function () {
      // If found, stop checking & reset the button to default state
      if (document.cookie.indexOf(cookie_name) !== -1) {
          // Wait 2 sec, then reset the buttons & clear the cookie
          // by setting a past expiration date
          setTimeout(function(){
            $('#image-export-pdf').button('reset');
            $('#image-export-zip').button('reset');
            document.cookie = cookie_name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
          }, 2000);
          clearInterval(export_listener);
      }
    }, 250);
  });
});
