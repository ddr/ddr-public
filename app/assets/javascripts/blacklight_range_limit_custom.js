// DUL CUSTOM configuration for Blacklight Range Limit (year facet).
// Note: there are a couple bugs/limitations in the gem re: customization.
// These changes apply custom colors/options even when in the View Larger
// modal view. However these configs still do not get applied in a search
// result display when a year range has been applied.
// https://github.com/projectblacklight/blacklight_range_limit#configuration

// Note: rgb(255,217,96) is $dandelion yellow from the Duke brand guide.

function applyRangeLimitColors() {
  $('.blrl-plot-config').data('plot-config', {
      selection: { color: '#aaaaaa' },
      colors: ['rgba(255, 217, 96, 1)'],
      series: { lines: { fillColor: 'rgba(255, 217, 96, 0.7)' }},
      grid: { color: '#aaaaaa', tickColor: '#aaaaaa', borderWidth: 1 }
  });
}

$(function () {
  applyRangeLimitColors();

  /* Upon clicking "View Larger" in the BL range limit facet, we need to */
  /* re-set the custom config within the modal. */
  $('#blacklight-modal').on('shown.bs.modal', function () {
    applyRangeLimitColors();
  });
});
