// Support stateful buttons (a BS3 feature removed from BS4). See:
// https://getbootstrap.com/docs/4.6/migration/#buttons
// https://www.robertmullaney.com/2018/10/25/continue-using-data-loading-text-buttons-bootstrap-4-jquery/
(function($) {
  $.fn.button = function(action) {
    if (action === 'loading' && this.data('loading-text')) {
      this.data('original-text', this.html()).html(this.data('loading-text'));
      this.addClass('disabled');
    }
    if (action === 'reset' && this.data('original-text')) {
      this.html(this.data('original-text'));
      this.removeClass('disabled');
    }
  };
}(jQuery));
