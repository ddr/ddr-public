// Example:
// https://github.com/sul-dlss/sul-embed/blob/main/app/javascript/packs/m3.js

import MiradorViewer from './src/modules/mirador';

MiradorViewer.init();
