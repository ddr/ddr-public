// Examples:
// https://github.com/sul-dlss/sul-embed/blob/main/app/javascript/src/modules/m3_viewer.js
// https://github.com/wragge/mirador-ghpages/blob/main/src/index.js

// See all config settings:
// https://github.com/ProjectMirador/mirador/blob/master/src/config/settings.js

import Mirador from 'mirador/dist/es/src/index';
import { miradorImageToolsPlugin } from 'mirador-image-tools';
import keyboardShortcutsPlugin from 'mirador-keyboardshortcuts/es';
import bugfixScrollWheel from './bugfix-scroll-wheel';
import accessibilityPatches from './accessibility-patches';
import updateDownloadMenu from './update-download-menu';

export default {
  init() {
    const el = document.getElementById('ddr-mirador-setup');
    const miradorData = el.dataset;
    // eslint-disable-next-line prefer-destructuring
    const manifestUrl = miradorData.manifestUrl;

    const config = {
      id: 'my-mirador',
      // Optionally configure the OpenSeadragon viewer
      // See https://openseadragon.github.io/docs/OpenSeadragon.html#.Options
      osdConfig: {},
      selectedTheme: 'dark',
      themes: {
        dark: {
          palette: {
            mode: 'dark',
            primary: {
              main: '#a1b70d',
            },
            secondary: {
              main: '#a1b70d',
            },
            shades: {},
          },
        },
        light: {
          palette: {
            mode: 'light',
            primary: {
              main: '#004ccc',
            },
            secondary: {
              main: '#004ccc',
            },
            shades: {},
          },
        },
      },
      window: {
        allowClose: false,
        allowFullscreen: true,
        allowMaximize: false,
        defaultSideBarPanel: 'info',
        hideWindowTitle: true,
        sideBarOpenByDefault: false,
        // See mirador-image-tools:
        // https://github.com/ProjectMirador/mirador-image-tools/blob/master/src/plugins/MiradorImageTools.js
        imageToolsEnabled: false,
        imageToolsOpen: true,
        views: [
          { key: 'single' },
          { key: 'book' },
          { key: 'gallery' },
        ],
      },
      windows: [
        {
          id: 'main',
          loadedManifest: manifestUrl,
          maximized: true,
          thumbnailNavigationPosition: miradorData.thumbnailNavigationPosition,
        },
      ],
      workspace: {
        showZoomControls: true,
      },
      workspaceControlPanel: {
        enabled: false,
      },
      // Keyboard controls via https://github.com/dbmdz/mirador-keyboardshortcuts
      // Defaults: https://github.com/dbmdz/mirador-keyboardshortcuts/blob/main/src/state/events.js
      // Mirador core includes some keyboard shortcuts, but none for page-turning:
      // * shift+up: zoom in
      // * shift+down: zoom out
      // * shift+left, a: pan left
      // * shift+right, d: pan right
      // * up: pan up
      // * down: pan down
      // * r: rotate 90deg
      keyboardShortcuts: {
        shortcutMapping: {
          // Plugin doesn't seem to support Mac control, command, option keys
          'navigate-to-first-canvas': 'ctrl+left,f',
          'navigate-to-last-canvas': 'ctrl+right,l',
          // Default includes [space] for next; but space should be page down
          'navigate-to-next-canvas': 'right',
          'navigate-to-previous-canvas': 'left',
          'switch-to-book-view': 'b',
          'switch-to-gallery-view': 'g',
          'switch-to-single-image-view': 's',
          // Default includes [enter] for full screen, which breaks normal keyboard nav
          'toggle-fullscreen': '',
        },
      },
    };

    const plugins = [
      ...miradorImageToolsPlugin,
      keyboardShortcutsPlugin,
      bugfixScrollWheel,
      accessibilityPatches,
      updateDownloadMenu
    ];

    Mirador.viewer(config, plugins);
  },
};
