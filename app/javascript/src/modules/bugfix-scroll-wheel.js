import { takeEvery } from 'redux-saga/effects';
import ActionTypes from 'mirador/dist/es/src/state/actions/action-types';

// This DUL-developed plugin allows a user to scroll past the viewer UI
// when using a mousewheel or trackpad without the viewer automatically
// intercepting the event for its zoom feature. It does however activate
// wheel zooming once the user has clicked into the viewer.

// Mirador/OSD could be configured to globally permit or disable wheel zooming
// but there are no built-in options to make this de/activate upon focus. See:
// osdConfig: { gestureSettingsMouse: { scrollToZoom: false } }

// Some ideas derived from:
// https://github.com/ProjectMirador/mirador/issues/3488
// https://github.com/duke-libraries/ddr-public/commit/21ca66d093625584ca2902984c436d796ef52e6b

const wheelHandler = (evt) => {
  evt.stopPropagation();
};

const setScrollWheelZoomable = (windowId) => {
  const osdCanvas = document.querySelector('div.openseadragon-canvas');
  if (!osdCanvas) {
    return;
  }
  const currentWindow = document.getElementById(windowId);
  const scrollWheelZoomable = currentWindow.classList.contains('scroll-wheel-zoomable');

  if (scrollWheelZoomable) {
    // Enable mousewheel zoom
    osdCanvas.removeEventListener('wheel', wheelHandler, { passive: true, capture: true });
  } else {
    // Disable mousewheel zoom
    osdCanvas.addEventListener('wheel', wheelHandler, { passive: true, capture: true });
  }
};

function onSetCanvas(action) {
  setScrollWheelZoomable(action.windowId);
}

function onFocusWindow(action) {
  // This doesn't fire until you've clicked or focused on an element in the viewer.
  const focusedWindow = document.getElementById(action.windowId);
  focusedWindow.classList.add('scroll-wheel-zoomable');

  setScrollWheelZoomable(action.windowId);
}

// See all available Mirador ActionTypes:
// https://github.com/ProjectMirador/mirador/blob/master/src/state/actions/action-types.js

function* rootSaga() {
  yield takeEvery(ActionTypes.SET_CANVAS, onSetCanvas);
  yield takeEvery(ActionTypes.FOCUS_WINDOW, onFocusWindow);
}

export default {
  component: () => {},
  saga: rootSaga,
};
