import { takeEvery } from 'redux-saga/effects';
import ActionTypes from 'mirador/dist/es/src/state/actions/action-types';

// This DUL-developed plugin is used to address some accessibility issues
// that are introduced by Mirador. This needs to be revisited when future
// versions of Mirador are released.
// https://github.com/ProjectMirador/mirador/issues?q=is%3Aissue+is%3Aopen+label%3AAccessibility

const miradorElement = document.querySelector('#my-mirador');

function onSetCanvas(action) {

  // Document should not have more than one main landmark
  // https://dequeuniversity.com/rules/axe/4.9/landmark-no-duplicate-main
  // Main landmark should not be contained in another landmark
  // https://dequeuniversity.com/rules/axe/4.9/landmark-main-is-top-level

  // Change the tag name of Mirador's <main> element to a <section>:
  const mainElement = miradorElement.querySelector('main.mirador-viewer');
  if (mainElement) {
    changeTagName(mainElement, 'section');
  }
}

// Function to change the tag name of an element
function changeTagName(element, newTagName) {
  // Create a new element with the desired tag name
  const newElement = document.createElement(newTagName);

  // Copy attributes from the original element
  for (const attr of element.attributes) {
    newElement.setAttribute(attr.name, attr.value);
  }

  // Copy children from the original element
  while (element.firstChild) {
    newElement.appendChild(element.firstChild);
  }

  // Replace the original element with the new element
  element.parentNode.replaceChild(newElement, element);

  return newElement;
}

// See all available Mirador ActionTypes:
// https://github.com/ProjectMirador/mirador/blob/master/src/state/actions/action-types.js

function* rootSaga() {
  yield takeEvery(ActionTypes.SET_CANVAS, onSetCanvas);
}

export default {
  component: () => {},
  saga: rootSaga,
};
