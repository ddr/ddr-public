import { takeEvery } from 'redux-saga/effects';
import ActionTypes from 'mirador/dist/es/src/state/actions/action-types';

// This DUL-developed plugin is used to dynamically update the download
// menu to reflect the currently viewed image in the viewer.

function onSetCanvas(action) {
  // Using the current canvas id from the IIIF manifest, e.g.:
  // http://localhost:3000/iiif/ark%3A%2F87924%2Fr4fj2c59h/canvas

  // Change the download links to reflect the current canvas, e.g.:
  // /iiif/ark:/87924/r4fj2c59h/full/full/0/default.jpg
  const canvasIdUrl = decodeURIComponent(action.canvasId);
  const arkRoot = canvasIdUrl.replace(/^https?:\/\/[^\/]+/, '').replace(/\/[^\/]*$/, '/');
  const downloadLinks = document.querySelectorAll('#download-menu a.download-link-single');

  downloadLinks.forEach(link => {
    const currentHref = link.getAttribute('href');
    const newHref = currentHref.replace(/\/iiif\/ark:\/[^\/]+\/[^\/]+\//, arkRoot);
    link.setAttribute('href', newHref);
  });
}

// See all available Mirador ActionTypes:
// https://github.com/ProjectMirador/mirador/blob/master/src/state/actions/action-types.js

function* rootSaga() {
  yield takeEvery(ActionTypes.SET_CANVAS, onSetCanvas);
}

export default {
  component: () => {},
  saga: rootSaga,
};
