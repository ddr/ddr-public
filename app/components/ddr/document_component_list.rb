# frozen_string_literal: true

# DUL CUSTOM component to render a document in list view (search results).
# Core Blacklight just uses the same Blacklight::DocumentComponent as an
# item page does, and the way thumbnails render in that markup is challenging
# to style.

# https://github.com/projectblacklight/blacklight/blob/v7.33.0/app/components/blacklight/document_component.rb

module Ddr
  class DocumentComponentList < Blacklight::DocumentComponent
  end
end
