# frozen_string_literal: true

# DUL CUSTOM override of Blacklight core module to insert more logic for rendering
# metadata values, especially for more control over for things like fields that have long
# values or a lot of values. Also applies auto_link and simple_format helpers where
# configured.

# See:
# https://github.com/projectblacklight/blacklight/blob/release-7.x/app/presenters/blacklight/rendering/join.rb
module Blacklight
  module Rendering
    class Join < AbstractStep
      include ActionView::Helpers::TextHelper
      include ActionView::Helpers::TagHelper
      include ActionView::Helpers::UrlHelper

      DEFAULT_SEPARATOR_OPTIONS =
        { words_connector: '; ', two_words_connector: '; ', last_word_connector: '; ' }.freeze

      def render
        if options.fetch(:no_html, false) || context.action_name != 'show' || Array(values).length == 1
          next_step(join_values)
        else
          next_step(wrap_values)
        end
      end

      private

      def join_values
        options = config.separator_options || DEFAULT_SEPARATOR_OPTIONS
        vals = apply_auto_link(values)

        next_step(values.map { |x| html_escape(x) }.to_sentence(options).html_safe)
        next_step(vals.to_sentence(options).html_safe)
      end

      def html_escape(*)
        ERB::Util.html_escape(*)
      end

      def wrap_values
        plain_value = Array(values)
        value_lengths = plain_value.map { |x| x.to_s.length }
        count = plain_value.length
        average_length = value_lengths.sum.to_f / count

        if average_length > 150
          metadata_list_content_tags(values, 'long-metadata-values')
        elsif count >= 7
          metadata_list_content_tags(values, 'many-metadata-values')
        elsif count <= 6
          metadata_list_content_tags(values, 'few-metadata-values')
        end
      end

      def metadata_list_content_tags(vals, ul_class)
        return if vals.blank?

        vals = apply_simple_formatting(vals)
        vals = vals.map do |sfv|
          content_tag(:li, auto_link(sfv))
        end.join.html_safe
        content_tag(:ul, vals, class: ul_class).html_safe
      end

      def apply_auto_link(vals)
        return vals unless config.auto_link == true

        vals.map { |v| auto_link(v) }
      end

      def apply_simple_formatting(vals)
        return vals unless config.simple_format == true

        vals.map { |v| simple_format(v) }
      end
    end
  end
end
