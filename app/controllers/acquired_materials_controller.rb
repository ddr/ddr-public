# frozen_string_literal: true

class AcquiredMaterialsController < PortalController
  configure_blacklight do |config|
    config.search_builder_class = AcqMaterialsSearchBuilder
  end
end
