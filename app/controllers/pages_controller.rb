# frozen_string_literal: true

class PagesController < ApplicationController
  layout 'ddr'
  def homepage; end
end
