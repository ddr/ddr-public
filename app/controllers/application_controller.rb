# frozen_string_literal: true

class ApplicationController < ActionController::Base
  # Needed so that a SolrDocument can know about its
  # active controller scope. Any additional queries can then
  # use the same search builder context.
  cattr_accessor :current
  before_action { ApplicationController.current = self }
  after_action  { ApplicationController.current = nil  }

  before_action :store_user_location!, if: :storable_location?

  before_action :set_current_ability

  before_action :authorize_profiler if Rails.env.production?

  # Adds a few additional behaviors into the application controller
  include Blacklight::Controller
  layout :determine_layout if respond_to? :layout

  rescue_from CanCan::AccessDenied do |_exception|
    render file: Rails.public_path.join('403.html').to_s, status: :forbidden, layout: false
  end

  before_action :authenticate_user!, if: :authentication_required?

  before_action :configure_devise_params, if: :devise_controller?

  # Please be sure to implement current_user and user_session. Blacklight depends on
  # these methods in order to perform user specific actions.

  # layout 'blacklight'

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery prepend: true

  # We need `cached_read_public_ability' and `public_user' as helpers
  include Ddr::Public::Controller::IiifImagePaths

  helper_method :is_embed?
  helper_method :cached_read_public_ability
  helper_method :public_user

  def render_bookmarks_control?
    has_user_authentication_provider? && current_or_guest_user.present? && !is_embed?
  end

  def is_embed?
    params.fetch(:embed, false) == 'true'
  end

  def configure_devise_params
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[email username])
    devise_parameter_sanitizer.permit(:sign_in, keys: %i[email username])
  end

  # Replaces Ddr::Auth::RoleBasedAccessControlsEnforcement#current_ability.
  def current_ability
    @current_ability ||= Ability.new(current_user)
  end

  # Ported from Ddr::Auth::RoleBasedAccessControlsEnforcement, with changes.
  def enforce_show_permissions
    authorize! :discover, params[:id]
  end

  private

  def set_current_ability
    Current.current_ability = current_ability
  end

  def storable_location?
    request.get? && is_navigational_format? && !devise_controller? && !request.xhr?
  end

  def store_user_location!
    store_location_for(:user, request.fullpath)
  end

  def after_sign_in_path_for(resource_or_scope)
    stored_location_for(resource_or_scope) || super
  end

  protected

  def forbidden
    render file: Rails.public_path.join('403.html').to_s, status: :forbidden, layout: false
  end

  def not_found
    render file: Rails.public_path.join('404.html').to_s, status: :not_found, layout: false
  end

  def server_error
    render file: Rails.public_path.join('500.html').to_s, status: :internal_server_error, layout: false
  end

  def authentication_required?
    Ddr::Public.require_authentication
  end

  # https://github.com/MiniProfiler/rack-mini-profiler?tab=readme-ov-file#access-control-in-non-development-environments
  def authorize_profiler
    Rack::MiniProfiler.authorize_request if current_ability.superuser?
  end
end
