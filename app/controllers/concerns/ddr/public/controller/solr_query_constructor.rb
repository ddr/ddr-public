# frozen_string_literal: true

module Ddr
  module Public
    module Controller
      module SolrQueryConstructor
        extend ActiveSupport::Concern

        def repository_id_query(values)
          construct_boolean_terms_query('id', values)
        end

        def construct_boolean_terms_query(field, values)
          "{!terms f=#{field} method=booleanQuery}#{values.join(',')}"
        end
      end
    end
  end
end
