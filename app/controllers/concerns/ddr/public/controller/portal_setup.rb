# frozen_string_literal: true

module Ddr
  module Public
    module Controller
      module PortalSetup
        extend ActiveSupport::Concern

        def self.included(base)
          base.before_action :prepend_view_path_for_portal_overrides
        end

        def parent_collection_join_ids
          @parent_collection_join_ids ||= portal_controller_setup.parent_collection_join_ids
        end

        def search_service_context
          { parent_collection_join_ids: }
        end

        private

        def prepend_view_path_for_portal_overrides
          @prepend_view_path_for_portal_overrides ||= prepend_view_path portal_controller_setup.view_path
        end

        def portal_controller_setup
          @portal_controller_setup ||= Portal::ControllerSetup.new({ controller_name:,
                                                                     local_id: params[:collection],
                                                                     controller_scope: self })
        end
      end
    end
  end
end
