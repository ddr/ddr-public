# frozen_string_literal: true

module Ddr
  module Public
    module Controller
      module IiifImagePaths
        IIIF_IMAGE_DEFAULTS = {
          region: 'full',
          size: 'full',
          rotation: '0',
          quality: 'default',
          format: 'jpg'
        }.freeze

        def iiif_image_path(identifier, options)
          opts = IIIF_IMAGE_DEFAULTS
                 .merge(options)
                 .merge(identifier:,
                        controller: 'iiif_images',
                        action: 'show',
                        only_path: true)

          url_for(opts)
        end

        def iiif_image_info_path(identifier)
          url_for(controller: 'iiif_images', action: 'info', identifier:, only_path: true)
        end

        def iiif_image_size_maxpx(pixels)
          px = pixels.to_s || ''
          px.present? ? "!#{px},#{px}" : 'full'
        end

        #
        # Moved `cached_read_public_ability' and `public_user' here
        # from ApplicationHelper as part of work on DDK-59. They are
        # re-added to the request context by including this module
        # in ApplicationController and designation these methods
        # as helpers.
        #
        def cached_read_public_ability(doc)
          return false unless doc

          cache_key = "public_read_ability_#{doc.id}"
          Rails.cache.fetch(cache_key, expires_in: 5.minutes) do
            public_user.can?(:read, doc)
          end
        end

        def public_user
          @public_user ||= Ability.new
        end
      end
    end
  end
end
