# frozen_string_literal: true

module Ddr
  module Public
    module Controller
      module DeepPaging
        extend ActiveSupport::Concern

        def limit_results_paging
          limit_paging(params[:page],
                       Ddr::Public.paging_limit_results,
                       paging_message)
        end

        def limit_facet_paging
          limit_paging(params['facet.page'],
                       Ddr::Public.paging_limit_facets,
                       facet_paging_message)
        end

        private

        def limit_paging(page, limit, message)
          if show_paging_alert?(page, limit) && !request.xhr?
            flash[:alert] = message
          elsif show_paging_error?(page, limit)
            flash[:error] = message
            redirect_to root_path
          end
        end

        def show_paging_alert?(page, paging_limit)
          page && page.to_i == paging_limit.to_i
        end

        def show_paging_error?(page, paging_limit)
          page && page.to_i > paging_limit.to_i
        end

        def paging_message
          t('ddr.public.paging_limit.results',
            paging_limit: Ddr::Public.paging_limit_results)
        end

        def facet_paging_message
          t('ddr.public.paging_limit.facets',
            facet_paging_limit: Ddr::Public.paging_limit_facets)
        end
      end
    end
  end
end
