# frozen_string_literal: true

class ItemsController < CatalogController
  configure_blacklight do |config|
    # ItemsController is currently only used for OAI-PMH harvests.
    config.search_builder_class = OaiItemSearchBuilder
    config.document_model = SolrDocument
    config.oai = {
      provider: {
        repository_name: I18n.t('blacklight.application_name'),
        repository_url: Rails.application.routes.url_helpers.oai_items_url(host: Ddr::Public.root_url),
        record_prefix: 'oai:ddr',
        admin_email: Ddr::Public.contact_email,
        sample_id: '33073160-c9f8-4b9b-bf3c-6e61dc02dcf6'
      },
      document: {
        limit: 50,
        set_model: PublicSet,
        set_fields: [{ label: 'admin_set', solr_field: 'admin_set_title_ssi' },
                     { label: 'collection', solr_field: 'is_member_of_collection_ssim' }]
      }
    }
  end
end
