# frozen_string_literal: true

class DirectoryTreeController < CatalogController
  include BlacklightHelper

  def show
    case document['common_model_name_ssi']
    when 'Collection'
      render json: fetch_links_and_titles
    when 'Item'
      render json: item_parent_directories_and_siblings
    end
  end

  private

  def item_parent_directories_and_siblings
    item_directory_list(document.parent_directories, document.directory_siblings)
  end

  def item_directory_list(directories = [], siblings = [])
    return [] if directories.blank?

    directories.map do |_dir|
      { text: directories.shift,
        icon: 'fas fa-folder',
        state: { opened: true },
        a_attr: { href: 'javascript:void(0);' },
        children: (directories.blank? ? item_siblings_list(siblings) : item_directory_list(directories, siblings)) }
    end
  end

  def item_siblings_list(siblings)
    siblings.map do |sib|
      select_state = sib.id == document.id
      { id: sib.id,
        text: sib.title_ssi,
        icon: 'far fa-file',
        state: { selected: select_state },
        a_attr: { href: document_or_object_url(sib) } }
    end
  end

  def fetch_links_and_titles
    directory.filter_map do |item|
      if item.key?(:repo_id)
        doc = search_for_solr_document(item[:repo_id])
        if doc.present?
          { id: doc.id, text: doc.title_ssi, a_attr: { href: document_or_object_url(doc) }, icon: 'far fa-file' }
        end
      else
        item
      end
    end
  end

  def directory
    directories[params[:directory_id]]
  end

  def directories
    @directories ||= document.structures.directories.directory_id_lookup
  end

  def document
    @document ||= SolrDocument.find(params[:id])
  end

  def search_for_solr_document(doc_id)
    search_service = Blacklight::SearchService.new(config: blacklight_config,
                                                   user_params: { q: solr_document_query(doc_id) },
                                                   search_builder_class: SearchBuilder)
    (_response, doc_list) = search_service.search_results do |builder|
      builder.except(:add_facetting_to_solr)
    end
    doc_list&.first
  end

  def solr_document_query(doc_id)
    "id:#{doc_id}"
  end
end
