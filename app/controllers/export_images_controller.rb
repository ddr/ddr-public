# frozen_string_literal: true

require 'zip_kit'
require 'hexapdf'

# Controller for exporting a PDF or ZIP file on-the-fly for an image item
class ExportImagesController < ApplicationController
  include BlacklightHelper # for item URLs
  include ZipKit::RailsStreaming
  include ActionController::Live

  PERMISSION = :read

  layout 'ddr'

  attr_reader :item_doc

  before_action :get_item_doc
  before_action :verify_not_a_bot, :enforce_permission, :get_image_list, :set_started_cookie,
                only: %i[export_zip export_pdf]

  skip_before_action :verify_not_a_bot if Rails.env.development?

  rescue_from CanCan::AccessDenied do |_|
    head :unauthorized
  end

  protect_from_forgery

  def verification
    redirect_to document_or_object_url(@item_doc) and return if verified_not_a_bot?

    flash.now[:alert] = t('ddr.public.image_export.verify.help')
    render 'export_images/unauthenticated'
  end

  def verify_not_a_bot
    redirect_to verify_exportable_path(id: @item_doc.id) and return unless verified_not_a_bot?
  end

  def verify_export_recaptcha
    # verify_recaptcha method comes from the recaptcha gem; see:
    # https://github.com/ambethia/recaptcha#verify_recaptcha
    if verify_recaptcha
      session[:verified_recaptcha] = true
      flash[:notice] = t('ddr.public.image_export.verify.recaptcha_success')
    else
      session.delete(:verified_recaptcha)
      flash[:error] = t('ddr.public.image_export.verify.recaptcha_fail')
    end

    redirect_to document_or_object_url(@item_doc) and return
  end

  def export_zip
    response.headers['Content-Disposition'] = "attachment; filename=#{@item_doc.public_id}.zip"

    zip_kit_stream do |zip|
      @image_list.each_with_index do |path, index|
        filename = format('%<id>s-%<index>04d.jpg', id: @item_doc.public_id, index: (index + 1))

        # Use stored instead of deflated since JPGs can't deflate
        zip.write_stored_file(filename) do |sink|
          IO.copy_stream(path, sink)
        end
      end
    end
  end

  def export_pdf
    write_pdf_to_path do |path|
      ::File.open(path, 'rb') do |f|
        send_stream(filename: "#{@item_doc.public_id}.pdf", type: 'application/pdf') do |stream|
          while (chunk = f.read(16.kilobytes))
            stream.write(chunk)
          end
        end
      end
    end
  end

  private

  def get_item_doc
    @item_doc = SolrDocument.find(params[:id])
  end

  def enforce_permission
    authorize! PERMISSION, item_doc
  end

  def get_image_list
    if @item_doc.derived_image_file_paths.present?
      @image_list = @item_doc.derived_image_file_paths
    else
      flash.now[:error] = t('ddr.public.image_export.no_images')
      redirect_to document_or_object_url(@item_doc) and return
    end
  end

  def set_started_cookie
    # Set a cookie so the client can know that the ZIP or PDF has begun assembling
    cookies["duke_ddr_export_begin_#{item_doc.id}"] = DateTime.now
  end

  def verified_not_a_bot?
    user_signed_in? || verified_recaptcha_for_session?
  end

  def verified_recaptcha_for_session?
    session[:verified_recaptcha].present?
  end

  def write_pdf_to_path
    Dir.mktmpdir(nil, '/export') do |tmpdir|
      files = []

      @image_list.each_slice(20) do |image_paths|
        file = File.join(tmpdir, SecureRandom.uuid)
        spawn('hexapdf', 'image2pdf', *image_paths, file)
        files << file
      end

      Process.waitall

      if files.length == 1
        yield files.first
      else
        outfile = File.join(tmpdir, SecureRandom.uuid)

        pid = spawn('hexapdf', 'merge', *files, outfile)
        Process.wait(pid)

        yield outfile
      end
    end
  end
end
