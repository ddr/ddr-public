# frozen_string_literal: true

class StreamController < ApplicationController
  include ActionController::Live

  before_action :load_and_authorize_asset

  def show
    file = DdrFile.new(@asset.streamable_media)

    return head :ok, content_length: file.file_size, content_type: file.media_type if request.head?

    # X-Sendfile
    if file.disk?
      logger.debug('STREAMING') { "File on disk - serving #{file.file_path.inspect} via send_file" }

      filename = @asset.public_id + '.' + @asset.file_extension(file.media_type) # oof, that's ugly :(

      return send_file(file.file_path, type: file.media_type, disposition: 'inline', filename:)
    end

    # S3 - we shouldn't get here ...
    if file.s3?
      logger.debug('STREAMING') { "S3 file - redirecting to #{file.stream_url.inspect}" }

      return redirect_to file.stream_url
    end
  end

  protected

  def load_and_authorize_asset
    @asset = SolrDocument.find(params[:id])
    return head :not_found unless @asset.streamable?
    authorize! :read, @asset
  end
end
