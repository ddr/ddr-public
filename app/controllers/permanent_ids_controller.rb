# frozen_string_literal: true

class PermanentIdsController < ApplicationController
  include Blacklight::Catalog
  include BlacklightHelper

  def show
    (response, docs) = permanent_id_search
    doc = docs&.first
    if response.total.zero?
      render file: Rails.public_path.join('404.html').to_s, layout: true, status: :not_found
    else
      redirect_to redirect_url(doc)
    end
  end

  private

  def permanent_id
    params.require(:permanent_id)
  end

  def permanent_id_search
    search_service = Blacklight::SearchService.new(config: blacklight_config,
                                                   user_params: { q: "permanent_id_ssi:#{permanent_id}" })
    # returns ({response}, [doc_list])
    search_service.search_results do |builder|
      builder.append(:include_only_first_result)
             .except(:apply_gated_discovery)
             .except(:add_facetting_to_solr)
             .except(:add_range_limit_params)
    end
  end

  def redirect_url(doc)
    document_or_object_url(doc) + passthrough_params.to_s
  end

  def passthrough_params
    "?#{filter_and_queryify_params}" if filter_and_queryify_params.present?
  end

  def filter_and_queryify_params
    params.permit('embed').select { |k, _v| params_whitelist.include? k }.to_query
  end

  def params_whitelist
    %w[embed]
  end
end
