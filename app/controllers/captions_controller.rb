# frozen_string_literal: true

class CaptionsController < ApplicationController

  before_action :load_and_authorize_asset

  include CaptionHelper

  def show
    filename = @asset.public_id + '.' + @asset.caption_extension
    type = @asset.caption_type
    disposition = 'inline'

    send_data(@file.read, disposition:, filename:, type:)
  end

  def text
    send_data caption_text_from_vtt(caption_contents(@file)),
              filename: [@asset.public_id, 'txt'].join('.'),
              type: 'text/plain',
              disposition: 'inline'
  end

  def pdf
    alltext = caption_text_from_vtt(caption_contents(@file))
    permalink = @asset.permanent_url
    title = @asset.html_title

    pdf = Prawn::Document.new do
      # Registering a TTF font is needed to support UTF-8 character set
      font_families.update('SourceSansPro' => {
                             normal: Rails.root.join('app/assets/fonts/SourceSansPro-Regular.ttf'),
                             italic: Rails.root.join('app/assets/fonts/SourceSansPro-Italic.ttf'),
                             bold: Rails.root.join('app/assets/fonts/SourceSansPro-Bold.ttf'),
                             bold_italic: Rails.root.join('app/assets/fonts/SourceSansPro-BoldItalic.ttf')
                           })
      default_leading 5

      font('SourceSansPro', style: :bold) do
        text title
        move_down 5
      end

      font('SourceSansPro') do
        text permalink
        move_down 15

        stroke do
          stroke_color '333333'
          line_width 2
          stroke_horizontal_rule
          move_down 15
        end

        text alltext
      end
    end

    send_data pdf.render,
              filename: [@asset.public_id, 'pdf'].join('.'),
              type: 'application/pdf',
              disposition: 'inline'
  end

  def load_and_authorize_asset
    @asset = SolrDocument.find(params[:id])
    return head :not_found unless @asset.captioned?
    authorize! :read, @asset
    @file = DdrFile.new(@asset.caption)
  end
end
