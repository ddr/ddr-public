# frozen_string_literal: true

class DownloadsController < ApplicationController
  include ActionController::Live

  before_action :load_asset
  before_action :load_ddr_file
  before_action :authorize_download!

  attr_reader :asset, :ddr_file

  def show
    return not_found if ddr_file.null?
    return head :ok, content_length: ddr_file.file_size, content_type: ddr_file.media_type if request.head?

    # DDK-322 Chrome needs 'disposition: inline' to render PDFs inline, 2024-11-05 DCS.
    disposition = (params[:inline] && 'inline') || 'attachment'
    return send_file(ddr_file.file_path, type: ddr_file.media_type, filename: ddr_file_name, disposition:) if ddr_file.disk?

    # S3
    if disposition == 'inline'
      redirect_to ddr_file.stream_url
    else
      redirect_to ddr_file.download_url
    end
  end

  protected

  def ddr_file_type
    params.key?(:ddr_file_type) ? params[:ddr_file_type] : 'content'
  end

  def load_asset
    @asset = SolrDocument.find(params[:id])
  rescue SolrDocument::NotFound => _e
    render file: Rails.public_path.join('404.html').to_s, status: :not_found, layout: false
  end

  def load_ddr_file
    @ddr_file = ddr_file_to_show
  end

  def authorize_download!
    authorize! :"download_#{ddr_file_type}", asset
  end

  def ddr_file_to_show
    ddrf = asset.send(ddr_file_type.to_sym)
    raise "Unable to find a file for #{asset}" if ddrf.nil?

    ddrf
  end

  def ddr_file_name
    return ddr_file.original_filename if ddr_file.original_filename.present?
    # In DDR-C, the first 'identifier' is used if present.  However, I think that's probably a holdover from
    # the time before there was a 'local_id' field and the DPC identifier, if any, was the first entry in the
    # 'identifier' field.
    if asset.has?('local_id_ssi') # local_id may be file name minus extension
      return "#{asset.local_id_ssi}.#{asset.default_file_extension(ddr_file.media_type)}"
    end

    # Mimics DDR-C Ddr::Datastreams::DatastreamBehavior#default_file_name
    "#{asset.id}_#{ddr_file_type}.#{asset.default_file_extension(ddr_file.media_type)}"
  end
end
