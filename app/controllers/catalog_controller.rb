# frozen_string_literal: true

# rubocop:disable Metrics/ClassLength
class CatalogController < ApplicationController
  include Blacklight::Catalog
  include BlacklightRangeLimit::ControllerOverride
  include Blacklight::DefaultComponentConfiguration
  include BlacklightOaiProvider::Controller

  include Ddr::Public::Controller::ConfigureBlacklight
  include Ddr::Public::Controller::DeepPaging

  before_action :enforce_show_permissions, only: :show
  before_action :allow_iframe, only: :show
  before_action :limit_results_paging, only: %i[index index_portal]
  before_action :limit_facet_paging, only: :facet

  helper_method :repository, :search_builder

  rescue_from CanCan::AccessDenied do |_exception|
    if user_signed_in?
      forbidden
    else
      authenticate_user!
    end
  end

  layout 'ddr'

  # rubocop:disable Metrics/BlockLength
  configure_blacklight do |config|
    config.search_builder_class = SearchBuilder

    # Turn off default search session tracking
    config.track_search_session = false

    # Enable JSON-API for a resource at /raw
    config.raw_endpoint.enabled = true

    # Search results views:
    # config.view.list.document_component = Blacklight::DocumentComponent
    config.view.list.document_component = Ddr::DocumentComponentList
    config.view.gallery.document_component = Blacklight::Gallery::DocumentComponent
    # config.view.masonry.document_component = Blacklight::Gallery::DocumentComponent
    # config.view.slideshow.document_component = Blacklight::Gallery::SlideshowComponent

    config.show.tile_source_field = :content_metadata_image_iiif_info_ssm
    config.show.display_type_field = 'display_format_ssi'

    config.default_solr_params = {
      # :qt => 'search',
      rows: 20
    }

    config.per_page = [10, 20, 50, 100]
    config.default_per_page = 20
    config.max_per_page = 999

    # TODO: see whether DUL still needs custom presenters here
    # set custom presenters
    # config.index.document_presenter_class = IndexPresenter
    # config.show.document_presenter_class = ShowPresenter

    # solr field configuration for search results/index views
    config.index.title_field = 'title_ssi'
    config.index.ddr_model_field = 'common_model_name_ssi'
    config.index.display_type_field = 'display_format_ssi'

    config.index.thumbnail_method = :thumbnail_image_tag

    config.index.document_actions.delete(:bookmark)
    config.show.document_actions.delete(:bookmark)
    config.navbar.partials.delete(:bookmark)
    config.navbar.partials.delete(:search_history)
    config.navbar.partials.delete(:saved_searches)

    # solr fields that will be treated as facets by the blacklight application
    #   The ordering of the field names is the order of the display
    #
    # Setting a limit will trigger Blacklight's 'more' facet values link.
    # * If left unset, then all facet values returned by solr will be displayed.
    # * If set to an integer, then "f.somefield.facet.limit" will be added to
    # solr request, with actual solr request being +1 your configured limit --
    # you configure the number of items you actually want _tsimed_ in a page.
    # * If set to 'true', then no additional parameters will be sent to solr,
    # but any 'sniffed' request limit parameters will be used for paging, with
    # paging at requested limit -1. Can sniff from facet.limit or
    # f.specific_field.facet.limit solr request params. This 'true' config
    # can be used if you set limits in :default_solr_params, or as defaults
    # on the solr side in the request handler itself. Request handler defaults
    # sniffing requires solr requests to be made with "echoParams=all", for
    # app code to actually have it echo'd back to see it.
    #
    # :show may be set to false if you don't want the facet to be drawn in the
    # facet bar

    config.add_facet_field 'admin_set_title_ssi', label: 'Collection Group', collapse: false,
                                                  limit: 5
    config.add_facet_field 'collection_title_ssi', label: 'Collection', limit: 5
    config.add_facet_field 'year_facet_iim',
                           label: 'Year',
                           limit: 9999,
                           collapse: false,
                           range: { num_segments: 6, segments: true }
    config.add_facet_field 'creator_facet_sim', label: 'Creator', limit: 5
    config.add_facet_field 'format_facet_sim', label: 'Format', limit: 5
    config.add_facet_field 'subject_facet_sim', label: 'Subject', limit: 5
    config.add_facet_field 'language_facet_sim', label: 'Language', limit: 5
    config.add_facet_field 'spatial_facet_sim', label: 'Location', limit: 5
    config.add_facet_field('research_help_contact_ssi',
                           label: 'Collecting Area',
                           helper_method: 'research_help_contact',
                           limit: 20)
    config.add_facet_field 'common_model_name_ssi', label: 'Browse', show: false
    config.add_facet_field 'aspace_id_ssi', label: 'ArchivesSpace ID', show: false
    config.add_facet_field 'category_facet_sim', label: 'Category', show: false

    # Have BL send all facet field names to Solr, which has been the default
    # previously. Simply remove these lines if you'd rather use Solr request
    # handler defaults, or have no facets.
    config.add_facet_fields_to_solr_request!
    # use this instead if you don't want to query facets marked :show=>false
    # config.default_solr_params[:'facet.field'] = config.facet_fields.select{ |k, v| v[:show] != false}.keys

    # solr fields to be displayed in the index (search results) view
    #   The ordering of the field names is the order of the display
    config.add_index_field 'creator_tsim', separator: '; ', label: 'Creator'
    config.add_index_field 'date_tsim', accessor: 'humanized_date', separator: '; ',
                                        label: 'Date'
    config.add_index_field 'type_tsim', separator: '; ', label: 'Type'
    config.add_index_field 'permanent_url_ssi', helper_method: 'permalink', label: 'Permalink'
    config.add_index_field 'content_media_type_ssim', helper_method: 'file_info', label: 'File'
    config.add_index_field 'is_part_of_ssim', helper_method: 'descendant_of', label: 'Part of'
    config.add_index_field 'is_member_of_collection_ssim', helper_method: 'descendant_of',
                                                           label: 'Collection'

    # partials for show view
    config.show.partials = %i[show show_children item_directory_tree show_bottom related_items]

    # deactivate certain tools
    config.show.document_actions.delete(:email)
    config.show.document_actions.delete(:sms)
    config.show.document_actions.delete(:citation)
    config.show.document_actions.delete(:refworks)
    config.show.document_actions.delete(:endnote)
    config.show.document_actions.delete(:librarian_view)

    # solr fields to be displayed in the show (single result) view
    #   The ordering of the field names is the order of the display
    config.add_show_field 'title_tsim', separator: '; ', label: 'Title'
    config.add_show_field 'permanent_url_ssi', helper_method: 'permalink', label: 'Permalink'
    config.add_show_field 'content_media_type_ssim', helper_method: 'file_info', label: 'File'
    config.add_show_field 'creator_tsim', separator: '; ', label: 'Creator'
    config.add_show_field 'date_tsim', accessor: 'humanized_date', separator: '; ',
                                       label: 'Date'
    config.add_show_field 'type_tsim', separator: '; ', label: 'Type'

    config.add_show_field 'is_part_of_ssim', helper_method: 'descendant_of', label: 'Part of'
    config.add_show_field 'is_member_of_collection_ssim', helper_method: 'descendant_of', label: 'Collection'

    # "fielded" search configuration. Used by pulldown among other places.
    # For supported keys in hash, see rdoc for Blacklight::SearchFields
    #
    # Search fields will inherit the :qt solr request handler from
    # config[:default_solr_parameters], OR can specify a different one
    # with a :qt key/value. Below examples inherit, except for subject
    # that specifies the same :qt as default for our own internal
    # testing purposes.
    #
    # The :key is what will be used to identify this BL search field internally,
    # as well as in URLs -- so changing it after deployment may break bookmarked
    # urls.  A display label will be automatically calculated from the :key,
    # or can be specified manually to be different.

    # This one uses all the defaults set by the solr request handler. Which
    # solr request handler? The one set in config[:default_solr_parameters][:qt],
    # since we aren't specifying it otherwise.

    config.add_search_field 'all_fields', label: 'All Fields' do |field|
      field.solr_parameters = {
        qf: ['id',
             'abstract_tesim',
             'alternative_tesim',
             'artist_tesim',
             'biblical_book_tesim',
             'bibliographicCitation_tesim',
             'category_tesim',
             'chapter_and_verse_tesim',
             'company_tesim',
             'creator_tesim^15',
             'contributor_tesim',
             'description_tesim',
             'extent_tesim',
             'folder_tesim',
             'format_tesim',
             'genre_tesim',
             'headline_tesim',
             'identifier_tesim',
             'isPartOf_tesim',
             'isReferencedBy_tesim',
             'issue_number_tesim',
             'language_name_tesim',
             'medium_tesim',
             'nested_path_text_teim',
             'placement_company_tesim',
             'product_tesim',
             'provenance_tesim',
             'publication_tesim',
             'publisher_tesim',
             'rights_tesim',
             'series_tesim',
             'setting_tesim',
             'spatial_tesim',
             'sponsor_tesim',
             'subject_tesim^5',
             'temporal_tesim',
             'title_tesim^20',
             'tone_tesim',
             'type_tesim',
             'volume_tesim',
             'all_text_timv',
             'year_facet_iim',
             'identifier_all_ssim'].join(' ')
      }
    end

    # Now we see how to over-ride Solr request handler defaults, in this
    # case for a BL "search field", which is really a dismax aggregate
    # of Solr search fields.

    config.add_search_field('title') do |field|
      # :solr_local_parameters will be sent using Solr LocalParams
      # syntax, as eg {! qf=$title_qf }. This is neccesary to use
      # Solr parameter de-referencing like $title_qf.
      # See: http://wiki.apache.org/solr/LocalParams
      field.solr_local_parameters = {
        qf: '$title_qf',
        pf: '$title_pf'
      }
    end

    config.add_search_field('creator') do |field|
      field.solr_local_parameters = {
        qf: 'creator_tesim',
        pf: ''
      }
    end

    # Specifying a :qt only to show it's possible, and so our internal automated
    # tests can test it. In this case it's the same as
    # config[:default_solr_parameters][:qt], so isn't actually neccesary.
    config.add_search_field('subject') do |field|
      field.qt = 'search'
      field.solr_local_parameters = {
        qf: '$subject_qf',
        pf: '$subject_pf'
      }
    end

    # "sort results by" select (pulldown)
    # label in pulldown is followed by the name of the SOLR field to sort by and
    # whether the sort is ascending or descending (it must be asc or desc
    # except in the relevancy case).
    config.add_sort_field 'score desc,date_sort_si asc,title_ssi asc',
                          label: 'relevance'
    config.add_sort_field 'title_ssi asc', label: 'title'
    config.add_sort_field 'date_sort_si asc', label: 'date (old to new)'
    config.add_sort_field 'date_sort_si desc', label: 'date (new to old)'

    # If there are more than this many search results, no spelling ("did you
    # mean") suggestion is offered.
    config.spell_max = 5

    config.oai = {
      provider: {
        repository_name: I18n.t('blacklight.application_name'),
        repository_url: Rails.application.routes.url_helpers.oai_catalog_url(host: Ddr::Public.root_url),
        record_prefix: 'oai:ddr',
        admin_email: Ddr::Public.contact_email,
        sample_id: '33073160-c9f8-4b9b-bf3c-6e61dc02dcf6'
      },
      document: {
        limit: 25,
        set_model: PublicSet,
        set_fields: [{ label: 'admin_set', solr_field: 'admin_set_title_ssi' },
                     { label: 'collection', solr_field: 'is_member_of_collection_ssim' }]
      }
    }
  end
  # rubocop:enable Metrics/BlockLength

  def show
    super

    render layout: 'embed' if is_embed?
  end

  # Action exists to distinguish between the collection
  # scoped dc/:collection portals and the dc/ portal
  # This is for the dc/ portal page
  def index_portal
    index
  end

  private

  def allow_iframe
    response.headers.delete('X-Frame-Options') if is_embed?
  end
end
# rubocop:enable Metrics/ClassLength
