# frozen_string_literal: true

module Users
  class OmniauthCallbacksController < Devise::OmniauthCallbacksController
    def openid_connect
      auth = request.env['omniauth.auth']
      logger.debug { "Auth hash: #{auth.to_h}" }

      user = resource_class.from_omniauth(auth)

      if login_authorized?(user)
        set_flash_message :notice, :success, kind: 'Duke NetID'
        sign_in_and_redirect user
      else
        render file: Rails.public_path.join('401.html').to_s, status: :unauthorized, layout: false
      end
    end

    private

    def login_authorized?(user)
      return true if Ddr::Public.login_group.blank?

      user.groups.include?(Ddr::Public.login_group)
    end
  end
end
