# frozen_string_literal: true

module Users
  class SessionsController < Devise::SessionsController
    def new
      redirect_to user_openid_connect_omniauth_authorize_path
    end

    def after_sign_out_path_for(_scope)
      'https://oauth.oit.duke.edu/oidc/logout.jsp'
    end
  end
end
