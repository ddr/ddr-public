# frozen_string_literal: true

class PortalController < CatalogController
  include Ddr::Public::Controller::PortalSetup
end
