# frozen_string_literal: true

class IiifImagesController < ApplicationController
  include ActionController::Live

  PERMISSION = :read

  # https://iiif.io/api/image/2.1/#image-request-parameters
  IIIF_API_PARAMS = %i[region size rotation quality format].freeze

  attr_reader :image_doc

  before_action :find_image_doc
  before_action :enforce_permission

  rescue_from CanCan::AccessDenied do |_|
    head :unauthorized
  end

  protect_from_forgery

  def info
    info = Net::HTTP.get_response(proxy_request_uri('info.json'))

    # We have to fix up the @id attribute of the JSON-LD document
    image_info = JSON.parse(info.body)

    # Strip last URL path component, remove double-encoding, and encode the colon
    image_info['@id'] = request.url.sub(%r{/[^/]+$}, '').gsub('%25', '%').sub(%r{ark:}, 'ark%3A')

    render json: image_info, content_type: info.content_type
  end

  def show
    size = params[:size].sub('!', '').sub(',', 'x')
    filename = '%s-%s.%s' % [ @image_doc.id, size, params[:format] ]
    path = '%s/%s/%s/%s.%s' % params.require(IIIF_API_PARAMS)

    Net::HTTP.get_response(proxy_request_uri(path)) do |iipsrv_response|
      send_stream(filename:, type: iipsrv_response.content_type) do |stream|
        iipsrv_response.read_body do |chunk|
          stream.write(chunk)
        end
      end
    end
  end

  private

  def proxy_request_uri(path)
    URI("#{Ddr::Public.image_server_url}?IIIF=#{image_path}/#{path}")
  end

  def has_permission?
    can?(:read, image_doc)
  end

  def enforce_permission
    authorize! PERMISSION, image_doc
  end

  def image_path
    image_doc&.multires_image_file_path
  end

  def identifier
    @identifier ||= URI.decode_www_form_component(params.require(:identifier))
  end

  def find_image_doc
    @image_doc ||= image_doc_query
    head(:not_found) if @image_doc.blank?
  end

  def image_doc_query
    search_service = Blacklight::SearchService.new(config: blacklight_config,
                                                   user_params: { q: "permanent_id_ssi:#{identifier}" })
    (_response, doc_list) = search_service.search_results do |builder|
      builder.append(:include_only_multires_images)
             .append(:include_only_first_result)
             .except(:add_facetting_to_solr)
             .except(:add_range_limit_params)
    end
    doc_list&.first
  end
end
