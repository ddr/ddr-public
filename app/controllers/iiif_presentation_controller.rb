# frozen_string_literal: true

class IiifPresentationController < ApplicationController
  include ActionView::Helpers::TextHelper               # with rails_autolink, for `auto_link' helper.
  include ActionView::Helpers::UrlHelper                # for link_to
  include Ddr::Public::Controller::ConfigureBlacklight  # for configured metadata labels/values

  before_action :log_params
  before_action :fetch_doc
  before_action :authorize_manifest

  def manifest
    render json: iiif_manifest
  end

  private

  def preview?
    Ddr::Public.base_url == 'https://preview.repository.duke.edu'
  end

  def log_params
    params.permit!

    logger.debug { "IIIF manifest request: #{params.to_h}" }
  end

  def iiif_manifest
    Rails.cache.fetch(manifest_api_uri.to_s, expires_in: 1.day, force: preview?) do
      JSON.parse(fetch_manifest).tap do |manifest|
        logger.debug { "Overwriting metadata in IIIF manifest for #{@document.id}." }

        manifest['metadata'] = iiif_manifest_metadata

        if params[:start] && !manifest.key?('start')
          logger.debug { "Fetching start doc #{params[:start]} for IIIF manifest for #{@document.id}" }

          start_doc = find_by_permanent_id(params[:start])
          parent_id = Array(start_doc.parent_id).first

          if parent_id == @document.id
            logger.debug { "Adding 'start' key to IIIF manifest for #{@document.id}." }

            manifest['start'] = {
              id: "#{Ddr::Public.root_url}/iiif/#{CGI.escapeURIComponent(start_doc.permanent_id_ssi)}/canvas",
              type: 'Canvas'
            }
          else
            logger.error { "IIIF start doc parent_id (#{parent_id}) does not match document id (#{@document.id})" }
          end
        end
      end
    end
  end

  def dynamic
    Ddr::Public.iiif_dynamic_manifests
  end

  def fetch_manifest
    return iiif_file.read if Rails.env.test?
    return iiif_file.read unless dynamic || include_previewable || iiif_file.null?

    headers = {'accept'=>'application/json', 'authorization'=>"Bearer #{Ddr::Public.ddr_api_key}"}

    logger.debug { "Fetching IIIF manifest: #{manifest_api_uri}" }

    response = Net::HTTP.get_response(manifest_api_uri, headers)

    raise Ddr::Public::Error, "Something went wrong." unless response.is_a?(Net::HTTPSuccess)

    response.read_body.tap do |iiif|
      # We fix up URLs on the preview site b/c they are generated with production values.
      iiif.gsub!('https://repository.duke.edu', Ddr::Public.base_url) if preview?
    end

  rescue Ddr::Public::Error, Errno::ECONNREFUSED => e
    logger.error(e)

    # Fallback if there is an API connection or response error
    return iiif_file.read unless iiif_file.null?

    not_found
  end

  def iiif_file
    @document.iiif_file
  end

  def include_previewable
    Blacklight::Configuration.default_values[:solr_path] == 'previewable' ? true : false
  end

  def manifest_api_uri
    @manifest_api_uri ||= URI("#{Ddr::Public.ddr_api_url}/resources/#{@document.id}/iiif").tap do |uri|
      uri.query = URI.encode_www_form(
        { include_previewable:,
          dynamic:,
          force: true, # don't validate manifest
          start: params[:start]
        }.compact
      )
    end
  end

  def id
    @id ||= CGI.unescape_uri_component(params.require(:id))
  end

  def fetch_doc
    @document = id.start_with?('ark:/') ? find_by_permanent_id(id) : SolrDocument.find(id)
  end

  def authorize_manifest
    authorize! :download_iiif_file, @document
  end

  def find_by_permanent_id(ark)
    search_service = Blacklight::SearchService.new(config: blacklight_config,
                                                   user_params: { q: "permanent_id_ssi:#{ark}" })
    _, docs = search_service.search_results do |builder|
      builder.append(:include_only_first_result).except(:add_facetting_to_solr, :apply_gated_discovery)
    end

    return not_found if docs.empty?

    docs.first
  end

  def iiif_manifest_metadata
    # This gets us the configured Show fields for the Catalog Controller. Fields that use an
    # accessor method (e.g., humanized_date) do get their values transformed, however additional
    # helper_methods (e.g., descendant_of or source_collection) do not apply here.
    # See: config/ddr-portals/catalog/portal_view_configs.yml
    field_presenters = Blacklight::ShowPresenter.new(@document, view_context).field_presenters
    field_presenters.map { |presenter| iiif_label_values(presenter.label, presenter.values, presenter.field_config) }
  end

  def iiif_label_values(label, values, field_config)
    {
      label: { en: [ label ] },
      value: { en: enhance_metadata_values(values, field_config.key).compact_blank }
    }
  end

  # For a few fields, transform the values we get back to be more useful in the context
  # of a manifest. Note this is different from the helper methods (presenter.field_config.helper_method)
  # configured for use in the on-page metadata section.
  def enhance_metadata_values(values, key)
    case key
    when 'is_member_of_collection_ssim'
      coll = SolrDocument.find(values.first)

      [ link_to(coll.title_ssi, coll.permanent_url_ssi) ]

    when 'ead_id_ssi'
      [ link_to('View collection guide', "#{Ddr::Public.finding_aid_base_url}/catalog/#{values.first}") ]

    when 'bibsys_id_ssi'
      [ link_to('View catalog record', "#{Ddr::Public.catalog_url}#{values&.first}") ]

    else
      values.map { |value| auto_link(value) }
    end
  end
end
