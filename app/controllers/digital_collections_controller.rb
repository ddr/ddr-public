# frozen_string_literal: true

class DigitalCollectionsController < CatalogController
  include Ddr::Public::Controller::PortalSetup

  # Needs to run before enforce_show_permissions
  prepend_before_action :set_params_id_to_pid, only: :show

  # Updates action from CatalogController
  before_action :enforce_show_permissions, only: :show

  layout 'digital_collections'

  configure_blacklight do |config|
    config.search_builder_class = DcSearchBuilder
    config.view.gallery.default = true
  end

  def index
    super

    digital_collections_portal
    authorize_portal_page
  end

  def about
    digital_collections_portal
  end

  def featured
    digital_collections_portal
  end

  private

  # rubocop:disable Naming/MemoizedInstanceVariableName
  def digital_collections_portal
    @portal ||= Portal::DigitalCollections.new(
      {
        controller_name:,
        local_id: params[:collection],
        controller_scope: self
      }
    )
  end
  # rubocop:enable Naming/MemoizedInstanceVariableName

  def authorize_portal_page
    return if digital_collections_portal.collections.present?

    if user_signed_in?
      forbidden
    else
      authenticate_user!
    end
  end

  def verify_collection_slug
    not_found unless @document.collection.local_id_ssi == params['collection']
  end

  def set_params_id_to_pid
    # FIXME: Is this really how we have to do this?
    conn = RSolr.connect(url: Blacklight.connection_config[:url])
    solr_path = Blacklight.default_configuration[:solr_path]

    result = conn.get(solr_path, params: {q: local_id_query, rows: 1})

    docs = result.fetch('response', {}).fetch('docs', [])
    return if docs.blank?

    document = SolrDocument.new docs.first
    params[:id] = document.id
  end

  def local_id_query
    "local_id_ssi:\"#{params[:id]}\" AND common_model_name_ssi:\"Item\""
  end
end
