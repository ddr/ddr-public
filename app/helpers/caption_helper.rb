# frozen_string_literal: true
require 'webvtt'


module CaptionHelper
  # @param caption_file [DdrFile]
  def caption_contents(caption_file)
    WebVTT.from_blob(caption_file.read)
  end

  def clean_cue_text(line)
    Nokogiri::HTML(line).text
  end

  # Split cue into parts when there are intentional line breaks
  def cue_lines(cue_text)
    cue_text.split("\n")
  end

  def cue_speaker(line)
    # Regex instead of Nokogiri b/c <v Jane Done> not easily parsed w/xpath or css selectors
    match = /<v\s*([^>]*)>/.match(line)
    return unless match

    speaker = match[1]
    # Some transcripts only have <v -> to indicate speaker has changed, but
    # don't indicate speaker's name
    return '-' if ['', '-'].include? speaker

    speaker
  end

  def display_time(cue_time_sec)
    if cue_time_sec < 3600
      Time.at(cue_time_sec).utc.strftime('%-M:%S')
    else
      Time.at(cue_time_sec).utc.strftime('%-H:%M:%S')
    end
  end

  def caption_text_from_vtt(vtt)
    caption_text = []
    vtt.cues.each_with_index do |cue, cue_index|
      cue_lines(cue.text).each_with_index do |line, line_index|
        # keep any intentional line breaks within cues
        caption_text << "\n" unless line_index.zero?

        # Speaker change (<v>) gets double linebreak except in the very first cue
        if cue_speaker(line).present?
          unless cue_index.zero?
            caption_text << if line_index.zero?
                              "\n\n"
                            else
                              # already got one linebreak, just need one more
                              "\n"
                            end
          end

          # preface text with {speaker name}: if <v> is identified or just - if <v ->.
          caption_text << [cue_speaker(line), (cue_speaker(line) == '-' ? ' ' : ': ')].join
        end

        # combine cues with spaces.
        caption_text << [clean_cue_text(line), ' '].join
      end
    end
    caption_text.join
  end
end
