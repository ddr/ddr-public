# frozen_string_literal: true

module BlacklightHelper
  include Blacklight::BlacklightHelperBehavior

  def show_solr_document_url(doc, _options = {})
    if doc.public_collection.present?
      url_for controller: doc.public_controller, action: doc.public_action, collection: doc.public_collection,
              id: doc.public_id, only_path: false
    else
      url_for controller: doc.public_controller, action: doc.public_action, id: doc.public_id, only_path: false
    end
  end

  # Overrides method from Blacklight::UrlHelperBehavior to accommodate DUL practice
  # of using local_id based URLs for digital collections objects, via
  # document_or_object_url -- see:
  # https://github.com/projectblacklight/blacklight/blob/main/app/helpers/blacklight/url_helper_behavior.rb
  def link_to_document(doc, field_or_opts = nil, opts = { counter: nil })
    label = case field_or_opts
            when NilClass
              document_presenter(doc).heading
            when Hash
              opts = field_or_opts
              document_presenter(doc).heading
            else # String
              field_or_opts
            end

    link_to label, document_or_object_url(doc), document_link_params(doc, opts)
  end

  def document_or_object_url(doc_or_obj)
    if doc_or_obj.public_collection.present?
      url_for controller: doc_or_obj.public_controller, action: doc_or_obj.public_action,
              collection: doc_or_obj.public_collection, id: doc_or_obj.public_id, only_path: true
    else
      url_for controller: doc_or_obj.public_controller, action: doc_or_obj.public_action, id: doc_or_obj.public_id,
              only_path: true
    end
  end

  ##
  # Get the current "view type" (and ensure it is a valid type)
  #
  # This is an override of the default Blacklight method.
  # Our modification forces the list view for show action requests.
  # This view is used for displaying components on item pages
  # where it is desirable to see download links and metadata.
  #
  # @param [Hash] query_params the query parameters to check
  # @return [Symbol]
  def document_index_view_type(query_params = params)
    view_param = query_params[:view]
    view_param ||= :list if controller.action_name == 'show'
    view_param ||= session[:preferred_view]

    if view_param && document_index_views.key?(view_param.to_sym)
      view_param.to_sym
    else
      default_document_index_view_type
    end
  end

  def render_body_class
    # Overrides this method in
    # /projectblacklight/blacklight/app/helpers/blacklight/blacklight_helper_behavior.rb
    # Gives more fine-tuned control over CSS & JS selectors (esp for Matomo event tracking)
    body_class = extra_body_classes.join ' '
    body_class << ' search-results-page' if controller.is_a?(Blacklight::Catalog) && has_search_parameters?
    body_class << (" #{controller.action_name}")
    body_class
  end

  # Overrides Blacklight core helper
  # We want facets on the left (index view) but any other sidebars on the right
  # https://github.com/projectblacklight/blacklight/blob/main/app/helpers/blacklight/layout_helper_behavior.rb
  def main_content_classes
    classes = %w[col-md-8 col-lg-9]
    classes << if action_name.in?(%w[index index_portal])
                 'order-2'
               else
                 'order-1'
               end
    classes.join(' ')
  end

  # Overrides Blacklight core helper
  # We want facets on the left (index view) but other sidebars on the right
  # https://github.com/projectblacklight/blacklight/blob/main/app/helpers/blacklight/layout_helper_behavior.rb
  def sidebar_classes
    classes = %w[col-md-4 col-lg-3]
    classes << if action_name.in?(%w[index index_portal])
                 'order-1'
               else
                 'order-2'
               end
    classes.join(' ')
  end
end
