# frozen_string_literal: true

module MetadataDisplayHelper
  def permalink(options = {})
    plink = options[:value]&.first
    link_to(plink, plink).html_safe if plink.present?
  end

  def descendant_of(options = {})
    pid = options[:value].first
    docs = find_solr_documents(pid)
    return unless docs

    if cached_read_current_ability(docs.first)
      link_to_document(docs.first)
    else
      docs.map(&:title_ssi).first
    end
  end

  def find_solr_documents(id)
    Rails.cache.fetch("find_solr_document_#{id}", expires_in: 1.day) do
      query = repository_id_query([id])

      # FIXME: Is this really how we have to do this?
      conn = RSolr.connect(url: Blacklight.connection_config[:url])
      solr_path = Blacklight.default_configuration[:solr_path]

      results = conn.get(solr_path, params: { fq: query, q: '*:*' })
      results['response']['docs'].map { |result| SolrDocument.new(result) }
    end
  end

  def collection_sponsor(options = {})
    document = options[:document]
    placement = options[:placement] || 'top'
    link_to document.sponsor_display, Ddr::Public.adopt_url, { data: {
      toggle: 'tooltip',
      placement:,
      title: 'Digital preservation for this collection is supported through our Adopt a Digital Collection program. Click to learn more.'
    } }
  end

  def pronom_identifier_link(options = {})
    options[:value].map do |value|
      link_to(value, "#{Ddr::Public.pronomid_lookup_url}#{value}")
    end.join('; ').html_safe
  end

  def source_collection(options = {})
    document = options[:document]
    title = options[:title] || document.finding_aid&.title
    placement = options[:placement] || 'bottom'
    finding_aid = document.finding_aid
    link_to title, finding_aid.url,
            {
              data: {
                toggle: ('popover' if finding_aid.data?),
                boundary: 'viewport',
                placement:,
                html: true,
                title: [image_tag('ddr/archival-box.png',
                                  class: 'collection-guide-icon'),
                        'Source Collection Guide'].join,
                content: finding_aid_popover(finding_aid)
              }
            }
  end

  def link_to_catalog(options = {})
    auto_link("#{Ddr::Public.catalog_url}#{options[:value]&.first}")
  end

  def display_more_info_options?(document)
    document.ead_id_ssi.present? || document.bibsys_id_ssi.present? || is_collection?(document) || document.display_iiif_link?
  end

  # Don't display raw ARK values, and auto-link any URLs.
  def related_ark_prune(options = {})
    values = options[:value].reject { |value| value.match(/^ark:/) }
                            .map { |value| auto_link(value) }.join('<br/>').html_safe
    values.presence || 'See below'
  end
end
