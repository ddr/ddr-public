# frozen_string_literal: true

module ApplicationHelper
  include Ddr::Public::Controller::SolrQueryConstructor

  def iiif_presentation_url_params(doc)
    if doc.common_model_name == 'Component'
      { id: doc.parent_id, start: doc.permanent_id_ssi }
    else
      { id: doc.id }
    end
  end

  def cached_read_current_ability(doc)
    cache_key = "current_read_ability_#{current_ability.user}_#{doc.id}"
    Rails.cache.fetch(cache_key, expires_in: 5.minutes) do
      current_ability.can?(:read, doc)
    end
  end

  def cached_download_current_ability(doc)
    cache_key = "current_download_ability_#{current_ability.user}_#{doc.id}"
    Rails.cache.fetch(cache_key, expires_in: 5.minutes) do
      current_ability.can?(:download, doc)
    end
  end

  def cached_discover_current_ability(doc)
    cache_key = "current_discover_ability_#{current_ability.user}_#{doc.id}"
    Rails.cache.fetch(cache_key, expires_in: 5.minutes) do
      current_ability.can?(:discover, doc)
    end
  end

  def research_help_contact(code)
    Contact.call(code).to_s
  rescue Ddr::Public::Error => _e
    code
  end

  def ead_id_title(code)
    Rails.cache.fetch("#{code}/ead_id_title", expires_in: 1.day) do
      Timeout.timeout(2) do
        FindingAid.new(code).collection_title
      end
    rescue StandardError => e
      Rails.logger.error { "#{e.message} #{e.backtrace.join("\n")}" }
      code
    end
  end

  # Sort by a method derived title of coded field normalized to lower case for case-independent sorting
  # The 'value' attribute of each 'item' in the facet is the code
  def facet_display_value_sort(items = [], facet_value_method)
    # Coerce values to_s to avoid nil errors
    items.sort do |a, b|
      send(facet_value_method, a.value).to_s.downcase <=> send(facet_value_method, b.value).to_s.downcase
    end
  end

  def staff_url(doc_or_obj)
    "#{Ddr::Public.staff_app_url}id/#{doc_or_obj.permanent_id}"
  end

  def url_for_document(doc, options = {})
    if respond_to?(:blacklight_config) &&
       blacklight_config.show.route
      route = blacklight_config.show.route.merge(action: :show, id: doc).merge(options)
      route[:controller] = controller_name if route[:controller] == :current
      route
    else
      doc
    end
  end

  def git_branch_info
    `git rev-parse --abbrev-ref HEAD`
  end

  def has_search_parameters?
    params[:q].present? || params[:f].present? || params[:search_field].present?
  end
end
