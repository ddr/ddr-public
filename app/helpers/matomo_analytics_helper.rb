# frozen_string_literal: true

module MatomoAnalyticsHelper
  # Matomo Analytics Custom Dimensions.
  # These are logged with every pageview and custom Event.
  # https://matomo.org/guide/reporting-tools/custom-dimensions/

  def matomo_permanent_id(document, _options = {})
    document.permanent_id_ssi || document.id
  end

  def matomo_document_model(document, _options = {})
    document.common_model_name_ssi.downcase
  end

  def matomo_admin_set(document, _options = {})
    document.collection.admin_set_ssi || 'none' if document.collection
  end

  def matomo_collection(document, _options = {})
    document.collection.permanent_id_ssi || document.collection.id if document.collection
  end

  def matomo_item(document, _options = {})
    document.item.permanent_id_ssi || document.item.id if document.item
  end
end
