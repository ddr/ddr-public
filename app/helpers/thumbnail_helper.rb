# frozen_string_literal: true

module ThumbnailHelper
  def render_thumbnail_link(document, _size, counter = nil)
    link_to_document document, thumbnail_image_tag(document, {}), counter:
  end

  def thumbnail_image_tag(document, _image_options = {})
    image_tag(thumbnail_path(document), alt: 'Thumbnail', class: 'img-thumbnail', size: '175x175')
  end

  def thumbnail_path(document)
    if cached_read_public_ability(document)
      return Rails.cache.fetch("thumbnail_helper/thumbnail_path/#{document.id}", expires_in: 1.hour) do
        Thumbnail.new({ document:,
                        read_permission: true,
                        public_read_permission: true }).thumbnail_path
      end
    end

    Thumbnail.new({ document:,
                    read_permission: cached_read_current_ability(document),
                    public_read_permission: false }).thumbnail_path
  end

  def thumbnail_icon?(document)
    thumbnail_path(document).start_with?('ddr-icons/')
  end

  def default_thumbnail_path(document)
    Thumbnail::Default.new({ document: }).thumbnail_path
  end
end
