# frozen_string_literal: true

module MultiresImageHelper
  include Rails.application.routes.url_helpers
  include Ddr::Public::Controller::IiifImagePaths

  def iiif_image_tag(identifier, options)
    url = iiif_image_path(identifier, options)
    image_tag url, alt: options[:alt].presence, class: options[:class].presence
  end

  def image_item_identifiers(document)
    document.multires_image_file_arks
  end

  def image_item_tilesources(document)
    image_item_identifiers(document).map { |i| iiif_image_info_path(i) }
  end

  def image_item_first_component_doc(document)
    search_service = Blacklight::SearchService.new(config: blacklight_config,
                                                   user_params: { q: "permanent_id_ssi:#{document.first_multires_image_file_ark}" },
                                                   search_builder_class: SearchBuilder)
    (_response, doc_list) = search_service.search_results do |builder|
      builder.append(:include_only_multires_images)
             .append(:include_only_first_result)
             .except(:add_facetting_to_solr)
             .except(:add_range_limit_params)
    end
    doc_list&.first
  end

  def image_item_aspectratio(document)
    image_doc = image_item_first_component_doc(document)
    # Get the width / height ratio of the first multires component's image
    image_component_aspectratio(image_doc)
  end

  def image_component_aspectratio(document)
    return 1.0 if document.blank?

    w = document.fetch('techmd_image_width_isim', [1000])&.first
    h = document.fetch('techmd_image_height_isim', [1000])&.first
    w.to_f / h
  end

  def image_component_tilesource(document)
    # For a single component object that has a multi-res image, get the info.json path.
    return iiif_image_info_path(document.permanent_id_ssi) if cached_read_public_ability(document)

    iiif_image_info_path(document.permanent_id_ssi)
  end

  # Link to download an image server-generated JPG
  def image_download_link(document, options = {})
    # Use first image component document; fall back to current document
    image_doc = image_item_first_component_doc(document) || document
    text = options[:linklabel] + ' ' + content_tag(:span, multi_image_download_label(options[:pixels]),
                                                   class: 'text-muted small ml-1').html_safe
    url = iiif_image_path(image_doc.permanent_id_ssi, size: iiif_image_size_maxpx(options[:pixels]))
    link_to(text.html_safe, url, class: 'download-link-single dropdown-item',
                                 download: image_doc.public_id)
  end

  # Button to export images (PDF or ZIP)
  def image_export_button(document, options = {})
    form_tag({ controller: 'export_images', action: "export_#{options[:format]}", id: document.id }) do
      button_tag options[:label],
                 type: 'submit',
                 id: "image-export-#{options[:format]}",
                 class: 'btn btn-link dropdown-item rounded-0',
                 data: {
                   item: document.id,
                   loading_text: t('ddr.public.image_export.loading').html_safe,
                   original_text: options[:label].html_safe
                 }
    end
  end

  private

  def multi_image_download_label(pixels)
    px = pixels.to_s || ''
    px.present? ? "#{number_with_delimiter(px)}px" : 'Original Resolution'
  end
end
