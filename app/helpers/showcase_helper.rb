# frozen_string_literal: true

module ShowcaseHelper
  def display_collection_showcase?(options = {})
    portal = options.fetch(:portal, nil)
    return false unless portal

    collection_id = portal.collection.id

    cache_key = "display_collection_showcase_#{current_ability.user}_#{collection_id}"
    Rails.cache.fetch(cache_key, expires_in: 30.minutes) do
      portal.showcase_custom_images.any? || select_showcase_images(images: portal.showcase.documents).any?
    end
  end

  def shuffle_and_select_showcase_images(options = {})
    images = options.fetch(:images, [])
    count = options.fetch(:count, 1)

    shuffled_images = images.shuffle

    if user_signed_in?
      shuffled_images[0..count - 1]
    elsif count == 1
      [shuffled_images.find { |image| image.is_a?(String) || cached_read_public_ability(image) }]
    else
      shuffled_images.select { |image| image.is_a?(String) || cached_read_public_ability(image) }[0..count - 1]
    end
  end

  def home_showcase_locals(options = {})
    locals = {
      index: options[:index],
      image: options[:image],
      size_class: showcase_size_class(options[:showcase_layout])
    }
    if options[:image].respond_to? :multires_image_file_paths
      locals.merge!({
                      size: showcase_size(options[:showcase_layout]),
                      region: 'pct:5,5,90,90',
                      caption_length: showcase_caption_length(options[:showcase_layout])
                    })
    end
    locals
  end

  def showcase_samples(options = {})
    options[:showcase_layout] == 'landscape' ? 1 : 2
  end

  def dc_portal_showcase_image_path(options = {})
    doc = options.fetch(:doc, nil)
    size = options.fetch(:size, '!1200,1200')
    region = options.fetch(:region, 'pct:5,5,90,90')

    return unless doc.respond_to?(:multires_image_file_arks) && doc.multires_image_file_arks.any?

    iiif_image_path(doc.multires_image_file_arks.first, { size:, region: })
  end

  # TODO: consider refactoring. This method is a bit overloaded given that a
  # showcase image can be either a filename of a static asset (a string) or a
  # SolrDocument for a DDR object.
  def showcase_image_path(options = {})
    image = options[:locals][:image]
    doc = image if image.is_a?(SolrDocument)
    size = options[:locals][:size]
    region = options[:locals][:region]
    collection = options[:params][:collection]
    controller_name = options[:controller_name]

    if doc&.first_multires_image_file_ark
      iiif_image_path(doc&.first_multires_image_file_ark, { size:, region: })
    elsif asset_exist?("ddr-portals/#{collection || controller_name}/#{image}")
      image_path("ddr-portals/#{collection || controller_name}/#{image}")
    end
  end

  def showcase_abstract_more(_options = {})
    return unless @portal.abstract.present? && @portal.description.present?

    link_to raw('More&nbsp;&raquo;'), { anchor: 'about-collection' }, class: 'more-link font-weight-semibold ml-1',
                                                                      title: 'Learn more about the collection'
  end

  private

  def asset_exist?(path)
    if Rails.configuration.assets.compile
      Rails.application.precompiled_assets.include? path
    else
      Rails.application.assets_manifest.assets[path].present?
    end
  end

  def select_showcase_images(options = {})
    images = options.fetch(:images, [])

    if user_signed_in?
      images
    else
      images.select { |i| i.is_a?(String) || cached_read_public_ability(i) }
    end
  end

  def showcase_caption_length(layout)
    layout == 'landscape' ? 100 : 50
  end

  def showcase_size(layout)
    layout == 'landscape' ? '1200,' : '600,'
  end

  def showcase_size_class(layout)
    layout == 'landscape' ? 'col-md-8 col-sm-12' : 'col-md-4 col-sm-6'
  end
end
