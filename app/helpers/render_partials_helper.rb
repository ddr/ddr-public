# frozen_string_literal: true

module RenderPartialsHelper
  include Blacklight::RenderPartialsHelperBehavior

  ##
  # Given a doc and a base name for a partial, this method will attempt to render
  # an appropriate partial based on the document format and view type.
  #
  # If a partial that matches the document format is not found,
  # render a default partial for the base name.
  #
  # @see #document_partial_path_templates
  #
  # @param [SolrDocument] doc
  # @param [String] base name for the partial
  # @param [Hash] local variables to pass through to the partials

  # This should check whether there is a field set for collection_name
  # If there is, this value should be passed into the method that checks for templates
  # LIKE find_document_show_template_with_view(view_type, base_name, collection_name, format, locals)
  def render_document_partial(doc, base_name, locals = {})
    format = Deprecation.silence(Blacklight::RenderPartialsHelperBehavior) { document_partial_name(doc, base_name) }

    # DUL CUSTOMIZATION: also check for model-specific partials.
    ddr_model = document_ddr_model_name(doc, base_name).downcase

    view_type = document_index_view_type
    template = cached_view ['show', view_type, base_name, ddr_model, format].join('_') do
      find_document_show_template_with_view(view_type, base_name, ddr_model, format, locals)
    end

    if template
      template.render(self, locals.merge(document: doc))
    else
      ''
    end
  end

  def document_ddr_model_name(document, base_name = nil)
    view_config = blacklight_config.view_config(:show)

    ddr_model_name = if base_name && view_config.key?(:"#{base_name}_ddr_model_name_field")
                       document[view_config[:"#{base_name}_ddr_model_name_field"]]
                     end

    ddr_model_name || document[view_config.ddr_model_field]
  end

  ##
  # A list of document partial templates to try to render for a document
  #
  # The partial names will be interpolated with the following variables:
  #   - action_name: (e.g. index, show)
  #   - index_view_type: (the current view type, e.g. list, gallery)
  #   - format: the document's format (e.g. book)
  #
  # @see #render_document_partial
  def document_partial_path_templates
    # first, the legacy template names for backwards compatbility
    # followed by the new, inheritable style
    # finally, a controller-specific path for non-catalog subclasses

    # DUL CUSTOMIZATION: check for model-specific partials.
    @document_partial_path_templates ||= ['%<action_name>s_%<index_view_type>s_%<format>s',
                                          '%<action_name>s_%<index_view_type>s_default',
                                          '%<action_name>s_%<format>s_%<ddr_model>s',
                                          '%<action_name>s_%<format>s',
                                          '%<action_name>s_%<ddr_model>s',
                                          '%<action_name>s_default',
                                          'catalog/%<action_name>s_%<format>s',
                                          'catalog/_%<action_name>s_partials/%<format>s',
                                          'catalog/_%<action_name>s_partials/default']
  end

  private

  # DUL CUSTOMIZATION: check for model-specific partials.
  def find_document_show_template_with_view(view_type, base_name, ddr_model, format, locals)
    document_partial_path_templates.each do |str|
      partial = format(str, action_name: base_name, ddr_model:, format:, index_view_type: view_type)
      logger.debug "Looking for document partial #{partial}"
      template = lookup_context.find_all(partial, lookup_context.prefixes + [''], true, locals.keys + [:document],
                                         {}).first
      return template if template
    end
    nil
  end
end
