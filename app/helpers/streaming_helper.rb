# frozen_string_literal: true

module StreamingHelper
  def jwplayer_locals(options = {})
    case options[:media_mode]
    when 'audio'
      options[:height] = '40'
    when 'video'
      options[:aspectratio] = '4:3'
    end
    {
      derivatives: options[:derivatives],
      width: options[:width] ||= '100%',
      height: options[:height],
      aspectratio: options[:aspectratio],
      media_type: options[:media_type],
      media_mode: options[:media_mode]
    }
  end

  def media_mode(mimetype)
    case mimetype
    when %r{^(audio/)}
      'audio'
    when 'application/mp4', %r{^(video/)}
      'video'
    else
      'unknown'
    end
  end
end
