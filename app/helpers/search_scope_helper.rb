# frozen_string_literal: true

module SearchScopeHelper
  # View helper
  def render_search_scope_dropdown_home
    library_collections_scopes = []
    library_collections_scopes << search_scope_ddr_all
    library_collections_scopes << search_scope_dc_from_home
    library_collections_scopes << search_scope_acquired

    duke_scholarship_scopes = []
    duke_scholarship_scopes << search_scope_rdr
    duke_scholarship_scopes << search_scope_dspace

    home_search_scopes = {
      'Library Collections': library_collections_scopes,
      'Duke Scholarship': duke_scholarship_scopes
    }

    render 'catalog/search_scope_dropdown_homepage', search_scope_options: home_search_scopes
  end

  # View helper
  def render_search_scope_dropdown(params = {})
    active_search_scopes = []
    if request.path =~ %r{^/dc.*$} && params[:collection].present?
      active_search_scopes << search_scope_dc_this_collection
    end
    active_search_scopes << search_scope_dc if %r{^/dc.*$}.match?(request.path)
    active_search_scopes << search_scope_acquired if params[:controller] == 'acquired_materials'
    active_search_scopes << search_scope_ddr_all

    return unless active_search_scopes.count > 1

    render 'search_scope_dropdown', active_search_scope_options: active_search_scopes
  end

  private

  # Search scope dropdown configurations. These end up rendered as
  # <option> tags -- see options_for_select Rails helper docs.
  # [text label, value / form action, { additional html config } ]
  # ==============================================================

  def search_scope_ddr_all
    [t('blacklight.search.form.dropdown.ddr.label'),
     search_catalog_url,
     { data: { placeholder: t('blacklight.search.form.dropdown.ddr.placeholder') } }]
  end

  def search_scope_dc_from_home
    [t('blacklight.search.form.dropdown.dc.label_from_home'),
     digital_collections_index_portal_url,
     { data: { placeholder: t('blacklight.search.form.dropdown.dc.placeholder') } }]
  end

  def search_scope_dc
    [t('blacklight.search.form.dropdown.dc.label'),
     digital_collections_index_portal_url,
     { data: { placeholder: t('blacklight.search.form.dropdown.dc.placeholder') } }]
  end

  def search_scope_dc_this_collection
    [t('blacklight.search.form.dropdown.this_coll.label'),
     digital_collections_url(params[:collection]),
     { data: { placeholder: t('blacklight.search.form.dropdown.this_coll.placeholder') } }]
  end

  def search_scope_acquired
    [t('blacklight.search.form.dropdown.acquired.label'),
     search_acquired_materials_url,
     { data: { placeholder: t('blacklight.search.form.dropdown.acquired.placeholder') } }]
  end

  def search_scope_rdr
    [t('blacklight.search.form.dropdown.rdr.label'),
     'external',
     { data: { placeholder: t('blacklight.search.form.dropdown.rdr.placeholder'),
               baseurl: t('blacklight.search.form.dropdown.rdr.baseurl') } }]
  end

  def search_scope_dspace
    [t('blacklight.search.form.dropdown.dspace.label'),
     'external',
     { data: { placeholder: t('blacklight.search.form.dropdown.dspace.placeholder'),
               baseurl: t('blacklight.search.form.dropdown.dspace.baseurl') } }]
  end
end
