# frozen_string_literal: true

module EmbedHelper
  def embeddable?(document)
    (%w[image folder audio video].include? document.display_format_ssi)
  end

  def purl_or_doclink(document, _options = {})
    @purl_or_doclink ||= document.permanent_url_ssi || "#{Ddr::Public.root_url}#{document_or_object_url(document)}"
  end

  def iframe_src_path(document, _options = {})
    path = purl_or_doclink(document)
    "#{path}?embed=true"
  end

  def iframe_embed_height(document, _options = {})
    case document.display_format_ssi
    when 'audio'
      125
    else
      500
    end
  end

  # Indicate presence of a transcript, whether WebVTT closed captions datastream
  # OR a sibling component (e.g., a PDF) that is likely a transcript, determined
  # by the word "transcript" appearing in the component's title or format field.

  def has_caption_or_transcript?(document)
    document.captions_urls.present? || has_transcript_component?(document)
  end

  private

  def has_transcript_component?(document)
    titles_and_formats = []
    titles_and_formats << component_titles(document) << component_formats(document)
    titles_and_formats.flatten.any? { |s| s&.downcase&.include?('transcript') }
  end

  def component_titles(document)
    document.structures.files.map { |f| f[:doc].title_ssi }
  end

  def component_formats(document)
    document.structures.files.map { |f| f[:doc].format }
  end
end
