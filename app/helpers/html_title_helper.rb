# frozen_string_literal: true

module HtmlTitleHelper
  include Blacklight::CatalogHelperBehavior

  # DUL CUSTOM html title that extends BL's native render_search_to_page_title.
  # We add our own portal context (e.g., Digital Collections or Ad*Access) and
  # also have a catch to make a blank search not just return an empty string in
  # the title.
  # https://github.com/projectblacklight/blacklight/blob/release-7.x/app/helpers/blacklight/catalog_helper_behavior.rb#L322-L349
  def search_results_html_title(portal)
    t('blacklight.search.page_title.title',
      constraints: [blank_search_check(params),
                    sanitize(render_search_to_page_title(params)),
                    portal&.html_title_context].compact_blank.join(' / '),
      application_name:)
  end

  private

  def blank_search_check(params)
    'Browse Everything' unless params[:q].present? || params[:f].present?
  end
end
