# frozen_string_literal: true

require 'open-uri'

# rubocop:disable Metrics/ModuleLength
module CatalogHelper
  include Blacklight::CatalogHelperBehavior

  # Predicate methods for object types
  def is_item?(document)
    document['common_model_name_ssi'] == 'Item'
  end

  def is_component?(document)
    document['common_model_name_ssi'] == 'Component'
  end

  def is_collection?(document)
    document['common_model_name_ssi'] == 'Collection'
  end

  def has_two_or_more_multires_images?(document)
    document.multires_image_file_paths.present? && document.multires_image_file_paths.length > 1
  end

  # Used in DC "about" pages to render inline img tags for items using local_id
  def item_image_embed(options = {})
    search_service = Blacklight::SearchService.new(config: blacklight_config,
                                                   user_params: { q: "local_id_ssi:#{options[:local_id]}",
                                                                  rows: 1 },
                                                   search_builder_class: ::ItemSearchBuilder)
    # search_service.search_results returns [@solr_response, @document_list]
    (_, doc_list) = search_service.search_results

    return if doc_list.empty?

    identifier = doc_list.first.multires_image_file_arks.first
    iiif_image_tag(identifier,
                   { size: options[:size], alt: doc_list.first.title_ssi, class: options[:class] })
  end

  # Used in DC "about" pages to render inline img tags for items using local_id
  def item_title_link(options = {})
    search_service = Blacklight::SearchService.new(config: blacklight_config,
                                                   user_params: { q: "local_id_ssi:#{options[:local_id]}",
                                                                  rows: 1 },
                                                   search_builder_class: ::ItemSearchBuilder)
    (response, doc_list) = search_service.search_results
    link_to_document(doc_list.first) unless response.empty?
  end

  # View helper: from EITHER collection show page or configured collection portal, browse items.
  def collection_browse_items_url(document, _options = {})
    if @portal
      search_action_url(search_state.add_facet_params('common_model_name_ssi', 'Item'))
    else
      source_params = { 'f[collection_title_ssi][]' => document['collection_title_ssi'],
                        'f[admin_set_title_ssi][]' => document['admin_set_title_ssi'] }
      search_action_url(
        controller.search_state_class.new(source_params, blacklight_config)
                  .add_facet_params('common_model_name_ssi', 'Item')
      )
    end
  end

  def item_browse_components_url(document, _options = {})
    search_action_url(search_state.add_facet_params('is_part_of_ssim', document.id)
                                  .merge({ show_components: 'true' }))
  end

  # Index / Show field view helper
  def file_info(options = {})
    document = options[:document]
    if cached_download_current_ability(document)
      render('download_link_and_icon', document:)
    elsif user_signed_in?
      render('download_not_authorized', document:)
    else
      render 'download_restricted', document:
    end
  end

  # View helper
  def render_download_link(args = {})
    return unless args[:document]

    label = icon('fas', 'download', 'Download')
    link_to label, download_path(args[:document]), class: args[:css_class], id: args[:css_id], data: args[:data]
  end

  # View helper
  def default_mime_type_label(mime_type)
    case mime_type
    when /^image/
      'Image'
    when /^video/
      'Video'
    when /^audio/
      'Audio'
    when %r{^application/(x-)?pdf}
      'PDF'
    when /^application/
      'Binary'
    when %r{^text/comma-separated-values}
      'CSV'
    else
      'File'
    end
  end

  def get_research_help(document)
    Timeout.timeout(2) do
      document.research_help
    end
  rescue StandardError => e
    Rails.logger.error { "#{e.message} #{e.backtrace.join("\n")}" }
    false
  end

  # View helper
  def research_help_title(research_help)
    link_to_if(research_help.url, research_help.name, research_help.url) if research_help.name.present?
  end

  def read_not_authorized_contact_name(research_help)
    return research_help.name if research_help && research_help.name.present?

    I18n.t('blacklight.branding.library_name')
  end

  def read_not_authorized_contact_url(research_help)
    if research_help && research_help.ask.present?
      "#{research_help.ask}?&referrer=#{request.original_url}"
    elsif research_help && research_help.email.present?
      "mailto:#{research_help.email}"
    else
      Ddr::Public.help_url
    end
  end

  # View helper
  def finding_aid_popover(finding_aid)
    popover = []
    if finding_aid.collection_title
      popover << ("<h4>#{finding_aid.collection_title}#{faid_date_span(finding_aid)}</h4>")
    end
    if finding_aid.collection_number || finding_aid.extent
      popover << ("<p class=\"small text-muted\">#{faid_collno_and_extent(finding_aid)}</p><hr/>")
    end
    popover << ("<p class=\"small\">#{finding_aid.abstract.truncate(250, separator: ' ')}</p>") if finding_aid.abstract
    if finding_aid.repository
      popover << ("<div class=\"small card bg-light\"><div class=\"card-body p-2\">View this item in person at:<br/>#{finding_aid.repository}</div></div>")
    end
    popover.join
  end

  # We need to (temporarily?) account for the fact that we have a mix of ASpace ID values
  # in DDR metadata: some have an aspace_* prefix and others do not. ArcLight
  # component URLs will always have the prefix, so it must be present in the link.
  def finding_aid_component_link(document)
    return unless document.aspace_id_ssi

    stored_id = document.aspace_id_ssi
    normalized_id = stored_id.start_with?('aspace_') ? stored_id : "aspace_#{stored_id}"
    [document.finding_aid.url, '_', normalized_id].join
  end

  # Find matching collections for the search results "Matching Collections" box. This
  # needs to use the SearchService's default search_builder_class b/c the scope varies by
  # controller (e.g., Acquired Materials vs. DC).
  def find_collection_results
    @find_collection_results ||=
      begin
        search_service = Blacklight::SearchService.new(config: blacklight_config,
                                                       search_state:,
                                                       **controller.search_service_context)

        # search_service.search_results returns [@solr_response, @document_list]
        (response, doc_list) = search_service.search_results do |builder|
          builder.append(:include_only_collections)
                 .except(:add_facetting_to_solr)
                 .except(:add_range_limit_params)
        end
        { docs: doc_list, count: response.dig(:response, :numFound)&.to_i }
      end
  end

  def show_collection_results?
    # We only want to show the matching collections box when a particular
    # combination of conditions is true:

    ### 1. There are matching collection results
    find_collection_results.fetch(:count).positive? &&

      # 2. The matching collection results aren't identical to the main results
      @response.documents.length != find_collection_results.fetch(:count) &&

      # 3. We are not in a single-collection portal context with a local_id like /dc/adaccess
      !@portal&.local_id &&

      # 4. We aren't currently faceted on Model (Collection, Item, Component)
      !params.dig(:f, :common_model_name_ssi) &&

      # 5. We are on the first page of results
      @response.first_page?
  end

  def get_blog_posts(blog_url)
    return if blog_url.blank?

    begin
      JSON.parse(
        URI.open(blog_url,
                 { read_timeout: 2 }).read
      )
    rescue Net::ReadTimeout, Errno::ECONNREFUSED, JSON::ParserError, OpenURI::HTTPError => e
      Rails.logger.error { "#{e.message} #{e.backtrace.join("\n")}" }
      fallback_link = link_to 'See All Blog Posts', 'https://blogs.library.duke.edu/bitstreams'
      "<p class='small'>#{fallback_link}</p>"
    end
  end

  def blog_post_thumb(post)
    # If a post has a selected "featured image", use that.
    # In the WP-JSON API, for some reason, thumbnail_images can either be
    # an empty array or a populated hash, so coerce to hash.
    post['thumbnail_images'] = post['thumbnail_images']&.to_h
    post.dig('thumbnail_images', 'thumbnail', 'url') || image_path('ddr/devillogo-150-square.jpg')
  end

  def link_to_admin_set(document, options = {})
    name = document['admin_set_title_ssi']
    url =  facet_search_url('admin_set_title_ssi', name)
    link_to name, url, class: options[:class], id: 'admin-set'
  end

  def research_guide(values = [])
    values.find { |value| is_research_guide? value } if values.present?
  end

  def document_dropdown_label(document)
    if document.structures.files.count == 1 &&
       document.structures.files.first[:doc].format.present?
      document.structures.files.first[:doc].format.join(' ')
    else
      I18n.t('ddr.public.document_dropdown')
    end
  end

  def link_to_search_inside_full_text(doc)
    url = "#{Ddr::Public.contentdm_url}/collection/" \
          "#{doc['contentdm_id_ssi']}"
    link_to(I18n.t('ddr.public.search_inside_dropdown.full_text_link'), url, class: 'dropdown-item')
  end

  def link_to_search_inside_pdf(doc)
    url = "#{Ddr::Public.contentdm_url}/api/collection/" \
          "#{doc['contentdm_id_ssi']}/page/0/inline/" \
          "#{doc['contentdm_id_ssi'].split('/id/').join('_')}_0"
    link_to(I18n.t('ddr.public.search_inside_dropdown.pdf_link'), url, class: 'dropdown-item')
  end

  private

  def facet_search_url(field, value)
    search_action_url(search_state.add_facet_params(field, value))
  end

  def derivative_file_extension(document)
    case document.display_format_ssi
    when 'audio'
      'mp3'
    when 'video'
      'mp4'
    end
  end

  def faid_date_span(finding_aid)
    " <span class=\"small text-muted\">#{finding_aid.collection_date_span}</span>" if finding_aid.collection_date_span
  end

  def faid_collno_and_extent(finding_aid)
    section = []
    section << '<p class="small text-muted">'
    section << ("Collection ##{finding_aid.collection_number}") if finding_aid.collection_number
    section << ("<br/>#{finding_aid.extent}") if finding_aid.extent
    section << '</p>'
    section.join
  end

  def is_research_guide?(value)
    research_guides_whitelist.filter_map do |config|
      value =~ %r{^https?://#{Regexp.quote(config).chomp('/')}/.*}
    end.present?
  end

  def research_guides_whitelist
    String(Ddr::Public.research_guides).split(/,\s?/)
  end

  # DUL CUSTOM override of a BL helper to patch a bug in blacklight_range_limit.
  # We have to wrap the filter_value in a strip_tags(), else it causes problems
  # on a search results page with a range facet applied.
  # https://github.com/projectblacklight/blacklight_range_limit/issues/189
  # https://github.com/projectblacklight/blacklight/blob/main/app/helpers/blacklight/catalog_helper_behavior.rb#L190-L205
  def render_search_to_page_title_filter(facet, values)
    facet_config = facet_configuration_for_field(facet)
    filter_label = facet_field_label(facet_config.key)
    filter_value = if values.size < 3
                     values.map { |value| facet_item_presenter(facet_config, value, facet).label }.to_sentence
                   else
                     t('blacklight.search.page_title.many_constraint_values', values: values.size)
                   end
    t('blacklight.search.page_title.constraint', label: filter_label, value: strip_tags(filter_value))
  end
end
# rubocop:enable Metrics/ModuleLength
