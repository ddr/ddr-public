# frozen_string_literal: true

require 'rspec/core/rake_task'

# rubocop:disable Metrics/BlockLength
namespace :ddr_public do
  desc 'Disable certain tasks from running in a production environment'
  task guard_production!: [:environment] do
    puts "Checking environment before proceeding. Currently on: #{Rails.env}."
    raise "Sorry, you can't run this task in a PRODUCTION environment." if Rails.env.production?
  end

  desc 'Destroy all documents in the index'
  task destroy_index_docs: [:guard_production!] do
    puts '------------------------------------'
    puts 'Deleting all documents from index...'
    puts '------------------------------------'
    system("curl 'solr:8983/solr/ddr-public/update?commit=true' --data-binary \
           '<delete><query>*:*</query></delete>' -H 'Content-Type: text/xml'")
  end

  desc 'Wait for solr and the ddr-public core to be ready'
  task wait_for_solr: :environment do
    puts '----------------------------------------'
    puts 'Waiting for the solr core to be ready...'
    puts '----------------------------------------'
    status = ''
    until status == 'OK'
      healthcheck = JSON.parse(`curl 'solr:8983/solr/ddr-public/admin/ping?wt=json'`)
      status = healthcheck['status']
      sleep 3
    end
  end

  namespace :config do
    desc 'Copy sample config files'
    task samples: :environment do
      Dir.glob('config/**/*.sample') do |sample|
        actual = sample.gsub('.sample', '')
        FileUtils.cp sample, actual, verbose: true unless File.exist?(actual)
      end
    end
  end

  namespace :ci do
    desc 'Prepare for CI build'
    task prepare: ['ddr_public:config:samples', 'db:test:prepare', 'jetty:clean', 'jetty:config']

    desc 'CI build'
    task build: :prepare do
      ENV['environment'] = 'test'
      jetty_params = Jettywrapper.load_config
      jetty_params[:startup_wait] = 60
      Jettywrapper.wrap(jetty_params) do
        Rake::Task['spec'].invoke
      end
    end
  end

  namespace :test do
    desc 'Default tests (all tests except those tagged "accessibility")'
    RSpec::Core::RakeTask.new(default: ['ddr_public:wait_for_solr', 'ddr_public:destroy_index_docs',
                                        'ddr_public:seed:fixtures', 'db:test:prepare']) do |t, _|
      t.rspec_opts = '--tag ~accessibility'
    end
    desc 'Accessibility tests'
    RSpec::Core::RakeTask.new(accessibility: ['ddr_public:wait_for_solr', 'ddr_public:destroy_index_docs',
                                              'ddr_public:seed:fixtures', 'db:test:prepare']) do |t, _|
      t.rspec_opts = '--tag accessibility'
    end
  end

  namespace :seed do
    # Seed Test Data (From spec/fixtures/*)
    desc 'Ingest seed data for tests'
    task fixtures: [:guard_production!] do
      %w[collections items components].each do |obj|
        puts "Seeding index with data for #{obj}..."
        Dir.glob("spec/fixtures/files/solr_documents/#{obj}/*.json").each do |file|
          puts "indexing #{file}"
          system("curl 'solr:8983/solr/ddr-public/update?commit=true' --data-binary \
                  @#{file} -H 'Content-type:application/json'")
        end
      end
    end
  end

  namespace :sample_data do
    desc 'Transform saved Solr query results into cleaned sample JSON data for indexing'
    task prepare: [:guard_production!] do
      puts 'Preparing sample data for indexing.'
      script_path = Rails.root.join('sample_data/get-solr-docs.rb')
      ruby script_path
    end

    desc 'Index all sample data into Solr'
    task index: [:guard_production!] do
      puts '============================'
      puts 'Indexing the sample data.   '
      puts '============================'

      puts '----------------------------'
      puts 'Indexing sample collections.'
      puts '----------------------------'
      system("curl 'solr:8983/solr/ddr-public/update?commit=true' --data-binary \
              @sample_data/data/collections.json -H 'Content-type:application/json'")

      puts '----------------------------'
      puts 'Indexing sample items.      '
      puts '----------------------------'
      system("curl 'solr:8983/solr/ddr-public/update?commit=true' --data-binary \
              @sample_data/data/items.json -H 'Content-type:application/json'")

      puts '-----------------------------'
      puts 'Indexing sample components.  '
      puts '-----------------------------'
      system("curl 'solr:8983/solr/ddr-public/update?commit=true' --data-binary \
              @sample_data/data/components.json -H 'Content-type:application/json'")
    end
  end
end
# rubocop:enable Metrics/BlockLength
