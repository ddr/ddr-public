# frozen_string_literal: true

module Ddr
  module Public
    module Configurable
      extend ActiveSupport::Concern

      included do
        mattr_accessor :adopt_url do
          ENV['ADOPT_URL'] || 'http://library.duke.edu/about/adopt-digital-collection'
        end

        mattr_accessor :alert_message do
          ENV.fetch('ALERT_MESSAGE', nil)
        end

        mattr_accessor :base_url do
          if ENV['APPLICATION_HOSTNAME'].present?
            "https://#{ENV['APPLICATION_HOSTNAME']}"
          else
            'http://localhost:3000'
          end
        end

        mattr_accessor :contact_email do
          ENV['CONTACT_EMAIL'] || 'repositoryhelp@duke.edu'
        end

        mattr_accessor :contentdm_url do
          ENV['CONTENTDM_URL'] || 'https://dukelibraries.contentdm.oclc.org/digital'
        end

        mattr_accessor :catalog_url do
          ENV['CATALOG_URL'] || 'https://find.library.duke.edu/catalog/DUKE'
        end

        mattr_accessor :ddr_api_key do
          ENV['DDR_API_KEY']
        end

        mattr_accessor :ddr_api_url do
          ENV.fetch('DDR_API_URL', nil)
        end

        mattr_accessor :ddr_logo do
          ENV['DDR_LOGO'] || false
        end

        mattr_accessor :ddr_public_preview do
          ENV['DDR_PUBLIC_PREVIEW'] || false
        end

        mattr_accessor :download_request_form do
          ENV['DOWNLOAD_REQUEST_FORM'] || 'https://duke.qualtrics.com/jfe/form/SV_6rnSzrjrZff22FL'
        end

        mattr_accessor :finding_aid_base_url do
          ENV['FINDING_AID_BASE_URL'] || 'https://archives.lib.duke.edu'
        end

        mattr_accessor :help_url do
          ENV['HELP_URL'] || 'https://duke.qualtrics.com/jfe/form/SV_eo1QIMoUtvmteQZ?Q_JFE=qdg'
        end

        mattr_accessor :iiif_dynamic_manifests do
          ActiveModel::Type::Boolean.new.cast(ENV.fetch('IIIF_DYNAMIC_MANIFESTS', 'false'))
        end

        mattr_accessor :image_server_url do
          ENV['IMAGE_SERVER_URL'] || 'https://repository.duke.edu/iipsrv/iipsrv.fcgi'
        end

        mattr_accessor :login_group do
          ENV.fetch('LOGIN_GROUP', nil) # If not set, all authenticated users can login
        end

        mattr_accessor :matomo_analytics_debug do
          ENV['MATOMO_ANALYTICS_DEBUG'] || false
        end

        mattr_accessor :matomo_analytics_embed_id do
          ENV['MATOMO_ANALYTICS_EMBED_ID'] || false
        end

        mattr_accessor :matomo_analytics_host do
          ENV['MATOMO_ANALYTICS_HOST'] || 'analytics.lib.duke.edu'
        end

        mattr_accessor :matomo_analytics_site_id do
          ENV['MATOMO_ANALYTICS_SITE_ID'] || false
        end

        mattr_accessor :paging_limit_facets do
          ENV['PAGING_LIMIT_FACETS'] || '50'
        end

        mattr_accessor :paging_limit_results do
          ENV['PAGING_LIMIT_RESULTS'] || '250'
        end

        mattr_accessor :pronomid_lookup_url do
          ENV['PRONOMID_LOOKUP_URL'] || 'https://www.nationalarchives.gov.uk/pronom/'
        end

        mattr_accessor :require_authentication do
          ENV['REQUIRE_AUTHENTICATION'].present?
        end

        mattr_accessor :research_guides do
          ENV['RESEARCH_GUIDES'] || 'guides.library.duke.edu'
        end

        mattr_accessor :root_url do
          ENV['ROOT_URL'] || 'https://repository.duke.edu'
        end

        mattr_accessor :staff_app_url do
          ENV['STAFF_APP_URL'] || 'https://ddr-admin.lib.duke.edu/'
        end

        mattr_accessor :storage_base_path do
          ENV['STORAGE_BASE_PATH'] || '/data'
        end
      end

      module ClassMethods
        def configure
          yield self
        end
      end
    end
  end
end
