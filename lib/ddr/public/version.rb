# frozen_string_literal: true

# rubocop:disable Naming/ConstantName
module Ddr
  module Public
    Version = ENV.fetch('APP_VERSION', 'development')
  end
end
# rubocop:enable Naming/ConstantName
