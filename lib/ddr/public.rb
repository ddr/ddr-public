# frozen_string_literal: true

require 'ddr/public/version'

module Ddr
  module Public
    autoload :Configurable, 'ddr/public/configurable'

    include Ddr::Public::Configurable

    class Error < ::StandardError; end
  end

  # Maps media types to preferred file extensions
  # where we want to override the first result from MIME::Types.
  def self.preferred_file_extensions
    @preferred_file_extensions ||= YAML.load_file(File.expand_path('../../config/preferred_file_extensions.yml',
                                                                   __dir__))
  end
end
