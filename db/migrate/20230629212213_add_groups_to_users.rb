class AddGroupsToUsers < ActiveRecord::Migration[6.1]
  def change
    change_table :users do |t|
      t.jsonb :groups, default: []
    end
  end
end
