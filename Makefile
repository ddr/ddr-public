SHELL = /bin/bash

repository ?= ddr-public
build_tag ?= $(repository):latest

bundler_version = $(shell tail -1 Gemfile.lock | awk '{print $$1}')
ruby_version = $(shell cat .ruby-version)
bundler_image ?= bundler:ddr-public

.PHONY : build
build:
	build_tag=$(build_tag) repository=$(repository) ./build.sh

.PHONY : clean
clean:
	rm -rf ./tmp/*
	rm -f ./log/*.log

.PHONY : test
test: clean
	./test.sh

.PHONY : accessibility
accessibility:
	./test-a11y.sh

.PHONY: lock
lock:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) \
		bundle lock

.PHONY: audit
audit:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) ./audit.sh

.PHONY: update
update:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) \
		bundle update $(args)

.PHONY: rubocop
rubocop:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) \
		bundle exec rubocop $(args)

.PHONY: helm-update
helm-update:
	helm dependency update chart

.PHONY: helm-lint
helm-lint:
	helm lint chart --with-subcharts

.PHONY: chart
chart: helm-update helm-lint
