ARG ruby_version="3.3.3"
ARG builder="builder"
ARG bundle="bundle"

FROM ruby:${ruby_version} AS builder

SHELL ["/bin/bash", "-c"]

ENV APP_ROOT="/opt/app-root" \
    APP_USER="app-user" \
    APP_UID="1001" \
    APP_GID="0" \
    BUNDLE_USER_HOME="${GEM_HOME}" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    RAILS_ENV="production" \
    TZ="US/Eastern"

WORKDIR $APP_ROOT

RUN set -eux; \
    apt-get -y update; \
    apt-get -y install jq libjemalloc2 libmemcached-tools libpq-dev locales nodejs npm rsync unzip wait-for-it zip; \
    apt-get -y clean; \
    rm -rf /var/lib/apt/lists/*; \
    echo "$LANG UTF-8" >> /etc/locale.gen; \
    locale-gen $LANG; \
    npm install -g yarn; \
    gem update --system; \
    useradd -u $APP_UID -g $APP_GID -d $APP_ROOT -s /sbin/nologin $APP_USER

#------------------------------+

FROM ${builder} AS bundle

COPY .ruby-version Gemfile Gemfile.lock ./

RUN gem install bundler -v "$(tail -1 Gemfile.lock | awk '{print $1}')" && \
    bundle install && \
    chmod -R g=u $GEM_HOME

#------------------------------+

FROM ${bundle} AS app

ARG app_version="development"
ARG build_date="1970-01-01T00:00:00Z"
ARG git_commit="0"

ENV APP_VERSION="${app_version}"

LABEL org.opencontainers.image.description="Duke Digital Repository public end-user application"
LABEL org.opencontainers.image.title="Duke Digital Repository"
LABEL org.opencontainers.image.created="{build_date}"
LABEL org.opencontainers.image.license="BSD-3-Clause"
LABEL org.opencontainers.image.revision="${git_commit}"
LABEL org.opencontainers.image.source="https://gitlab.oit.duke.edu/ddr/ddr-public"
LABEL org.opencontainers.image.url="https://repository.duke.edu"
LABEL org.opencontainers.image.vendor="Duke University Libraries"
LABEL org.opencontainers.image.version="${app_version}"

COPY . .

RUN set -eux; \
    SECRET_KEY_BASE=$(./bin/rails secret) ./bin/rails assets:precompile; \
    mkdir -p -m 0775 /export; \
    chmod -R g=u .

VOLUME /export

USER $APP_USER

EXPOSE 3000

CMD ["./bin/rails", "server"]
