# ddr-public

DDR public application powering public-facing discovery & access for the [Duke Digital Repository](https://repository.duke.edu). Please consult the **[DDR-Public wiki](https://gitlab.oit.duke.edu/ddr/ddr-public/-/wikis/home)** for full documentation.

## Using the docker stack

### Requirements:

- Linux: [docker](https://docs.docker.com/engine/install/#server) + [docker-compose](https://docs.docker.com/compose/install/)
- Mac/Windows: [Docker Desktop](https://docs.docker.com/desktop/) or [Rancher Desktop](https://rancherdesktop.io)
- GNU Make - on Mac install with home brew.

### Building the containers

To build the `app` container:

    $ make

A rebuild is required when there are changes in `Gemfile.lock`.

### The development stack

To start the development stack in the background:

    $ .docker/dev.sh up -d

To initialize (or reset) the development database, run:

    $ .docker/dev.sh exec app bin/rails db:setup

To run a Bash shell on the development app server:

    $ .docker/dev.sh exec app bash

To connect to the development database:

    $ .docker/dev.sh exec app psql -U postgres -h db -d ddr_public

Stop the development stack:

    $ .docker/dev.sh down

(If using this script to start the stack, also use it
in the commands below instead of `dev.sh`.)

NOTE: You may need to export the variable `EZID_PASSWORD` with the value of the
test account password before starting the stack.

### Sample Corpus

To load (or reload), the sample corpus, run

    $ .docker/dev-plus-admin.sh exec ddr-admin bin/rails r lib/ddr/scripts/sample_corpus.rb

Reloading the sample corpus deletes the sample corpus resources and related files and database records and then recreates them.

### The test stack

To run a shell in the test stack:

    $ .docker/test-interactive.sh

Note that the working code is mounting in the `app` container
when using this script.

To run the test suite *without* the working code mounted in
the container (mainly for CI/CD), run:

    $ .docker/run_test_suite.sh

# Customizing Digital Collections (Portal Configuration)

DDR-Portals contains configurations for Digital Collections in the Duke Digital Repository.

## Configuring a Portal

A portal configuration comprises a directory named for the portal that contains yml files that set configurations for the portal. These settings override those set in Blacklight's CatalogController.

The directory structure generally looks like the following.

```
config/
  ddr-portals/
    wdukesons/
        portal_doc_configs.yml
        portal_view_configs.yml
```

### `portal_doc_configs.yml`

This configuration file contains settings that pertain to the collection and defines what controller should own the collection, the id that should be used for items in the collection (local_id or id) and the thumbnail image that should be used for the collection document. This file is required but may not contain configurations if the portal does not have a collection document, such as in the case of the Digital Collections portal.

```yml
# Specifies the identifier field
collection_local_id:
    # Specifies the identifier for the collection
    wdukesons:
        # The controller that owns this collection
        controller: digital_collections
        # How should the collection be identified in the URL
        collection: wdukesons
        # What ID should be used in the URL for items in this collection
        item_id_field: local_id
        # What item should be used to provide the thumbnail for the collection
        thumbnail_image: dscsi03005
```

### `portal_view_configs.yml`

This file contains settings that define what items belong to the portal, the items that should be displayed in the showcase and highlight sections of the portal landing page (the index view of the controller), blog post URL, and any alerts that should be displayed. It also defines the blacklight configurations that should be applied the controller when displaying the portal page for this collection.

```yml
# The items (defined by the id of the parent collections) should be included in this portal
includes:
    local_ids:
       - wdukesons
# The (if any) showcase images should be displayed prominently in the portal index view. (Defined by the local_ids of the items.)
showcase_images:
  local_ids:
     - dscsi02223
     - dscsi02230
     - dscsi02371
     - dscsi02373
  layout: portrait
# The (if any) highlight items should be displayed in the portal index view. (Defined by the local_ids of the items.)
highlight_items:
  local_ids:
    - dscsi02317
    - dscsi02331
    - dscsi04001
    - dscsi04016
  highlight_type: image # <-- optional; for cropped image squares
  display: 8 # <-- optional; how many to display
blog_posts: https://blogs.library.duke.edu/bitstreams/wp-json/ddr/v1/posts?tag_slug=wdukesons
alert: 'Welcome to the W. Duke & Sons collection...</a>'
# The blacklight configurations that should be applied to the controller for this collection.
configure_blacklight:
    add_facet_field:
      - field: "common_model_name_ssi"
        label: Browse
        show: false
      - field: "year_facet_iim"
        label: Year
        limit: 9999
        collapse: false
        range:
            num_segments: 6
            segments: true
    add_show_field:
      - field: "title_tsim"
        separator: "; "
        label: "Title"
      - field: "is_member_of_collection_ssim"
        label: "Collection"
        helper_method: descendant_of
      - field: "series_tsim"
        separator: "; "
        label: "Card Series"
        link_to_facet: "series_facet_sim"
```

## Custom about pages

Custom HTML about pages for a collection may placed in:

```
app
  views/
    ddr-portals/
      adaccess/
        about.html.erb
```

## Custom images

Images stored as part of the application can be specified for use as part of the configuration.

### Custom thumbnail for a collection

Configuration (portal_doc_configs.yml)
```yml
collection_local_id:
  adviews:
    controller: digital_collections
    collection: adviews
    item_id_field: local_id
    thumbnail_image:
      custom_image: adviews-thumb.jpg
```

Location of image in application
```
app/
  assets/
    images/
      ddr-portals/
        adviews/
          adviews-thumb.jpg
```

### Custom portal page showcase image

Configuration (portal_view_config.yml)
```yml
includes:
  local_ids:
    - adviews
showcase_images:
  custom_images:
    - adviews-showcase.jpg
  layout: landscape
```

Location of image in application
```
app/
  assets/
    images/
      ddr-portals/
        adviews/
          adviews-showcase.jpg
```
